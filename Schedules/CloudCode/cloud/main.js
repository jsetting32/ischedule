// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
require('cloud/app.js');

Parse.Cloud.define("hello", function(request, response) {
    response.success("Hello world!");
});

// Define a function like so if you want to be able to call such function
// from a web app or ios app or whatever

/* Here is an example of how to make a curl command to execute a function.
 Refer to the Parse Documents under cloud code to get better and more explained examples

 curl -X POST \
 -H "X-Parse-Application-Id: MBQdfWF9n0yIFJ3KJruX4XELCCDg4OyzUIHhGi4i" \
 -H "X-Parse-REST-API-Key: XHJngIxBQWjiEcP2eroxa3kzMMLwzIYVpNOm70Qd" \
 -H "Content-Type: application/json" \
 -d '{"movie":"The Matrix"}' \
 https://api.parse.com/1/functions/averageStars

 */

Parse.Cloud.define("getFriends", function(request, response) {


    var User = Parse.User.current();

    var friendQuery1 = new Parse.Query("Activity");
    friendQuery1.equalTo("type", "friend");
    friendQuery1.equalTo("toUser", User);

    var friendQuery2 = new Parse.Query("Activity");
    friendQuery2.equalTo("type", "friend");
    friendQuery2.equalTo("fromUser", User);

    var friendOrQuery = Parse.Query.or(friendQuery1, friendQuery2);
    friendOrQuery.find({
        success: function (results) {
            response.success(results);
        },
        error: function (){
            response.error("Friend Lookup Failed");
        }
    });

});

// Example of running code on save
Parse.Cloud.beforeSave("Review", function(request, response) {
    if (request.object.get("stars") < 1) {
        response.error("you cannot give less than one star");
    } else if (request.object.get("stars") > 5) {
        response.error("you cannot give more than five stars");
    } else {
        response.success();
    }
});

Parse.Cloud.beforeSave("Class", function(request, response) {
    if (request.object.get("stars") < 1) {
        response.error("you cannot give less than one star");
    } else if (request.object.get("stars") > 5) {
        response.error("you cannot give more than five stars");
    } else {
        response.success();
    }
});

// Example of modifying objects on saving
/*
 Parse.Cloud.beforeSave("Review", function(request, response) {
 var comment = request.object.get("comment");
 if (comment.length > 140) {
 // Truncate and add a ...
 request.object.set("comment", comment.substring(0, 137) + "...");
 }
 response.success();
 });
 */

// Example of performing actions after save
Parse.Cloud.afterSave("Comment", function(request) {
    query = new Parse.Query("Post");
    query.get(request.object.get("post").id, {
        success: function(post) {
            post.increment("comments");
            post.save();
        },
        error: function(error) {
            console.error("Got an error " + error.code + " : " + error.message);
        }
    });
});

// Example of running code on deletion
Parse.Cloud.beforeDelete("Album", function(request) {
    query = new Parse.Query("Photo");
    query.equalTo("album", request.object.id);
    query.count({
        success: function(count) {
            if (count > 0) {
                response.error("Can't delete album if it still has photos.");
            } else {
                response.success();
            }
        },
        error: function(error) {
            response.error("Error " + error.code + " : " + error.message + " when getting photo count.");
        }
    });
});

// Example of performing actions after a deletion
Parse.Cloud.afterDelete("Post", function(request) {
    query = new Parse.Query("Comment");
    query.equalTo("post", request.object.id);
    query.find({
        success: function(comments) {
            Parse.Object.destroyAll(comments, {
                success: function() {},
                error: function(error) {
                    console.error("Error deleting related comments " + error.code + ": " + error.message);
                }
            });
        },
        error: function(error) {
            console.error("Error finding related comments " + error.code + ": " + error.message);
        }
    });
});


// Example of a background job
Parse.Cloud.job("userMigration", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    var counter = 0;
    // Query for all users
    var query = new Parse.Query(Parse.User);
    query.each(function(user) {
        // Update to plan value passed in
        user.set("plan", request.params.plan);
        if (counter % 100 === 0) {
            // Set the  job's progress status
            status.message(counter + " users processed.");
        }
        counter += 1;
        return user.save();
    }).then(function() {
            // Set the job's success status
            status.success("Migration completed successfully.");
        }, function(error) {
            // Set the job's error status
            status.error("Uh oh, something went wrong.");
        });
});

/* Command on executing the above function

 curl -X POST \
 -H "X-Parse-Application-Id: MBQdfWF9n0yIFJ3KJruX4XELCCDg4OyzUIHhGi4i" \
 -H "X-Parse-Master-Key: vBdDo9YC1vtrEFgkPiXHXpvPJTSOrfOWPFzEf2j7" \
 -H "Content-Type: application/json" \
 -d '{"plan":"paid"}' \
 https://api.parse.com/1/jobs/userMigration

 */

/*
 // Example http request with json encoding
 Parse.Cloud.httpRequest({
 url: 'http://www.parse.com/',
 params: {
 q : 'Sean Plott'
 },
 headers: {
 'Content-Type': 'application/json'
 },
 success: function(httpResponse) {
 console.log(httpResponse.text);
 },
 error: function(httpResponse) {
 console.error('Request failed with response code ' + httpResponse.status);
 }
 });
 */


/*
 // Example Post request with json encoding
 Parse.Cloud.httpRequest({
 method: 'POST',
 url: 'http://www.example.com/create_post',
 body: {
 title: 'Vote for Pedro',
 body: 'If you vote for Pedro, your wildest dreams will come true'
 },
 headers: {
 'Content-Type': 'application/json'
 },
 success: function(httpResponse) {
 console.log(httpResponse.text);
 },
 error: function(httpResponse) {
 console.error('Request failed with response code ' + httpResponse.status);
 }
 });
 */