//
//  PhotoDetailViewController.m
//
//  Created by John Setting on 29/1/12.
//

#import "PhotoDetailViewController.h"

@implementation PhotoDetailViewController
@synthesize photoImageView, selectedImage, imageName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(popViewControllerWithAnimation)];
	}
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	[self initImageView];
}

- (void)initImageView {
	self.photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, 320, self.view.frame.size.height)];
	self.photoImageView.backgroundColor = [UIColor whiteColor];
	self.photoImageView.image = selectedImage;
	self.photoImageView.contentMode = UIViewContentModeScaleAspectFit;
	[self.view addSubview:self.photoImageView];
}

-(void)popViewControllerWithAnimation {
	[self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

@end
