//
//  FollowingFollowersCell.h
//  MYSCHEDULER
//
//  Created by John Setting on 9/8/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SCHProfileImageView;
@protocol FollowingFollowersCellDelegate;

@interface FollowingFollowersCell : UITableViewCell {
    id _delegate;
}
@property (nonatomic, strong) id<FollowingFollowersCellDelegate> delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier type:(NSString *)type;

@property (nonatomic, strong) PFUser *user;

@property (nonatomic, strong) UIButton *followButton;

@property (nonatomic, strong) UIButton *nameButton;
@property (nonatomic, strong) UIButton *avatarImageButton;
@property (nonatomic, strong) UIButton *descriptionButton;

@property (nonatomic, strong) SCHProfileImageView *avatarImageView;

/*! Setters for the cell's content */
- (void)setUser:(PFUser *)user;
+ (CGFloat)heightForCell;

@end

/*!
 The protocol defines methods a delegate of a PAPBaseTextCell should implement.
 */
@protocol FollowingFollowersCellDelegate <NSObject>
@optional

/*!
 Sent to the delegate when a user button is tapped
 @param aUser the PFUser of the user that was tapped
 */
- (void)cell:(FollowingFollowersCell *)cellView didTapUserButton:(PFUser *)aUser;
- (void)cell:(FollowingFollowersCell *)cellView didToggleFriendButton:(PFUser *)aUser;

@end
