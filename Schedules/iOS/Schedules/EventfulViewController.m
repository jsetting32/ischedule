//
//  EventViewController.m
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "EventfulViewController.h"
#import "EventfulEventCell.h"
#import "EventDataSource.h"
#import "EventfulEvent.h"
#import "SWRevealViewController.h"
#import "CalendarDataSource.h"

@interface EventfulViewController () <UITableViewDataSource, UITableViewDelegate, EventfulEventCellDelegate, EventfulDataSourceDelegate, EventfulEventDelegate, UINavigationControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic) NSArray *events;
@property (nonatomic) UITableView *eventTableView;
@property (nonatomic) CalendarDataSource *dataSource;
@property (nonatomic) UISearchBar *searchBar;
@property (nonatomic) UISearchDisplayController *searchController;
@end

@implementation EventfulViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupRevealController];
    
    self.dataSource = [CalendarDataSource sharedInstance];
    
    self.navigationController.delegate = self;
    
    UIView *combinedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    self.eventTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) style:UITableViewStylePlain];
    [self.eventTableView setDelegate:self];
    [self.eventTableView setDataSource:self];
    [self.eventTableView setShowsHorizontalScrollIndicator:NO];
    [self.eventTableView setShowsVerticalScrollIndicator:NO];
    [self.eventTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.eventTableView];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:self.view.frame];
    [self.searchBar setBarStyle:UIBarStyleDefault];
    [self.searchBar setShowsCancelButton:NO];
    [self.searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self.searchBar setDelegate:self];
    [combinedView addSubview:self.searchBar];
    
    UISearchDisplayController *searchCon = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.searchController = searchCon;
    [self.searchController setDelegate:self];
    [self.searchController setSearchResultsDataSource:self];
    [self.searchController setSearchResultsDelegate:self];
    [self.searchController setActive:NO animated:YES];

    UISegmentedControl *segmentedController = [[UISegmentedControl alloc] initWithItems:@[@"one", @"two", @"three"]];
    segmentedController.frame = CGRectMake(0, 44, 320, 20);
    segmentedController.selectedSegmentIndex = 1;
    [segmentedController addTarget:self action:@selector(:) forControlEvents:UIControlEventValueChanged];
    [combinedView addSubview:segmentedController];


    [self.eventTableView setTableHeaderView:combinedView];
    
    EventDataSource *eventDataSource = [EventDataSource sharedInstance];
    [eventDataSource setDelegate:self];
    
    //[eventDataSource getEventsByVenue:@"Sonoma State University" location:@"Rohnert Park, Ca"];
    //[eventDataSource getEventsByLocation:@"Rohnert Park"];
    [eventDataSource getEventsByCurrentLocation];
    
}


- (void)setupRevealController {
	self.title = NSLocalizedString(@"Front View", nil);
    
    SWRevealViewController *revealController = [self revealViewController];
    
    //[revealController panGestureRecognizer];
    //[revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    UIBarButtonItem *search = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(loadSearch)];
    self.navigationItem.rightBarButtonItems = @[rightRevealButtonItem, search];
}

- (void)loadSearch
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, -30, 320, 50)];
	[self.view addSubview:searchBar];
    [UIView animateWithDuration:.5 animations:^{
		
		//self.animationInProgress = true;
		[searchBar setFrame:CGRectMake(0, 20, 320, 50)];
	} completion:^(BOOL finished) {
        [searchBar setShowsCancelButton:YES animated:YES];
        //[searchBar setSearchResultsButtonSelected:YES];
        //self.animationInProgress = false;
	}];
    
    //UISearchDisplayController *search = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    //[self.navigationController presentViewController:search animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.events count];
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [EventfulEventCell heightForCell];
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) return 44.0f;
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ActivityCell";
    
    EventfulEventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[EventfulEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setDelegate:self];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    }
    
    EventfulEvent * Event = [[EventfulEvent alloc] init];
    [Event setEvent:[self.events objectAtIndex:[indexPath row]]];
    [cell setEventForCell:Event];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController *view = [[UIViewController alloc] init];
    EventfulEvent * Event = [[EventfulEvent alloc] init];
    [Event setEvent:[self.events objectAtIndex:[indexPath row]]];
    NSLog(@"%@", [self.dataSource returnConvertedEvent:Event]);
    [self.navigationController pushViewController:view animated:YES];
}

- (void)eventsDidFinishWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

- (void)eventsDidFinishLoading:(NSArray *)events {
    self.events = events;
    [self.eventTableView reloadData];
}

- (void)cell:(EventfulEventCell *)cellView didTapImageButton:(UIButton *)imageButton
{
    UIViewController *view = [[UIViewController alloc] init];
    [self.navigationController pushViewController:view animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
