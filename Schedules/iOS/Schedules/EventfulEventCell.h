//
//  EventfulEventCell.h
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EventfulImageView;
@class EventfulEvent;
@protocol EventfulEventCellDelegate;

@interface EventfulEventCell : UITableViewCell

@property (nonatomic, weak) id<EventfulEventCellDelegate> delegate;

// The event represented in the cell
@property (nonatomic) EventfulEvent *event;

// Setter for the cell's content
- (void)setEventForCell:(EventfulEvent *)aEvent;


- (void)didTapImageButtonAction:(id)sender;

// Static Helper method
+ (CGFloat)heightForCell;

@end



// The protocol defines methods a delegate of a EventfulEventCell should implement.
@protocol EventfulEventCellDelegate <NSObject>
@optional
- (void)cell:(EventfulEventCell *)cellView didTapImageButton:(UIButton *)imageButton;
@end