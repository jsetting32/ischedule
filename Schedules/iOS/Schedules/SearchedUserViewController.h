//
//  SearcedUserViewController.h
//  Schedules
//
//  Created by John Setting on 11/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "EventDetailController.h"
#import "EventsTableViewCell.h"
#import "CustomCellBackground.h"
#import "CustomFooterView.h"
#import "CustomHeaderView.h"

@protocol SearchedUserViewControllerDeletegate;

@interface SearchedUserViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, EventsTableViewCellDelegate>
{
	SWRevealViewController *revealController;
	
    id _delegate;
	PFUser *user;
    
    UIView *tableHeaderView;
    UITableView *userTableView;
	
    NSMutableArray *events;
    NSMutableDictionary *dict;
    NSMutableArray *sortedDays;
    
    UIView *labelView;
    
    UIButton *coverPicture;
	UIButton *profilePicture;
    UIButton *friendButton;
    
    NSString *bar;
}

@property (nonatomic, strong) id<SearchedUserViewControllerDeletegate> delegate;
@property (nonatomic, retain) PFUser *user;

@property (nonatomic, strong) UIView *tableHeaderView;
@property (nonatomic, strong) UITableView *userTableView;

@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) NSMutableArray *sortedDays;

@property (nonatomic, strong) UIView *labelView;

@property (nonatomic, strong) UIButton *coverPicture;
@property (nonatomic, strong) UIButton *profilePicture;
@property (nonatomic, strong) UIButton *friendButton;

@property (nonatomic, strong) NSString *bar;


@end
