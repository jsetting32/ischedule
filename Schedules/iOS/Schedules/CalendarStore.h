//
//  CalendarStore.h
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>
#import "SchedulesEvent.h"
#import <Parse/PF_MBProgressHUD.h>

@interface CalendarStore : NSObject
{
    PF_MBProgressHUD *HUD;
    NSMutableArray *events;
}

@property (nonatomic, strong) NSMutableArray *events;

+ (void)getEventsForSchedules:(PFObject *)object block:(void (^)(BOOL finished, NSError * error))completionBlock;

+ (void) assignEvent:(EKEvent *)event storageArray:(NSMutableArray *)storageArray;
+ (void) saveEvent:(SchedulesEvent *)event;
+ (void) handleEvents:(NSArray *)events storageArray:(NSMutableArray *)storageArray;

@end
