//
//  CustomHeaderView.h
//  MYSCHEDULER
//
//  Created by John Setting on 9/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "Common.h"

@interface CustomHeaderView : UIView

@property (nonatomic, strong) UILabel * leftTitleLabel;
@property (nonatomic, strong) UILabel * rightTitleLabel;

@property (nonatomic, strong) UIColor * lightColor;
@property (nonatomic, strong) UIColor * darkColor;

@property (nonatomic, assign) CGRect coloredBoxRect;
@property (nonatomic, assign) CGRect paperRect;

@end
