//
//  CalendarStore.m
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CalendarStore.h"

@implementation CalendarStore
@synthesize events;

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


+ (void)getEventsForSchedules:(PFObject *)object block:(void (^)(BOOL finished, NSError * error))completionBlock
{
    EKEventStore *store = [[EKEventStore alloc] init];
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (granted) {
            NSLog(@"Successfully accessed events");
            
            // Create the start date components
            NSDateComponents *beginning = [[NSDateComponents alloc] init];
            beginning.day = -1;
            NSDate *beginningDate = [[NSCalendar currentCalendar] dateByAddingComponents:beginning toDate:[NSDate date] options:0];
            
            // Create the end date components
            NSDateComponents *end = [[NSDateComponents alloc] init];
            end.year = 5;
            NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:end toDate:[NSDate date] options:0];
            
            // Create the predicate from the event store's instance method
            NSPredicate *predicate = [store predicateForEventsWithStartDate:beginningDate endDate:endDate calendars:nil];
            
            // Fetch all events that match the predicate
            NSArray *events = [store eventsMatchingPredicate:predicate];
            NSMutableArray *array = [NSMutableArray array];
            
            [self handleEvents:events storageArray:array];
            
        } else {
            NSLog(@"%@", error);
        }
    }];
}

+ (void) handleEvents:(NSArray *)events storageArray:(NSMutableArray *)storageArray {
    NSLog(@"running handleEvents:storageEvents;");
    for (EKEvent *event in events) {
        [self assignEvent:event storageArray:storageArray];
    }
    
    for (int i = 0; i < [events count]; i++)
    {
        [self saveEvent:[storageArray objectAtIndex:i]];
    }
}

+ (void) assignEvent:(EKEvent *)event storageArray:(NSMutableArray *)storageArray {
    NSLog(@"running assignEvent:storageArray;");
    SchedulesEvent *specialEvent = [[SchedulesEvent alloc] init];
    specialEvent.SeventId = [PFUser currentUser].objectId;
    specialEvent.Stitle = event.title;
    specialEvent.Slocation = event.location;
    specialEvent.Sdescription = event.description;
    specialEvent.ScalendarTitle = event.calendar.title;
    //specialEvent.ScalendarType = event.calendar.type;
    //specialEvent.ScalendarAllowsModify = event.calendar.allowsContentModifications;
    //specialEvent.ScalendarColor = (__bridge NSData *)(event.calendar.CGColor);
    //specialEvent.Salarms = event.alarms;
    specialEvent.SURL = [NSString stringWithFormat:@"%@", event.URL];
    //specialEvent.SlastModified = event.lastModifiedDate;
    specialEvent.StimeZone = [event.timeZone abbreviation];
    specialEvent.SstartDate = event.startDate;
    specialEvent.SendDate = event.endDate;
    specialEvent.SallDay = event.allDay;
    //specialEvent.floating = event.floating;
    //specialEvent.Srecurrence = event.recurrenceRules;
    //specialEvent.Sattendees = event.attendees;
    specialEvent.SeventIdentifier = event.eventIdentifier;
    [storageArray addObject:specialEvent];
}

+ (void) saveEvent:(SchedulesEvent *)event {
    NSLog(@"running saveEvent:event;");
    PFObject *eventObject = [PFObject objectWithClassName:@"Events"];
    
    if (!([event SeventId] == (id)[NSNull null] || [event SeventId].length == 0)){
        eventObject[@"eventId"] = [event SeventId];
    } else {
        eventObject[@"eventId"] = @"";
    }
    
    if (!([event Stitle] == (id)[NSNull null] || [event Stitle].length == 0)){
        eventObject[@"title"] = [event Stitle];
    } else {
        eventObject[@"title"] = @"";
    }
    
    if (!([event Sdescription] == (id)[NSNull null] || [event Sdescription].length == 0)){
        eventObject[@"description"] = [event Sdescription];
    } else {
        eventObject[@"description"] = @"";
    }
    
    if (!([event Slocation] == (id)[NSNull null] || [event Slocation].length == 0)){
        eventObject[@"location"] = [event Slocation];
    } else {
        eventObject[@"location"] = @"";
    }
    
    if (!([event ScalendarTitle] == (id)[NSNull null] || [event ScalendarTitle].length == 0)){
        eventObject[@"calendarTitle"] = [event ScalendarTitle];
    } else {
        eventObject[@"calendarTitle"] = @"";
    }
    
    
    /*
     if (!([[array objectAtIndex:i] ScalendarType] == (id)[NSNull null] || [[array objectAtIndex:i] ScalendarType].length == 0)){
     events[@"calendarType"] = [[array objectAtIndex:i] ScalendarType];
     } else {
     events[@"calendarType"] = @"";
     }
     NSLog(@"%@", events);
     */
    //if (![[[array objectAtIndex:i]ScalendarAllowsModify] isKindOfClass:[NSNull class]]){
    //events[@"calendarAllowsModifications"] = [[array objectAtIndex:i] ScalendarAllowsModify];
    //} else {
    //events[@"calendarAllowsModifications"] = @"";
    //}
    
    //events[@"calendarColor"] = [NSKeyedArchiver archivedDataWithRootObject:[[array objectAtIndex:i] ScalendarColor]];
    
    //if (!([[array objectAtIndex:i] Salarms] == nil || [[[array objectAtIndex:i] Salarms] count] == 0)) {
    //    eventObject[@"alarms"] = [[array objectAtIndex:i] Salarms];
    //} else {
    //    eventObject[@"alarms"] = @"";
    //}
    
    if (!([event SURL] == (id)[NSNull null])){
        eventObject[@"URL"] = [event SURL];
    } else {
        eventObject[@"URL"] = @"";
    }
    
    //if (!([[array objectAtIndex:i] SlastModified] == (id)[NSNull null])){
    //    events[@"lastModfied"] = [[array objectAtIndex:i] SlastModified];
    //} else {
    //    events[@"lastModfied"] = @"";
    //}
    
    if (!([event StimeZone] == (id)[NSNull null] || [event StimeZone].length == 0)){
        eventObject[@"timeZone"] = [event StimeZone];
    } else {
        eventObject[@"timeZone"] = @"";
    }
    
    if (!([event SstartDate] == (id)[NSNull null])){
        eventObject[@"startDate"] = [event SstartDate];
    } else {
        eventObject[@"startDate"] = @"";
    }
    
    if (!([event SendDate] == (id)[NSNull null])){
        eventObject[@"endDate"] = [event SendDate];
    } else {
        eventObject[@"endDate"] = @"";
    }
    
    eventObject[@"allDay"] = [NSNumber numberWithBool:[event SallDay]];
    
    //if (!([[array objectAtIndex:i] Srecurrence] == (id)[NSNull null])){
    //    events[@"recurrence"] = [[array objectAtIndex:i] Srecurrence];
    //} else {
    //    events[@"recurrence"] = @"";
    //}
    //NSLog(@"%@", events);
    
    //if (!([[array objectAtIndex:i] Sattendees] == (id)[NSNull null] || [[[array objectAtIndex:i] Sattendees] count] == 0)){
    //    eventObject[@"attendees"] = [[array objectAtIndex:i] Sattendees];
    //} else {
    //    eventObject[@"attendees"] = @"";
    //}
    
    if (!([event SeventIdentifier] == (id)[NSNull null] || [event SeventIdentifier].length == 0)){
        eventObject[@"eventIdentifier"] = [event SeventIdentifier];
    } else {
        eventObject[@"eventIdentifier"] = @"";
    }
    
    [eventObject save];
    NSLog(@"Saved Event");
}

@end
