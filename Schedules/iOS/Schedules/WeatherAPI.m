//
//  WeatherAPI.m
//  MyScheduler
//
//  Created by John Setting on 2/21/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "WeatherAPI.h"
#import "GeoLocation.h"

@interface WeatherAPI() <GeoLocationDelegate, NSURLConnectionDelegate>
@property (nonatomic) GeoLocation *location;
@property (nonatomic) NSMutableData *data;
@property (nonatomic) NSURLConnection *theConnection;
@property (nonatomic) float contentSize;
@end

@implementation WeatherAPI

#pragma mark - Initializer Methods
- (id)initWithCurrentLocation {
    if ((self = [super init]) == nil) return nil;
	
    self.location = [[GeoLocation alloc] init];
    [self.location setDelegate:self];
    [self.location.locationManager startUpdatingLocation];
    return self;
}

- (id)initWithCustomLocation:(NSString *)location {
    if ((self = [super init]) == nil) return nil;
    
    self.location = [[GeoLocation alloc] initWithCustomLocation:location];
    [self.location setDelegate:self];
    return self;
}

#pragma mark - GeoLocationDelegate Methods
- (void)WOEIDupdate:(id)WOEID
{
	NSString *query = [[NSString stringWithFormat:@"select * from weather.forecast where woeid=%@", WOEID] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://query.yahooapis.com/v1/public/yql?q=%@&format=json&diagnostics=true&callback=", query]];
	self.theConnection = [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
}

- (void)locationError:(NSError *)error
{
    NSLog(@"locationError:%@", error);
}


#pragma mark - NSURLConnectionDelegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	self.data = [[NSMutableData alloc] init];
	NSLog(@"Expected data length %lld", [response expectedContentLength]);
	self.contentSize = (float)[response expectedContentLength];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[self.data appendData:data];
	float progress = (float)[self.data length] / (float)self.contentSize;
	NSLog(@"Progress: %f", progress);
}


- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Succeeded! Received %lu bytes of data",(unsigned long)[self.data length]);
	NSError *error;
	NSDictionary *something = [NSJSONSerialization JSONObjectWithData:self.data options:kNilOptions error:&error];
    [self.delegate weatherUpdateDidFinish:[[[something objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"channel"]];
    self.theConnection = nil;
    self.data = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.theConnection = nil;
    self.data = nil;
    [self.delegate weatherUpdateDidFailWithError:error];
}


@end
