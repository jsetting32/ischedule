//
//  EventListTableViewCell.m
//  iSchedj
//
//  Created by John Setting on 3/20/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "ListTableViewCell.h"

@interface ListTableViewCell()
@property (nonatomic) UIView  *calendarColor;
@property (nonatomic) UILabel *startTime;
@property (nonatomic) UILabel *endTime;
@property (nonatomic) UILabel *eventTitleLabel;
@property (nonatomic) UILabel *eventLocation;
@property (nonatomic) UILabel *eventDescription;
@end

@implementation ListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDetailButton;
        
        self.eventTitleLabel = [[UILabel alloc] init];
        self.eventTitleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.eventTitleLabel.textAlignment = NSTextAlignmentLeft;
        self.eventTitleLabel.textColor = [UIColor blackColor];
        self.eventTitleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.eventTitleLabel];

        self.calendarColor = [[UIView alloc] init];
        [self.contentView addSubview:self.calendarColor];
        
        self.startTime = [[UILabel alloc] init];
        self.startTime.font = [UIFont systemFontOfSize:10.0f];
        self.startTime.textColor = [UIColor blackColor];
        self.startTime.textAlignment = NSTextAlignmentRight;
        self.startTime.backgroundColor = [UIColor clearColor];
        self.startTime.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.startTime.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.startTime];

        self.endTime = [[UILabel alloc] init];
        self.endTime.font = [UIFont systemFontOfSize:10.0f];
        self.endTime.textColor = [UIColor grayColor];
        self.endTime.textAlignment = NSTextAlignmentRight;
        self.endTime.backgroundColor = [UIColor clearColor];
        self.endTime.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.endTime.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.endTime];

        self.eventLocation = [[UILabel alloc] init];
        self.eventLocation.font = [UIFont systemFontOfSize:9.0f];
        self.eventLocation.textAlignment = NSTextAlignmentLeft;
        self.eventLocation.textColor = [UIColor grayColor];
        self.eventLocation.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.eventLocation];

        self.eventDescription = [[UILabel alloc] init];
        self.eventDescription.font = [UIFont systemFontOfSize:9.0f];
        self.eventDescription.textColor = [UIColor blackColor];
        self.eventDescription.textAlignment = NSTextAlignmentLeft;
        self.eventDescription.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.eventDescription];
    
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected)
        [self.delegate didTapEvent:self];
}

- (void)setEventForCell:(EKEvent *)aEvent {
    self.event = aEvent;
    
    [self.calendarColor setBackgroundColor:[UIColor colorWithCGColor:[[self.event calendar] CGColor]]];
    [self.calendarColor setFrame:CGRectMake(70, 3, 2, self.frame.size.height - 6)];

    [self.eventTitleLabel setText:[self.event title]];
    [self.eventTitleLabel setFrame:CGRectMake( 80.0f, 8.0f, 200.0f, 15.0f)];

    if (self.event.allDay) {
        [self.startTime setText:@"all-day"];
        [self.startTime setFrame:CGRectMake( 10.0f, 8.0f, 50.0f, 12.0f)];

        [self.endTime setText:@""];
        [self.endTime setFrame:CGRectMake( 10.0f, 25.0f, 50.0f, 12.0f)];
        
    } else {
        [self.startTime setText:[self modifiedDate:[self.event startDate]]];
        [self.startTime setFrame:CGRectMake( 10.0f, 8.0f, 50.0f, 12.0f)];
        
        [self.endTime setText:[self modifiedDate:[self.event endDate]]];
        [self.endTime setFrame:CGRectMake( 10.0f, 25.0f, 50.0f, 12.0f)];
    }
    
    
    if (![[aEvent location] isEqual:[NSNull null]]) {
        [self.eventLocation setText:[self.event location]];
        [self.eventLocation setFrame:CGRectMake( 80.0f, 25.0f, 200.0f, 12.0f)];
    } else if (![[aEvent description] isEqual:[NSNull null]])  {
        [self.eventDescription setText:[self.event description]];
        [self.eventDescription setFrame:CGRectMake( 80.0f, 25.0f, 200.0f, 12.0f)];
    }
    
}

- (NSString *)modifiedDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"h:mm a";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
    [dateFormatter setTimeZone:gmt];
    return [dateFormatter stringFromDate:date];
}


@end
