//
//  SearchedUserViewController.m
//  Schedules
//
//  Created by John Setting on 11/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SearchedUserViewController.h"

@interface SearchedUserViewController ()

@end

@implementation SearchedUserViewController
@synthesize user;

@synthesize userTableView;
@synthesize tableHeaderView;

@synthesize events;
@synthesize sortedDays;
@synthesize dict;

@synthesize labelView;

@synthesize coverPicture;
@synthesize profilePicture;
@synthesize friendButton;

@synthesize bar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.title = self.user[@"username"];
    
    self.userTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    self.userTableView.delegate = self;
    self.userTableView.dataSource = self;
    [self.view addSubview:self.userTableView];
    
    [self loadNavigationBarItems];
	[self createHeaderView];
    [self createGradientBackground];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self queryForUserEvents];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void) createGradientBackground
{
    UIColor *colorOne = [UIColor colorWithRed:(120/255.0) green:(135/255.0) blue:(150/255.0) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(57/255.0)  green:(79/255.0)  blue:(96/255.0)  alpha:1.0];
    CAGradientLayer *bgLayer = [GradientLayer gradientClash:colorOne colorTwo:colorTwo];
    bgLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:bgLayer atIndex:0];
}

- (void) loadNavigationBarItems
{
    if (![bar isEqualToString:@"pushed"]) {
        revealController = [self revealViewController];
        [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
        
        UIBarButtonItem *leftRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:revealController action:@selector(revealToggle:)];
        self.navigationItem.leftBarButtonItem = leftRevealButtonItem;
        
        UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:revealController action:@selector(rightRevealToggle:)];
        self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    }
}

- (void) createHeaderView
{
	tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 190)];
	tableHeaderView.backgroundColor = [UIColor blackColor];
    
    
	coverPicture = [UIButton buttonWithType:UIButtonTypeCustom];
	coverPicture.frame = CGRectMake(0, 0, 320, 150);
	[[coverPicture imageView] setContentMode:UIViewContentModeScaleAspectFill];
    [self.user[@"coverPicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!data) {
            [coverPicture setImage:[UIImage imageNamed:@"AvatarPlaceholder.png"] forState:UIControlStateNormal];
        } else {
            [coverPicture setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
        }
    }];
    [tableHeaderView addSubview:coverPicture];
	
	
	profilePicture = [UIButton buttonWithType:UIButtonTypeCustom];
	profilePicture.frame = CGRectMake(120, 35, 80, 80);
	[[profilePicture imageView] setContentMode:UIViewContentModeScaleAspectFill];
    [self.user[@"profilePicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!data) {
            [profilePicture setImage:[UIImage imageNamed:@"AvatarPlaceholder.png"] forState:UIControlStateNormal];
        } else {
            [profilePicture setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
        }
    }];
	profilePicture.layer.cornerRadius = 40.0f;
	profilePicture.layer.borderColor = [[UIColor whiteColor] CGColor];
	profilePicture.layer.borderWidth = 2.0f;
	profilePicture.layer.masksToBounds = YES;
    [tableHeaderView addSubview:profilePicture];
    
    
    [self initFriendButton];
    [self initLabels];
	
    self.userTableView.tableHeaderView = tableHeaderView;
}


- (void)initLabels {
    self.labelView = [[UIView alloc] initWithFrame:CGRectMake(5, 150, 260, 20)];
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 260, 20)];
    [name setText:[NSString stringWithFormat:@"%@ %@", self.user[@"firstName"], self.user[@"lastName"]]];
    [name setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [name setTextColor:[UIColor whiteColor]];
    [name setShadowColor:[UIColor grayColor]];
    [name setShadowOffset:CGSizeMake(-1.0f, 1.0f)];
    [self.labelView addSubview:name];
    
    UILabel *email = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 260, 20)];
    [email setText:self.user[@"email"]];
    [email setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [email setTextColor:[UIColor whiteColor]];
    [email setShadowColor:[UIColor grayColor]];
    [email setShadowOffset:CGSizeMake(-1.0f, 1.0f)];
    [self.labelView addSubview:email];

    [self.tableHeaderView addSubview:self.labelView];
}

- (void)initFriendButton {
    self.friendButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.friendButton.frame = CGRectMake(210, 155, 100, 30);
	[self.friendButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
	[self.friendButton setTitleEdgeInsets:UIEdgeInsetsMake( 0.0f, 10.0f, 0.0f, 0.0f )];
    [self.tableHeaderView addSubview:self.friendButton];

	if (![[self.user objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
        PFQuery *queryIsFriends = [PFQuery queryWithClassName:@"Activity"];
        [queryIsFriends whereKey:@"toUser" equalTo:self.user];
        [queryIsFriends whereKey:@"fromUser" equalTo:[PFUser currentUser]];
        [queryIsFriends getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                if ([[object objectForKey:@"type"] isEqualToString:@"friend"]) {
                    [self configureUnfriendedButton];
                } else if ([[object objectForKey:@"type"] isEqualToString:@"pending"]) {
                    [self configureRequestFriendButton];
                }
            } else {
                [self configureFriendButton];
            }
        }];
	} else {
		[[[UIAlertView alloc] initWithTitle:@"Viewing Self" message:@"Currently, your viewing your personal profile" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
	}
    
}

- (void)configureFriendButton {
    [self.friendButton setTitle:@"Add Friend " forState:UIControlStateNormal]; // space added for centering
    [[self.friendButton titleLabel] setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [self.friendButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollow.png"] forState:UIControlStateNormal];
    [self.friendButton setImage:nil forState:UIControlStateNormal];
    [self.friendButton setImage:nil forState:UIControlStateSelected];
    [self.friendButton setTitleColor:[UIColor colorWithRed:84.0f/255.0f green:57.0f/255.0f blue:45.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.friendButton setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.friendButton addTarget:self action:@selector(sendFriendRequestAction:) forControlEvents:UIControlEventTouchUpInside];
    
	[[Cache sharedCache] setFriendStatus:@"NO" user:self.user];

}

- (void)configureRequestFriendButton {
    [self.friendButton setTitle:@"Request Sent" forState:UIControlStateNormal]; // space added for centering
    [[self.friendButton titleLabel] setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [self.friendButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollow.png"] forState:UIControlStateNormal];
    [self.friendButton setImage:nil forState:UIControlStateNormal];
    [self.friendButton setImage:nil forState:UIControlStateSelected];
    [self.friendButton setTitleColor:[UIColor colorWithRed:84.0f/255.0f green:57.0f/255.0f blue:45.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.friendButton setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.friendButton addTarget:self action:@selector(removeFriendRequestAction:) forControlEvents:UIControlEventTouchUpInside];
    
	[[Cache sharedCache] setFriendStatus:@"pending" user:self.user];
}

- (void)configureUnfriendedButton {
    
	[self.friendButton setTitle:@"Friends  " forState:UIControlStateNormal]; // space added for centering
    [[self.friendButton titleLabel] setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [self.friendButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollowing.png"] forState:UIControlStateNormal];
    [self.friendButton setImage:[UIImage imageNamed:@"iconTick.png"] forState:UIControlStateNormal];
	[self.friendButton setImage:[UIImage imageNamed:@"iconTick.png"] forState:UIControlStateSelected];
	[self.friendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.friendButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.friendButton addTarget:self action:@selector(unfriendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
	[[Cache sharedCache] setFriendStatus:@"YES" user:self.user];
}

- (void)sendFriendRequestAction:(id)sender {
    [self configureRequestFriendButton];
    [Utility friendUserRequestInBackground:self.user block:^(BOOL succeeded, NSError *error) {
        if (error) {
            [self configureFriendButton];
        }
    }];
}

- (void)removeFriendRequestAction:(id)sender {
    [self configureFriendButton];
    [Utility unfriendUserRequestInBackground:self.user block:^(BOOL succeeded, NSError *error) {
        if (error) {
            [self configureRequestFriendButton];
        }
    }];
}

- (void)unfriendButtonAction:(id)sender {
	[self configureFriendButton];
    [Utility unfriendUserEventually:self.user];
}

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (void) queryForUserEvents {
    PFQuery *query = [PFQuery queryWithClassName:@"Events"];
    [query orderByAscending:@"startDate"];
    [query whereKey:@"eventId" equalTo:[self.user objectId]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        events = [NSMutableArray arrayWithArray:objects];
        dict = [NSMutableDictionary dictionary];
        
        for (SchedulesEvent *event in objects)
        {
            // Reduce event start date to date components (year, month, day)
            NSDate *dateRepresentingThisDay = [self dateAtBeginningOfDayForDate:[event valueForKey:@"startDate"]];
            
            // If we don't yet have an array to hold the events for this day, create one
            NSMutableArray *eventsOnThisDay = [dict objectForKey:dateRepresentingThisDay];
            if (eventsOnThisDay == nil) {
                eventsOnThisDay = [NSMutableArray array];
                
                // Use the reduced date as dictionary key to later retrieve the event list this day
                [dict setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
            }
            // Add the event to the list for this day
            [eventsOnThisDay addObject:event];
        }
        
        sortedDays = (NSMutableArray *)[[dict allKeys] sortedArrayUsingSelector:@selector(compare:)];
        
        [self.userTableView reloadData];
    }];
}

#pragma mark - Table view data source
// Add new methods
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CustomHeaderView* header = [[CustomHeaderView alloc] init];
    
    UIColor * light = [UIColor colorWithRed:105.0f/255.0f green:179.0f/255.0f blue:216.0f/255.0f alpha:1.0];
    UIColor *dark = [UIColor colorWithRed:21.0/255.0 green:92.0/255.0 blue:136.0/255.0 alpha:1.0];
    
    header.lightColor = light;
    header.darkColor = dark;
    
    NSDate *dateRepresentingThisDay = [sortedDays objectAtIndex:section];
	
    header.leftTitleLabel.text = [NSString stringWithFormat:@"%@ %@", [[self dateFormatter:dateRepresentingThisDay] objectAtIndex:2], [[self dateFormatter:dateRepresentingThisDay] objectAtIndex:3]];
	header.rightTitleLabel.text = [[self dateFormatter:dateRepresentingThisDay] objectAtIndex:1];
	
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[CustomFooterView alloc] init];
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 30;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [dict count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSDate *dateRepresentingThisDay = [sortedDays objectAtIndex:section];
    NSArray *eventsOnThisDay = [dict objectForKey:dateRepresentingThisDay];
    return [eventsOnThisDay count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"EventsTableViewCell";
	EventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	
	if (cell == nil) {
    	cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
	}
	
	[cell setDelegate:self];
	
	if (![cell.backgroundView isKindOfClass:[CustomCellBackground class]]) {
		CustomCellBackground *backgroundCell = [[CustomCellBackground alloc] init];
		cell.backgroundView = backgroundCell;
	}
	
	if (![cell.selectedBackgroundView isKindOfClass:[CustomCellBackground class]]) {
		CustomCellBackground *selectedBackgroundCell = [[CustomCellBackground alloc] init];
		selectedBackgroundCell.selected = YES;
		cell.selectedBackgroundView = selectedBackgroundCell;
	}
	
	((CustomCellBackground *) cell.backgroundView).lastCell = indexPath.section == dict.count - 1;
    ((CustomCellBackground *) cell.selectedBackgroundView).lastCell = indexPath.section == dict.count - 1;
    
    
	NSDate *dateRepresentingThisDay = [sortedDays objectAtIndex:indexPath.section];
    NSArray *eventsOnThisDay = [dict objectForKey:dateRepresentingThisDay];
	SchedulesEvent *event = [eventsOnThisDay objectAtIndex:indexPath.row];
	[cell setCellForEvent:event];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{


}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailController *view = [[EventDetailController alloc] init];
    view.event = [events objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:view animated:YES];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSArray *)dateFormatter:(NSDate *)date
{
	
	static NSDateFormatter *stringToDateFormatter = nil;
	if (nil == stringToDateFormatter) {
		stringToDateFormatter = [[NSDateFormatter alloc] init];
		[stringToDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	}
	
	static NSDateFormatter *dateToStringFormatter = nil;
	if (nil == dateToStringFormatter) {
		dateToStringFormatter = [[NSDateFormatter alloc] init];
		[dateToStringFormatter setDateFormat:@"hh:mm a"];
	}
	
	static NSDateFormatter *monthFormatter = nil;
	if (nil == monthFormatter) {
		monthFormatter = [[NSDateFormatter alloc] init];
		[monthFormatter setDateFormat:@"MMMM yyyy"];
	}
	
	static NSDateFormatter *dayFormatter = nil;
	if (nil == dayFormatter)
	{
		dayFormatter = [[NSDateFormatter alloc] init];
		[dayFormatter setDateFormat:@"dd"];
	}
	
	static NSDateFormatter *dayStringFormatter = nil;
	if (nil == dayStringFormatter)
	{
		dayStringFormatter = [[NSDateFormatter alloc] init];
		[dayStringFormatter setDateFormat:@"EEEE"];
	}
	
	NSArray *array = [[NSArray alloc] initWithObjects:[dateToStringFormatter stringFromDate:date], [monthFormatter stringFromDate:date], [dayFormatter stringFromDate:date], [dayStringFormatter stringFromDate:date], nil];
	
	return array;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
