//
//  UserTableViewCell.h
//  Schedules
//
//  Created by John Setting on 11/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SCHProfileImageView.h"

@class SCHProfileImageView;
@protocol UserTableViewCellDelegate;

@interface UserTableViewCell : UITableViewCell {
    id _delegate;
}
@property (nonatomic, strong) id<UserTableViewCellDelegate> delegate;

@property (nonatomic, strong) PFUser *user;

@property (nonatomic, strong) UIButton *followButton;

@property (nonatomic, strong) UIButton *nameButton;
@property (nonatomic, strong) UIButton *avatarImageButton;
@property (nonatomic, strong) UIButton *descriptionButton;

@property (nonatomic, strong) SCHProfileImageView *avatarImageView;

/*! Setters for the cell's content */
- (void)setUser:(PFUser *)user;
+ (CGFloat)heightForCell;

@end

/*!
 The protocol defines methods a delegate of a PAPBaseTextCell should implement.
 */
@protocol UserTableViewCellDelegate <NSObject>
@optional

/*!
 Sent to the delegate when a user button is tapped
 @param aUser the PFUser of the user that was tapped
 */
- (void)cell:(UserTableViewCell *)cellView didTapUserButton:(PFUser *)aUser;
- (void)cell:(UserTableViewCell *)cellView didTapFollowButton:(PFUser *)aUser;

@end
