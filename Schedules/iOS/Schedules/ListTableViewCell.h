//
//  EventListTableViewCell.h
//  iSchedj
//
//  Created by John Setting on 3/20/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
@protocol ListTableViewCellDelegate;
@interface ListTableViewCell : UITableViewCell
@property (nonatomic, weak) id <ListTableViewCellDelegate> delegate;
@property (nonatomic) EKEvent *event;
- (void)setEventForCell:(EKEvent *)aEvent;
@end
@protocol ListTableViewCellDelegate <NSObject>
- (void)didTapEvent:(ListTableViewCell *)cell;
@end
