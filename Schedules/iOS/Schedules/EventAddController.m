//
//  EventAddController.m
//  Schedules
//
//  Created by John Setting on 11/20/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "EventAddController.h"

@interface EventAddController ()

@end

@implementation EventAddController
@synthesize eventTableView;
@synthesize event;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(pop)];
    
    self.eventTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    self.eventTableView.delegate = self;
    self.eventTableView.dataSource = self;
    [self.view addSubview:self.eventTableView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void) pop {
    [self dismissViewControllerAnimated:YES completion:nil];
    self.eventTableView = nil;
    self.event = nil;
}


#pragma mark - Table view data source
/*
 -(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 return nil;
 }
 
 - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
 {
 return 50.0;
 }
 
 - (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
 {
 return nil;
 }
 
 -(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
 {
 return 15;
 }
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [eventTableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
	}
    
    
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = event[@"title"];
            } else if (indexPath.row == 1) {
                cell.textLabel.text = event[@"location"];
            }
        }
            break;
            
        case 1:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@", event[@"startDate"]];
            } else if (indexPath.row == 1) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@", event[@"endDate"]];
            }
        }
            break;
            
        case 2:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = event[@"calendarTitle"];
            } else if (indexPath.row == 1) {
                cell.textLabel.text = event[@"timeZone"];
            }
        }
            break;
        default:
            break;
    }
    
    
    
	return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
