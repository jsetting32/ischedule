//
//  FrapleCache.h
//  Fraple
//
//  Created by John Setting on 8/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cache : NSObject
@property (strong, nonatomic) NSCache *cache;

+ (id) sharedCache;
- (void)clear;

- (NSDictionary *)attributesForPhoto:(PFObject *)photo;
- (NSNumber *)likeCountForPhoto:(PFObject *)photo;
- (NSNumber *)commentCountForPhoto:(PFObject *)photo;
- (NSArray *)likersForPhoto:(PFObject *)photo;
- (NSArray *)commentersForPhoto:(PFObject *)photo;
- (void)setPhotoIsLikedByCurrentUser:(PFObject *)photo liked:(BOOL)liked;
- (BOOL)isPhotoLikedByCurrentUser:(PFObject *)photo;
- (void)incrementLikerCountForPhoto:(PFObject *)photo;
- (void)decrementLikerCountForPhoto:(PFObject *)photo;
- (void)incrementCommentCountForPhoto:(PFObject *)photo;
- (void)decrementCommentCountForPhoto:(PFObject *)photo;

- (NSDictionary *)attributesForUser:(PFUser *)user;
- (NSNumber *)photoCountForUser:(PFUser *)user;
- (BOOL)friendStatusForUser:(PFUser *)user;
- (void)setPhotoCount:(NSNumber *)count user:(PFUser *)user;
- (void)setFriendStatus:(NSString *)status user:(PFUser *)user;

- (void)setFriends:(NSArray *)friends;
- (NSArray *)userFriends;

- (void)setAttributes:(NSDictionary *)attributes forPhoto:(PFObject *)photo;
- (void)setAttributesForPhoto:(PFObject *)photo likers:(NSArray *)likers commenters:(NSArray *)commenters likedByCurrentUser:(BOOL)likedByCurrentUser;

- (NSDictionary *)attributesForVenue:(NSString *)venue;
- (void)setFollowStatus:(BOOL)following venueName:(NSString *)venueName;
- (void)setAttributes:(NSDictionary *)attributes forVenue:(NSString *)venue;

@end
