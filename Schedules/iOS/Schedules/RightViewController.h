//
//  RightViewController.h
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "ProfileViewController.h"
#import "SearchedUserViewController.h"

@interface RightViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate>
{
}

@end
