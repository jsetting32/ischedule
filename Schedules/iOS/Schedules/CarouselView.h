//
//  CarouselView.h
//  iSchedj
//
//  Created by John Setting on 3/22/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CarouselViewDelegate;
@interface CarouselView : UIView {
    int currIndex;
}

@property (nonatomic, weak) id <CarouselViewDelegate> delegate;


/*
 * setFullDateLabel:
 * Precondition:
 * Postcondition:
 */
- (void)setFullDateLabel:(NSDate *)date;

/* 
 * setWeek:withDate:
 * Precondition: 
 *  1st Parameter: An array of arrays of days with a list of info for each day.
                   The inner array contains the day of the week in a string format,
                   the day of the week in a number format, and the full NSDate of the day.
    2nd Parameter: The NSDate of the currently already selected day
 *
 * Postcondition: Returns success when the data has been loaded into the view
 *
 */
- (BOOL)setWeek:(NSArray *)days withDate:(NSDate *)date;

/*
 * setOtherWeek:withDate:
 * Precondition:
 *  1st Parameter: An array of arrays of days with a list of info for each day.
 *                 The inner array contains the day of the week in a string format,
 *                 the day of the week in a number format, and the full NSDate of the day.
 *  2nd Parameter: The NSDate of the currently already selected day
 *
 * Postcondition: Returns success when the data has been loaded into the view
 *
 */
- (BOOL)setOtherWeek:(NSArray *)days withDate:(NSDate *)date;
@end

@protocol CarouselViewDelegate <NSObject>
// Returns to the delegate two objects
// array[0] = the text of the button pressed (ex. 25)
// array[1] = an enum of the days of the week the button lies on (ex. 0 for sunday)
- (void)didTapDateButton:(NSArray *)dayInfo;
- (void)didSwipeLeft:(UISwipeGestureRecognizer *)sender;
- (void)didSwipeRight:(UISwipeGestureRecognizer *)sender;
@end
