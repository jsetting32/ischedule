//
//  GeoLocation.h
//  MyScheduler
//
//  Created by John Setting on 2/21/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

/* 
 * Whoever conforms to the delegate of this class will be returned the WOEID
 * for a set location or current location. Meant to handle the connection made
 * to and from Yahoo! Weather API to get a WOEID.
 * This class will be instantiated in the WeatherAPI class to make the WOEID useful.
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol GeoLocationDelegate;

@interface GeoLocation : NSObject <CLLocationManagerDelegate>
@property (weak, nonatomic) id <GeoLocationDelegate> delegate;
@property (nonatomic) CLLocationManager *locationManager;

// Loads the users current location and once 'self'
// is instantiated, the fetch to Yahoo! weather will commence
// then return, who conforms to the delegate, the WOEID
- (id)init;

// Loads the passed by location. Both following methods need to be used simultaneously.
// You can instantiate GeoLocation with initWithCustomLocation:(NSString *)location but
// it will not return the WOEID unless the -(void)getWOEIDFromLocation method is called.
- (id)initWithCustomLocation:(NSString *)location;
- (void)getWOEIDFromLocation:(NSString *)location;
@end

@protocol GeoLocationDelegate
@optional
- (void)WOEIDupdate:(id)WOEID;
- (void)locationError:(NSError *)error;
- (void)didReceivePlacemark:(CLPlacemark *)placemark;
@end

