//
//  TutorialViewController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    CycleScrollView *csView = [[CycleScrollView alloc] initWithFrame:self.view.bounds];
    csView.delegate = self;
    csView.datasource = self;
    [self.view addSubview:csView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfPages
{
    return 5;
}

- (UIView *)pageAtIndex:(NSInteger)index
{
    UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(view.bounds.origin.x, view.bounds.origin.y + 100, view.bounds.size.width, 100)];
    [label setFont:[UIFont fontWithName:@"Chalkduster" size:40.0f]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor blackColor]];
    [label setShadowOffset:CGSizeMake(1.0f, 2.0f)];
    [label setShadowColor:[UIColor yellowColor]];
    [view addSubview:label];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(view.bounds.origin.x + 20, view.bounds.origin.y + 200, view.bounds.size.width - 40, view.bounds.size.height - 300)];
    [image setContentMode:UIViewContentModeScaleAspectFill];
	image.layer.cornerRadius = 40.0f;
	image.layer.borderColor = [[UIColor whiteColor] CGColor];
	image.layer.borderWidth = 2.0f;
	image.layer.masksToBounds = YES;
    [view addSubview:image];
    
    if (index == 0) {
        [image setImage:[UIImage imageNamed:@"ios7_full.png"]];
        [label setText:@"1"];
    } else if (index == 1) {
        [image setImage:[UIImage imageNamed:@"ios7_full.png"]];
        [label setText:@"2"];
    } else if (index == 2) {
        [image setImage:[UIImage imageNamed:@"ios7_full.png"]];
        [label setText:@"3"];
    } else if (index == 3) {
        [image setImage:[UIImage imageNamed:@"ios7_full.png"]];
        [label setText:@"4"];
    } else if (index == 4) {
        [image setImage:[UIImage imageNamed:@"ios7_full.png"]];
        [label setText:@"5"];
    }
    
    return view;
}

- (void)didClickPage:(CycleScrollView *)csView atIndex:(NSInteger)index
{
    [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%ld", (long)index+1] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
