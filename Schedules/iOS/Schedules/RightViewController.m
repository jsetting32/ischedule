//
//  LeftViewController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "RightViewController.h"

@interface RightViewController ()
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, retain) UITableView *mainTableView;
@property (nonatomic, retain) UISearchDisplayController *searchController;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property BOOL wasShownForFirstTime;
@end

@implementation RightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        [self.view setFrame:CGRectMake(60, 0, self.view.frame.size.width - 60, self.view.frame.size.height)];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, 20, self.view.frame.size.width, self.view.frame.size.height - 20) style:UITableViewStylePlain];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setShowsHorizontalScrollIndicator:NO];
        [_mainTableView setShowsVerticalScrollIndicator:NO];
        [_mainTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_mainTableView];
        
        _searchBar = [[UISearchBar alloc] init];
        [_searchBar setBarStyle:UIBarStyleDefault];
        [_searchBar setShowsCancelButton:NO];
        [_searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
        [_searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [_searchBar setDelegate:self];
        
        UISearchDisplayController *searchCon = [[UISearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
        _searchController = searchCon;
        [_searchController setDelegate:self];
        [_searchController setSearchResultsDataSource:self];
        [_searchController setSearchResultsDelegate:self];
        [_searchController setActive:NO animated:YES];
        _mainTableView.tableHeaderView = _searchBar;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.wasShownForFirstTime = YES;
    
    PFQuery *friendsQuery = [PFUser query];
    [friendsQuery whereKey:kSCHUserIDKey containedIn:[[Cache sharedCache] userFriends]];
    friendsQuery.cachePolicy = kPFCachePolicyNetworkOnly;
    [friendsQuery orderByAscending:kSCHUserDisplayNameKey];
    [friendsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error){
            self.friends = [NSMutableArray arrayWithArray:objects];
            [self.mainTableView reloadData];
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Loading Friends Error!" message:@"Apparently, the connection to the backend was not successful. Please try again later." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}




#pragma mark - UITableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.mainTableView) {
        switch (section) {
            case 0:
                return [self.friends count];
            case 1:
                break;
            case 2:
                break;
            default:
                break;
        }
    } else if (tableView == self.searchDisplayController.searchResultsTableView) {
        return self.searchResults.count;
    }

    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableView == self.mainTableView) {
        return 1;
    } else if(tableView == self.searchDisplayController.searchResultsTableView){
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        [[cell textLabel] setTextAlignment:NSTextAlignmentRight];
	}
	
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        PFUser *obj2 = [self.searchResults objectAtIndex:indexPath.row];
        
        if (obj2[@"firstName"] && obj2[@"lastName"]) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", obj2[@"firstName"], obj2[@"lastName"]];
            cell.detailTextLabel.text = obj2[@"username"];
        } else {
            cell.textLabel.text = obj2[@"username"];
        }
        
        [obj2[@"profilePicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!data) {
                cell.imageView.image = [UIImage imageNamed:@"AvatarPlaceholder.png"];
            } else {
                cell.imageView.image = [UIImage imageWithData:data];
            }
            [self.searchDisplayController.searchResultsTableView reloadData];
        }];
    } else if (tableView == self.mainTableView) {
        
        switch (indexPath.section) {
                
            case 0:
                if (row == 0) {
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", [[self.friends objectAtIndex:indexPath.row] objectForKey:@"firstName"], [[self.friends objectAtIndex:indexPath.row] objectForKey:@"lastName"]];
                } else if (row == 1) {
                } else if (row == 2) {
                }
                break;
                
            case 1:
                break;
                
            case 2:
                if (row == 0) {
                } else if (row == 1) {
                } else if (row == 2) {
                }
                break;
                
            default:
                break;
        }
    }
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController = self.revealViewController;
    NSInteger row = indexPath.row;
    
    if (tableView == self.searchController.searchResultsTableView) {
        SearchedUserViewController *searchUserProfile = [[SearchedUserViewController alloc] init];
        searchUserProfile.user = [self.searchResults objectAtIndex:row];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchUserProfile];
        [revealController setFrontViewController:navigationController animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else if (tableView == self.mainTableView) {
        
        switch (indexPath.section) {
            case 0:
                if (row == 0)
                {
                    [PF_MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [[PFUser query] getObjectInBackgroundWithId:[[self.friends objectAtIndex:indexPath.row] objectId] block:^(PFObject *object, NSError *error) {
                        if (!error) {
                            SearchedUserViewController *friend = [[SearchedUserViewController alloc] init];
                            friend.user = [self.friends objectAtIndex:indexPath.row];
                            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:friend];
                            [revealController setFrontViewController:navigationController animated:NO];
                            [revealController rightRevealToggle:self];
                        } else {
                            NSLog(@"%@", error);
                        }
                        [PF_MBProgressHUD hideHUDForView:self.view animated:YES];
                    }];
                } else if (row == 1) {
                } else if (row == 2) {
                }
                
                break;
            case 1:
                break;
            case 2:
                break;
            default:
                break;
        }
    }
    [self.mainTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 20)];
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 70, 20)];
	[tempLabel setTextAlignment:NSTextAlignmentRight];
    
    switch (section)
    {
        case 0:
            tempLabel.text = [NSString stringWithFormat:@"%@ (%i)", NSLocalizedString(@"Friends", @"Friends"), [self.friends count]];
            [tempView addSubview:tempLabel];
            break;
        case 1:
            break;
        case 2:
            break;
    }
    
    return tempView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerheight = 20.0;
    
    switch (section)
    {
        case 0: { headerheight = 22.0; } break;
        case 1: { headerheight = 22.0; } break;
        case 2: { headerheight = 22.0; } break;
    }
    return headerheight;
}

#pragma mark - UISearchBar Delegate Methods
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}


#pragma mark - UISearchDisplayController Delegate Methods

- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView
{
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
{
    
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""])
        return NO;
    
    PFQuery *friends = [PFQuery queryWithClassName:@"Activity"];
    [friends whereKey:@"type" equalTo:@"friend"];
    [friends whereKey:@"fromUser" equalTo:[PFUser currentUser]];
    
    PFQuery *friended = [PFQuery queryWithClassName:@"Activity"];
    [friended whereKey:@"type" equalTo:@"friend"];
    [friended whereKey:@"toUser" equalTo:[PFUser currentUser]];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:@[friended, friends]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (!error) {
            for (PFObject *object in results) {
                if ([[[object valueForKey:@"fromUser"] objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                    PFQuery *user = [PFUser query];
                    [user getObjectInBackgroundWithId:[[object valueForKey:@"toUser"] objectId] block:^(PFObject *object, NSError *error) {
                        if (!error) {
                            [self.searchResults removeAllObjects];
                            [self.searchResults addObject:object];
                            [self.searchDisplayController.searchResultsTableView reloadData];
                        } else {
                            [[[UIAlertView alloc] initWithTitle:@"Failed Search" message:[NSString stringWithFormat:@"%@", [error userInfo]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                        }

                        
                    }];
                } else if ([[[object valueForKey:@"toUser"] objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                    PFQuery *user = [PFUser query];
                    [user getObjectInBackgroundWithId:[[object valueForKey:@"fromUser"] objectId] block:^(PFObject *object, NSError *error) {
                        if (!error) {
                            [self.searchResults removeAllObjects];
                            [self.searchResults addObject:object];
                            [self.searchDisplayController.searchResultsTableView reloadData];
                        } else {
                            [[[UIAlertView alloc] initWithTitle:@"Failed Search" message:[NSString stringWithFormat:@"%@", [error userInfo]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                        }

                        
                    }];
                }
            }
        }
    }];
    
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{

}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView
{

}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    SWRevealViewController *revealController = self.revealViewController;
    self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    self.mainTableView.frame = CGRectMake(0, 20, 320, self.view.frame.size.height);
    [revealController setFrontViewPosition:FrontViewPositionLeftSideMostRemoved animated:YES];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    SWRevealViewController *revealController = self.revealViewController;
    self.view.frame = CGRectMake(0, 0, 260, self.view.frame.size.height);
    self.mainTableView.frame = CGRectMake(60, 20, 260, self.view.frame.size.height);
    [revealController setFrontViewPosition:FrontViewPositionLeftSide animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
