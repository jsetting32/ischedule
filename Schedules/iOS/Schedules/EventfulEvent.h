//
//  EventfulEvent.h
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol EventfulEventDelegate;

@interface EventfulEvent : NSObject

@property (nonatomic, weak) id<EventfulEventDelegate> delegate;

@property (nonatomic) NSInteger allDay;
@property (nonatomic) NSInteger calendarCount;
@property (nonatomic) id calendars;
@property (nonatomic) NSString * cityName;
@property (nonatomic) NSInteger commentCount;
@property (nonatomic) NSString * countryAbbr1;
@property (nonatomic) NSString * countryAbbr2;
@property (nonatomic) NSString * countryName;
@property (nonatomic) NSString * createdDate;
@property (nonatomic) NSString * description;
@property (nonatomic) NSString * geoCode;
@property (nonatomic) id going;
@property (nonatomic) NSInteger goingCount;
@property (nonatomic) NSArray *groups;
@property (nonatomic) NSString * idEvent;
@property (nonatomic) NSDictionary *image;
@property (nonatomic) float latitude;
@property (nonatomic) NSInteger linkCount;
@property (nonatomic) float longitude;
@property (nonatomic) NSString *modifiedDate;
@property (nonatomic) NSString *owner;
@property (nonatomic) NSArray *performers;
@property (nonatomic) NSInteger postalCode;
@property (nonatomic) NSInteger privacy;
@property (nonatomic) NSString *recurrenceString;
@property (nonatomic) NSString *regionAbbr;
@property (nonatomic) NSString *regionName;
@property (nonatomic) NSString *startTime;
@property (nonatomic) NSString *stopTime;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *tzCity;
@property (nonatomic) NSString *tzCountry;
@property (nonatomic) NSString *tzId;
@property (nonatomic) NSString *tzOlsenPath;
@property (nonatomic) NSURL *url;
@property (nonatomic) NSString *venueAddress;
@property (nonatomic) NSString *venueDisplay;
@property (nonatomic) NSString *venueId;
@property (nonatomic) NSString *venueName;
@property (nonatomic) NSURL *venueURL;
@property (nonatomic) NSInteger watchingCount;

- (void)setEvent:(NSDictionary *)event;
- (void)eventImage:(NSDictionary *)image;
@end

@protocol EventfulEventDelegate <NSObject>
@optional
-(void)didFinishLoadingImageData:(UIImage *)image;
@end
