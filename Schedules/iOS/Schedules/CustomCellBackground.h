//
//  CustomCellBackground.h
//  MYSCHEDULER
//
//  Created by John Setting on 9/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "Common.h"

@interface CustomCellBackground : UIView

@property (nonatomic, assign) BOOL lastCell;
@property (nonatomic, assign) BOOL selected;

@end
