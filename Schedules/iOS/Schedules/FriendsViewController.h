//
//  FriendsViewController.h
//  Schedules
//
//  Created by John Setting on 11/15/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchedUserViewController.h"

@interface FriendsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) UITableView *friendTableView;
@property (nonatomic, retain) NSMutableArray *friendsArray;

@end
