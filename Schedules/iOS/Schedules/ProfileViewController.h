//
//  ProfileViewController.h
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "PhotoDetailViewController.h"
#import <Parse/PF_MBProgressHUD.h>
#import "AFNetworking.h"
#import "EventDetailController.h"
#import "EventsTableViewCell.h"
#import "CustomCellBackground.h"
#import "CustomFooterView.h"
#import "CustomHeaderView.h"
#import "FriendsViewController.h"
#import "EventDetailController.h"
#import "EventAddController.h"

#define PADDING_TOP 0 // For placing the images nicely in the grid
#define PADDING 4
#define THUMBNAIL_COLS 4
#define THUMBNAIL_WIDTH 75
#define THUMBNAIL_HEIGHT 75

@interface ProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EventsTableViewCellDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>
{
    SWRevealViewController *mainController;

	UIButton *profilePicture;
	UIButton *coverPicture;
    
    UIButton *friendsmade;
	UIButton *sentrequests;
	UIButton *friendrequests;
    
    BOOL configuredView;
    
    UIView *headerView;
    UITableView *profileTableView;
    
    /* Datasources for Events */
    NSMutableArray *events;
    NSMutableDictionary *dict;
    NSMutableArray *sortedDays;
    
    UIRefreshControl *refreshControl;
    
    NSString *whichPhotoDidUserClickToEdit;
    
    UITapGestureRecognizer *tapRecognizer;

    
}

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UITableView *profileTableView;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) NSMutableArray *sortedDays;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end
