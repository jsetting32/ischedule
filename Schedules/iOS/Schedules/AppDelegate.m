//
//  AppDelegate.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "AppDelegate.h"
#import "DayViewController.h"
#import "LeftViewController.h"
#import "RightViewController.h"
#import "SWRevealViewController.h"
#import "LoginTabBarController.h"
#import "Reachability.h"

@interface AppDelegate () <SWRevealViewControllerDelegate>
@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;
@property (nonatomic, strong) SWRevealViewController *viewController;
@property (nonatomic, retain) DayViewController *mainViewController;
@property (nonatomic, retain) LeftViewController *leftViewController;
@property (nonatomic, retain) RightViewController *rightViewController;
@property (nonatomic, readonly) int networkStatus;
@end

@implementation AppDelegate

@synthesize mainViewController;
@synthesize rightViewController;
@synthesize leftViewController;

@synthesize networkStatus;
@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // ****************************************************************************
    // Parse initialization
    [Parse setApplicationId:ParseAppId clientKey:ParseClientKey];
    [PFFacebookUtils initializeFacebook];
    [PFTwitterUtils initializeWithConsumerKey:TwitterConsumerKey consumerSecret:TwitterSecretKey];
    // ****************************************************************************
    
    // Used to track statistics around application opens
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Register for push notifications
    [application registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
    
    // Set up our app's global UIAppearance
    [self setupAppearance];
    
    // Use Reachability to monitor connectivity
    [self monitorReachability];    
    
    
    // Process of Left/Top/Right View Setup
	self.mainViewController = [[DayViewController alloc] init];
    self.leftViewController = [[LeftViewController alloc] init];
	UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:self.mainViewController];
	self.viewController = [[SWRevealViewController alloc] initWithRearViewController:self.leftViewController frontViewController:frontNavigationController];
    self.viewController.delegate = self;
    self.rightViewController = [[RightViewController alloc] init];
    self.viewController.rightViewController = self.rightViewController;
	
	self.window.rootViewController = self.viewController;
	[self.window makeKeyAndVisible];
    
    // Handle Push Notification App Open
    [self handlePush:launchOptions];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    // If your app is already running when the notification is received, the data is made available in the application:didReceiveRemoteNotification:fetchCompletionHandler: method through the userInfo dictionary.

    NSLog(@"AppDelegate:application:didReceiveRemoteNotification:fetchCompletionHandler: %@", userInfo);
    // Create empty photo object
    /*
    NSString *photoId = [userInfo objectForKey:@"p"];
    PFObject *targetPhoto = [PFObject objectWithoutDataWithClassName:@"Photo" objectId:photoId];
    
    // Fetch photo object
    [targetPhoto fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        // Show photo view controller
        if (error) {
            handler(UIBackgroundFetchResultFailed);
        } else if ([PFUser currentUser]) {
            //PhotoVC *viewController = [[PhotoVC alloc] initWithPhoto:object];
            //[self.navController pushViewController:viewController animated:YES];
            handler(UIBackgroundFetchResultNewData);
        } else {
            handler(UIBackgroundFetchResultNoData);
        }
    }];
    */
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	if ([error code] != 3010) { // 3010 is for the iPhone Simulator
        NSLog(@"Application failed to register for push notifications: %@", error);
	}
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
}

- (BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [FBAppCall handleOpenURL:url sourceApplication:nil withSession:nil];
}

- (BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url sourceApplication:nil withSession:nil];
}

- (void)handlePush:(NSDictionary *)launchOptions {
    
    NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    NSLog(@"AppDelegate:handlePush:launchOptions: %@", notificationPayload);
    
    // Create a pointer to the Object
    /*
    NSString *objectId = [notificationPayload objectForKey:@"objectInfo"];
    PFObject *targetPhoto = [PFObject objectWithoutDataWithClassName:@"User" objectId:objectId];
    
    // Fetch photo object
    [targetPhoto fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        // Show photo view controller
        if (!error && [PFUser currentUser]) {
            //PhotoVC *viewController = [[PhotoVC alloc] initWithPhoto:object];
            //[self.navController pushViewController:viewController animated:YES];
        }
    }];
    */
    /*
     
    // If the app was launched in response to a push notification, we'll handle the payload here
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SCHAppDelegateApplicationDidReceiveRemoteNotification object:nil userInfo:remoteNotificationPayload];
        
        if (![PFUser currentUser]) {
            return;
        }
        
        // If the push notification payload references a photo, we will attempt to push this view controller into view
        NSString *photoObjectId = [remoteNotificationPayload objectForKey:kSCHPushPayloadPhotoObjectIdKey];
        if (photoObjectId && photoObjectId.length > 0) {
            //[self shouldNavigateToPhoto:[PFObject objectWithoutDataWithClassName:kSCHPhotoClassKey objectId:photoObjectId]];
            return;
        }
        
        // If the push notification payload references a user, we will attempt to push their profile into view
        NSString *fromObjectId = [remoteNotificationPayload objectForKey:kSCHPushPayloadFromUserObjectIdKey];
        if (fromObjectId && fromObjectId.length > 0) {
            PFQuery *query = [PFUser query];
            query.cachePolicy = kPFCachePolicyCacheElseNetwork;
            [query getObjectInBackgroundWithId:fromObjectId block:^(PFObject *user, NSError *error) {
                if (!error) {
                    //UINavigationController *homeNavigationController = self.tabBarController.viewControllers[PAPHomeTabBarItemIndex];
                    //self.tabBarController.selectedViewController = homeNavigationController;
                    
                    //PAPAccountViewController *accountViewController = [[PAPAccountViewController alloc] initWithStyle:UITableViewStylePlain];
                    //accountViewController.user = (PFUser *)user;
                    //[homeNavigationController pushViewController:accountViewController animated:YES];
                }
            }];
        }
    }
    
    */
}

#pragma mark - App Appearance
- (void) setupAppearance {
    
    // Change the Status Bar Style
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSShadow *shadow = [NSShadow new];
    [shadow setShadowColor: [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.75f]];
    [shadow setShadowOffset: CGSizeMake(0.0f, 1.0f)];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{
       NSForegroundColorAttributeName:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
       NSShadowAttributeName: shadow,
       NSFontAttributeName: [UIFont fontWithName:@"Arial-Bold" size:0.0]
       }
     ];
    
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.498f green:0.388f blue:0.329f alpha:1.0f]];
    
    //[[UIButton appearanceWhenContainedIn:[UINavigationBar class], nil] setBackgroundImage:[UIImage imageNamed:@"ButtonNavigationBar.png"] forState:UIControlStateNormal];
    //[[UIButton appearanceWhenContainedIn:[UINavigationBar class], nil] setBackgroundImage:[UIImage imageNamed:@"ButtonNavigationBarSelected.png"] forState:UIControlStateHighlighted];
    [[UIButton appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleColor:[UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    //[[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage imageNamed:@"ButtonBack.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    //[[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage imageNamed:@"ButtonBackSelected.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:
     @{
       NSForegroundColorAttributeName: [UIColor colorWithRed:214.0f/255.0f green:210.0f/255.0f blue:197.0f/255.0f alpha:1.0f],
       NSShadowAttributeName: shadow,
       } forState:UIControlStateNormal];
    
    //[[UISearchBar appearance] setTintColor:[UIColor colorWithRed:32.0f/255.0f green:19.0f/255.0f blue:16.0f/255.0f alpha:1.0f]];
    //[[UISearchBar appearance] setTintColor:[UIColor whiteColor]];
}

#pragma mark - Reachability

- (BOOL)isParseReachable {
    return self.networkStatus != NotReachable;
}

- (void)monitorReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.hostReach = [Reachability reachabilityWithHostName:@"api.parse.com"];
    [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}

// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note {
    Reachability *curReach = (Reachability *)[note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    
    networkStatus = [curReach currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
    }
    
    if ([self isParseReachable] && [PFUser currentUser] && self.mainViewController.objects.count == 0) {
        // Refresh home timeline on network restoration. Takes care of a freshly installed app that failed to load the main timeline under bad network conditions.
        // In this case, they'd see the empty timeline placeholder and have no way of refreshing the timeline unless they followed someone.
        [self.mainViewController loadObjects];
    }
}

#pragma mark - User Logout
- (void) logout
{
    // clear cache
    [[Cache sharedCache] clear];
    
    // clear NSUserDefaults
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSCHUserDefaultsCacheFriendsKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSCHUserDefaultsActivityFeedViewControllerLastRefreshKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
	
    // Unsubscribe from push notifications by removing the user association from the current installation.
    [[PFInstallation currentInstallation] removeObjectForKey:kSCHInstallationUserKey];
    [[PFInstallation currentInstallation] saveInBackground];
    
    // Clear all caches
    [PFQuery clearAllCachedResults];
    
    // Log out
    [PFUser logOut];
    
    // clear out cached data, view controllers, etc
    self.mainViewController = nil;
    self.rightViewController = nil;
    self.leftViewController = nil;
    
    [self presentLoginViewController:YES];
}

- (void) presentLoginViewController:(BOOL)animated {
    LoginTabBarController *loginViewController = [[LoginTabBarController alloc] init];
    [self.viewController presentViewController:loginViewController animated:animated completion:^{
        // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
        self.mainViewController = [[DayViewController alloc] init];
        self.leftViewController = [[LeftViewController alloc] init];
        self.rightViewController = [[RightViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.mainViewController];
        [self.viewController setFrontViewController:navigationController animated:animated];
        [self.viewController setRearViewController:self.leftViewController];
        [self.viewController setRightViewController:self.rightViewController];
    }];
}

@end
