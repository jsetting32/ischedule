//
//  WeatherAPI.h
//  MyScheduler
//
//  Created by John Setting on 2/21/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol WeatherAPIDelegate;
@interface WeatherAPI : NSObject
@property (weak, nonatomic) id <WeatherAPIDelegate> delegate;
- (id)initWithCurrentLocation;
- (id)initWithCustomLocation:(NSString *)location;
@end

@protocol WeatherAPIDelegate
@required
- (void)weatherUpdateDidFinish:(NSDictionary *)weather;
- (void)weatherUpdateDidFailWithError:(NSError *)error;
@end