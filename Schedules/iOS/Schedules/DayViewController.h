//
//  DayViewController.h
//  iSchedj
//
//  Created by John Setting on 3/10/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DayViewCell.h"

@protocol DayViewControllerDelegate;
@interface DayViewController : UIViewController
@property (nonatomic, weak)id<DayViewControllerDelegate>delegate;
@property (nonatomic, strong) NSMutableArray *objects;
- (void) loadObjects;
@end

@protocol DayViewControllerDelegate <NSObject>
- (void)didTapNavigationButtonChangeToList:(id)sender;
@end