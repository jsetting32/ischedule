//
//  DayView.m
//  iSchedj
//
//  Created by John Setting on 3/10/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "DayView.h"
#import "DayViewCell.h"
#import "CalendarDataSource.h"

@interface DayView() <DayViewCellDelegate>
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIScrollView *dateView;
@property (nonatomic) NSMutableArray *events;
@property (nonatomic) UIView *timeLineLabel;
@property (nonatomic) UILabel *timeLineTimeLabel;
@end

@implementation DayView
static BOOL debug = TRUE;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [self loadScrollView:rect];
    [self drawLines];
    [self drawLabels:[UIFont fontWithName:@"Arial" size:10.0f]];
    [self addGesture];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.scrollView setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y + 70, self.frame.size.width, self.frame.size.height - 70)];
}

- (void)loadScrollView:(CGRect)rect
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setContentSize:CGSizeMake(320, 1465)];
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.scrollView];
}

- (void)drawLines
{
    for (int i = 0; i < 25; i++) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(50, 10+i*60, 260, 1)];
        [lineView setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.3f]];
        [self.scrollView addSubview:lineView];
    }
}

- (void)drawLabels:(UIFont *)font
{
    for (int i = 0; i < 25; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, i*60, 30, 20)];
        [label setTextAlignment:NSTextAlignmentRight];
        [label setFont:font];
        [self.scrollView addSubview:label];
        if (i == 0) {
            [label setText:@"12"];
        } else if (i < 12) {
            [label setText:[NSString stringWithFormat:@"%i", i]];
        } else {
            if (i == 12) {
                [label setText:@"Noon"];
            } else if (i == 24) {
                [label setText:@"12"];
            } else {
                [label setText:[NSString stringWithFormat:@"%i", i%12]];
            }
        }
    }
}

- (void)drawTimeLineWithLabel:(NSDate *)date
{
    NSLog(@"Draw timeLineWithLabel:%@", date);
    if (!self.timeLineLabel)
        self.timeLineLabel = [[UIView alloc] init];
    if (!self.timeLineTimeLabel)
        self.timeLineTimeLabel = [[UILabel alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:a"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"PST"]];
    NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
    NSArray *array = [stringFromDate componentsSeparatedByString:@":"];
    CGFloat num = 10 + (CGFloat)[[array objectAtIndex:0] floatValue] * 60 + (CGFloat)[[array objectAtIndex:1] floatValue];

    
    [self.timeLineLabel setFrame:CGRectMake(50, num, 270, 1)];
    [self.timeLineLabel setBackgroundColor:[[UIColor orangeColor] colorWithAlphaComponent:1.0f]];
    [self.timeLineTimeLabel setFrame:CGRectMake(5, num - 9, 45, 20)];
    [self.timeLineTimeLabel setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    [self.timeLineTimeLabel setTextColor:[UIColor orangeColor]];
    [self.timeLineTimeLabel setBackgroundColor:[UIColor whiteColor]];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"h:mm a"];
    [formatter1 setTimeZone:[NSTimeZone timeZoneWithName:@"PST"]];
    NSString *stringFromDate1 = [formatter1 stringFromDate:[NSDate date]];
    [self.timeLineTimeLabel setText:stringFromDate1];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.scrollView addSubview:self.timeLineLabel];
        [self.scrollView addSubview:self.timeLineTimeLabel];
    });

}

- (void)removeTimeLineWithLabel
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.timeLineLabel removeFromSuperview];
        [self.timeLineTimeLabel removeFromSuperview];
    });
}

- (void)addGesture
{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [longPress setNumberOfTouchesRequired:1];
    [self.scrollView addGestureRecognizer:longPress];
}

- (void)loadEventsIntoDayView:(NSArray *)events
{
    if(debug) NSLog(@"loadEventsIntoDayView:");
    
    [self.events removeAllObjects];
    
    for (id subview in self.scrollView.subviews) {
        if ([subview isMemberOfClass:[DayViewCell class]]) {
            [subview removeFromSuperview];
        }
    }
    
    for (EKEvent *event in events) {
        if ([[[CalendarDataSource convertDatesForDayViewCell:event] objectAtIndex:2] isEqualToNumber:@0]) {
            [self createCell:event];
        }
    }
}

- (void)createCell:(EKEvent *)event
{
    DayViewCell *eventCell = [DayViewCell eventViewWithEvent:event];
    [eventCell setDelegate:self];
    [eventCell setBackgroundColor:[[UIColor colorWithCGColor:[[event calendar] CGColor]] colorWithAlphaComponent:0.4f]];
    [eventCell setEdgeColor:[UIColor colorWithCGColor:[[event calendar] CGColor]]];
    [[[eventCell getInfo] objectAtIndex:3] setTextColor:[UIColor colorWithCGColor:[[event calendar] CGColor]]];
    [[[eventCell getInfo] objectAtIndex:4] setTextColor:[UIColor colorWithCGColor:[[event calendar] CGColor]]];
    NSArray *spliArray = [[CalendarDataSource convertDatesForDayViewCell:event] objectAtIndex:0];
    NSArray *spliArray1 = [[CalendarDataSource convertDatesForDayViewCell:event] objectAtIndex:1];
    CGFloat num = 10 + (CGFloat)[[spliArray objectAtIndex:0] floatValue] * 60 + (CGFloat)[[spliArray objectAtIndex:1] floatValue];
    int i = [[spliArray1 objectAtIndex:0] intValue] - [[spliArray objectAtIndex:0] intValue];
    int j = [[spliArray1 objectAtIndex:1] intValue] - [[spliArray objectAtIndex:1] intValue];
    if ([[spliArray objectAtIndex:1] isEqualToString:@"00"]) {
        [eventCell setFrame:CGRectMake(62, num + 2, 258, i*60+j - 4)];
    } else
        [eventCell setFrame:CGRectMake(62, num, 258, i*60+j)];
    [self.events addObject:eventCell];
    [self.scrollView addSubview:eventCell];
}

#pragma mark - Add/Delete Event Public Methods
- (void)didAddEventToView:(EKEvent *)event
{
    if (debug) NSLog(@"didAddEventToView:%@", event);
    NSString *start = [[CalendarDataSource convertDates:event] objectAtIndex:1];
    NSArray *spliArray = [start componentsSeparatedByString:@":"];
    NSString *end   = [[CalendarDataSource convertDates:event] objectAtIndex:2];
    NSArray *spliArray1 = [end componentsSeparatedByString:@":"];
    
    DayViewCell *sameTimeEvent = [DayViewCell eventViewWithEvent:event];
    [sameTimeEvent setEdgeColor:[UIColor colorWithCGColor:[[event calendar] CGColor]]];
    [sameTimeEvent setBackgroundColor:[[UIColor colorWithCGColor:[[event calendar] CGColor]] colorWithAlphaComponent:0.4f]];
    [[[sameTimeEvent getInfo] objectAtIndex:3] setTextColor:[UIColor colorWithCGColor:[[event calendar] CGColor]]];
    [[[sameTimeEvent getInfo] objectAtIndex:4] setTextColor:[UIColor colorWithCGColor:[[event calendar] CGColor]]];
    [sameTimeEvent setDelegate:self];
    CGFloat num = 10 + (CGFloat)[[spliArray objectAtIndex:0] floatValue] * 60 + (CGFloat)[[spliArray objectAtIndex:1] floatValue];
    
    int i = [[spliArray1 objectAtIndex:0] intValue] - [[spliArray objectAtIndex:0] intValue];
    int j = [[spliArray1 objectAtIndex:1] intValue] - [[spliArray objectAtIndex:1] intValue];
    
    if ([[spliArray objectAtIndex:1] isEqualToString:@"00"]) {
        [sameTimeEvent setFrame:CGRectMake(42, num + 2, 258, i*60+j - 4)];
    } else {
        [sameTimeEvent setFrame:CGRectMake(42, num, 258, i*60+j)];
    }
    
    [self.events addObject:sameTimeEvent];
    [self.scrollView addSubview:sameTimeEvent];
}

- (void)didDeleteEventFromView:(EKEvent *)event
{
    if (debug) NSLog(@"didDeleteEventFromView:%@", event);
    for (DayViewCell * someEvent in self.events) {
        if ([[[someEvent getInfo] objectAtIndex:0] isEqual:event.eventIdentifier]) {
            if ([self.events containsObject:someEvent]) {
                [someEvent removeFromSuperview];
                [self.events removeObject:someEvent];
            }
        }
    }
}


#pragma mark - DayViewCell Delegate Methods
/*
 * Inform the delegate that the user tapped the event cell
 * The desired feature is to push a navigation controller onto
 * the stack to show the details of the event cell.
 */
- (void)didTapEventView:(DayViewCell *)tappedDay recognizer:(UITapGestureRecognizer *)recognizer {
    [self.delegate didClickOnEvent:tappedDay];
}

/*
 * Inform the delegate that the user is currently panning the event cell
 * The desired feature is to move the cell to the location the user finishes 
 * panning. WThe animation of the user dragging should be present as well.
 */
- (void)didPanEventView:(DayViewCell *)dayViewCell recognizer:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.scrollView];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.scrollView];
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint velocity = [recognizer velocityInView:self.scrollView];
        CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));
        CGFloat slideMult = magnitude / 200;
        float slideFactor = 0.0 * slideMult; // Increase for more of a slide
        CGPoint finalPoint = CGPointMake(recognizer.view.center.x + (velocity.x * slideFactor),
                                         recognizer.view.center.y + (velocity.y * slideFactor));
        //finalPoint.x = MIN(MAX(finalPoint.x, 0), self.scrollView.contentSize.width);
        finalPoint.x = 172;
        finalPoint.y = MIN(MAX(finalPoint.y, 0), self.scrollView.contentSize.height);
        
        [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            recognizer.view.center = finalPoint;
        } completion:^(BOOL finished) {
            if (finished) {
                [self.delegate didFinishPanning:dayViewCell];
            }
        }];
        
    }
}

/* 
 * Inform the delegate that the user Long Pressed the event cell
 * The desired feature here is to allow the user to move the cell
 * anywhere on the screen to allow for a change in time of the event
 * NOTE: this should not be able to be done if the event is not changeable.
 */

- (void)didLongPressEventView:(DayViewCell *)longPressedDay recognizer:(UILongPressGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [self.scrollView bringSubviewToFront:longPressedDay];
        return;
    }
    
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        [self doStuff:longPressedDay morestuff:recognizer];
        
        //CGPoint recognizerPoint = [recognizer locationInView:self.scrollView];
    }
    

}




#pragma mark - Setter Methods
- (NSMutableArray *)events
{
    if (!_events) _events = [NSMutableArray array];
    return _events;
}


#pragma mark - Working on them 
-  (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    
    /*
     CGPoint initialSwipePoint = [sender locationInView:[sender view]];
     
     DayViewCell *sameTimeEvent = [DayViewCell eventViewWithDate:[NSString stringWithFormat: @"%.2f", initialSwipePoint.y - 10]];
     [sameTimeEvent setBackgroundColor:[[UIColor orangeColor] colorWithAlphaComponent:0.3f]];
     [sameTimeEvent setEdgeColor:[UIColor orangeColor]];
     [sameTimeEvent.titleLabel setTextColor:[UIColor orangeColor]];
     [sameTimeEvent.locationLabel setTextColor:[UIColor orangeColor]];
     sameTimeEvent.delegate = self;
     
     if (sender.state == UIGestureRecognizerStateEnded) {
     NSLog(@"UIGestureRecognizerStateEnded");
     //Do Whatever You want on End of Gesture
     [self.delegate didAddEvent:sameTimeEvent];
     
     } else if (sender.state == UIGestureRecognizerStateBegan){
     NSLog(@"UIGestureRecognizerStateBegan.");
     //Do Whatever You want on Began of Gesture
     [sameTimeEvent setFrame:CGRectMake(42, initialSwipePoint.y, 258, 50)];
     [self.scrollView addSubview:sameTimeEvent];
     }
     */
}

// Ex.
// ScrollView point pressed will be at ex. position (280,320);
// The Cells center is at (160, 330)
// Problem: need to move the cell without moving the center immediatly before moving the cell, causing a glitch look.
//    -----------
//    |-        | (-) Point pressed on cell, return 2 points.
//    -----------
// Point 1: The point is the change of distance from the point
// pressed on the cell to the point of the cells center
// Point 2: The point pressed on the scrollView where the cell was pressed.
//
// If we moved the cell from center (160, 330) to (160, 440)
//

- (void)doStuff:(DayViewCell *)longPressedDay morestuff:(UILongPressGestureRecognizer *)recognizer
{
    longPressedDay.center = [recognizer locationInView:self.scrollView];
    
    /*
     CGPoint dayCellPointPressed = [recognizer locationInView:longPressedDay];
     CGPoint dayCenter = [longPressedDay center];
     CGPoint scrollViewPointPressed = [recognizer locationInView:self.scrollView];
     
     
     int deltaX = dayCellPointPressed.x - dayCenter.x;
     int deltaY = dayCellPointPressed.y - dayCenter.y;
     CGPoint deltaPoint = CGPointMake(deltaX, deltaY);
     
     NSLog(@"%@", NSStringFromCGPoint(dayCellPointPressed));
     NSLog(@"%@", NSStringFromCGPoint(dayCenter));
     NSLog(@"%@", NSStringFromCGPoint(deltaPoint));
     
     
     
     longPressedDay.center = CGPointMake(longPressedDay.center.x + (scrollViewPointPressed.x - deltaPoint.x),
     longPressedDay.center.y + (scrollViewPointPressed.y - deltaPoint.y));
     
     
     //longPressedDay.center = scrollViewPointPressed;
     
     if (recognizer.state == UIGestureRecognizerStateEnded)
     {
     [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
     CGPoint placardCenter = longPressedDay.center;
     placardCenter.x = 172;
     longPressedDay.center = placardCenter;
     } completion:^(BOOL finished) {
     if (finished) {
     [self.delegate didFinishPanning:longPressedDay];
     }
     }];
     }
     */
}


/*
 - (void)didLongPressEventView:(DayViewCell *)longPressedDay recognizer:(UILongPressGestureRecognizer *)recognizer {
 
 
 CGPoint translation = [recognizer locationInView:self.scrollView];
 
 NSLog(@"%@", NSStringFromCGPoint(translation));
 
 recognizer.view.center = CGPointMake(recognizer.view.center.x,// + translation.x,
 recognizer.view.center.y);// + translation.y);
 //[recognizer setTranslation:CGPointMake(0, 0) inView:self.scrollView];
 CGPoint finalPoint;
 if (recognizer.state == UIGestureRecognizerStateChanged) {
 
 //CGPoint velocity = [recognizer velocityInView:self.scrollView];
 //CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));
 //CGFloat slideMult = magnitude / 200;
 //float slideFactor = 0.0 * slideMult; // Increase for more of a slide
 finalPoint = CGPointMake(recognizer.view.center.x,// + (velocity.x * slideFactor),
 recognizer.view.center.y);// + (velocity.y * slideFactor));
 //finalPoint.x = MIN(MAX(finalPoint.x, 0), self.scrollView.contentSize.width);
 finalPoint.x = 172;
 finalPoint.y = MIN(MAX(finalPoint.y, 0), self.scrollView.contentSize.height);
 
 [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
 recognizer.view.center = finalPoint;
 longPressedDay.center = finalPoint;
 } completion:^(BOOL finished) {
 if (finished) {
 [self.delegate didFinishPanning:longPressedDay];
 }
 }];
 
 }
 
 if (recognizer.state == UIGestureRecognizerStateEnded)
 {
 
 }
 }
 */

@end
