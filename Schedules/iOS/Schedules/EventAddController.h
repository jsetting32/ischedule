//
//  EventAddController.h
//  Schedules
//
//  Created by John Setting on 11/20/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventAddController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView     *   eventTableView;
    PFObject        *   event;
}

@property (nonatomic, retain) UITableView       *   eventTableView;
@property (nonatomic, strong) PFObject          *   event;

@end
