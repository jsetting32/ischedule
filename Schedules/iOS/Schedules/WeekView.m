//
//  WeekView.m
//  iSchedj
//
//  Created by John Setting on 3/30/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "WeekView.h"

@interface WeekView()
@property (nonatomic) UIScrollView *scrollView;

@end

@implementation WeekView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect
{
    [self loadScrollView:rect];
    [self drawLines];
    [self drawLabels:[UIFont fontWithName:@"Arial" size:10.0f]];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.scrollView setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y + 70, self.frame.size.width, self.frame.size.height - 70)];
}

- (void)loadScrollView:(CGRect)rect
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setContentSize:CGSizeMake(self.frame.size.width, 1465)];
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.scrollView];
}

- (void)drawLines
{
    for (int i = 0; i < 25; i++) {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(40, 10+i*60, 420, 1)];
        [lineView setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.3f]];
        [self.scrollView addSubview:lineView];
    }
}

- (void)drawLabels:(UIFont *)font
{
    for (int i = 0; i < 25; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, i*60, 30, 20)];
        [label setTextAlignment:NSTextAlignmentRight];
        [label setFont:font];
        [self.scrollView addSubview:label];
        if (i == 0) {
            [label setText:@"12"];
        } else if (i < 12) {
            [label setText:[NSString stringWithFormat:@"%i", i]];
        } else {
            if (i == 12) {
                [label setText:@"Noon"];
            } else if (i == 24) {
                [label setText:@"12"];
            } else {
                [label setText:[NSString stringWithFormat:@"%i", i%12]];
            }
        }
    }
}

@end
