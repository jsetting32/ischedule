//
//  EventfulEvent.m
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "EventfulEvent.h"

@interface EventfulEvent()
@end

@implementation EventfulEvent

- (void)setEvent:(NSDictionary *)event {
    
    if ( [event objectForKey:@"allday"] != [NSNull null] ) self.allDay = [[event objectForKey:@"allday"] integerValue];
    if ( [event objectForKey:@"calendar_count"] != [NSNull null] ) self.calendarCount = [[event objectForKey:@"calendar_count"] integerValue];
    if ( [event objectForKey:@"calendars"] != [NSNull null] ) self.calendars = [event objectForKey:@"calendars"];
    if ( [event objectForKey:@"city_name"] != [NSNull null] ) self.cityName = [event objectForKey:@"city_name"];
    if ( [event objectForKey:@"country_abbr"] != [NSNull null] ) self.countryAbbr1 = [event objectForKey:@"country_abbr"];
    if ( [event objectForKey:@"country_abbr2"] != [NSNull null] ) self.countryAbbr2 = [event objectForKey:@"country_abbr2"];
    if ( [event objectForKey:@"country_name"] != [NSNull null] ) self.countryName = [event objectForKey:@"country_name"];
    if ( [event objectForKey:@"created"] != [NSNull null] ) self.createdDate = [event objectForKey:@"created"];
    if ( [event objectForKey:@"description"] != [NSNull null] ) self.description = [event objectForKey:@"description"];
    if ( [event objectForKey:@"geocode_type"] != [NSNull null] ) self.geoCode = [event objectForKey:@"geocode_type"];
    if ( [event objectForKey:@"going"] != [NSNull null] ) self.going = [event objectForKey:@"going"];
    if ( [event objectForKey:@"going_count"] != [NSNull null] ) self.goingCount = [[event objectForKey:@"going_count"] intValue];
    if ( [event objectForKey:@"groups"] != [NSNull null] ) self.groups = [event objectForKey:@"groups"];
    if ( [event objectForKey:@"id"] != [NSNull null] ) self.idEvent = [event objectForKey:@"id"];
    if ( [event objectForKey:@"image"] != [NSNull null] ) self.image = [event objectForKey:@"image"];
    if ( [event objectForKey:@"latitude"] != [NSNull null] ) self.latitude = [[event objectForKey:@"latitude"] floatValue];
    if ( [event objectForKey:@"link_count"] != [NSNull null] ) self.linkCount = [[event objectForKey:@"link_count"] integerValue];
    if ( [event objectForKey:@"longitude"] != [NSNull null] ) self.longitude = [[event objectForKey:@"longitude"] floatValue];
    if ( [event objectForKey:@"modified"] != [NSNull null] ) self.modifiedDate = [event objectForKey:@"modified"];
    if ( [event objectForKey:@"owner"] != [NSNull null] ) self.owner = [event objectForKey:@"owner"];
    if ( [event objectForKey:@"performers"] != [NSNull null] ) self.performers = [event objectForKey:@"performers"];
    if ( [event objectForKey:@"postal_code"] != [NSNull null] ) self.postalCode = [[event objectForKey:@"postal_code"] integerValue];
    if ( [event objectForKey:@"privacy"] != [NSNull null] ) self.privacy = [[event objectForKey:@"privacy"] integerValue];
    if ( [event objectForKey:@"recur_string"] != [NSNull null] ) self.recurrenceString = [event objectForKey:@"recur_string"];
    if ( [event objectForKey:@"region_abbr"] != [NSNull null] ) self.regionAbbr = [event objectForKey:@"region_abbr"];
    if ( [event objectForKey:@"start_time"] != [NSNull null] ) self.startTime = [event objectForKey:@"start_time"];
    if ( [event objectForKey:@"stop_time"] != [NSNull null] ) self.stopTime = [event objectForKey:@"stop_time"];
    if ( [event objectForKey:@"title"] != [NSNull null] ) self.title = [event objectForKey:@"title"];
    if ( [event objectForKey:@"tz_city"] != [NSNull null] ) self.tzCity = [event objectForKey:@"tz_city"];
    if ( [event objectForKey:@"tz_country"] != [NSNull null] ) self.tzCountry = [event objectForKey:@"tz_country"];
    if ( [event objectForKey:@"tz_id"] != [NSNull null] ) self.tzId = [event objectForKey:@"tz_id"];
    if ( [event objectForKey:@"tz_olson_path"] != [NSNull null] ) self.tzOlsenPath = [event objectForKey:@"tz_olson_path"];
    if ( [event objectForKey:@"url"] != [NSNull null] ) self.url = [event objectForKey:@"url"];
    if ( [event objectForKey:@"venue_address"] != [NSNull null] ) self.venueAddress = [event objectForKey:@"venue_address"];
    if ( [event objectForKey:@"venue_display"] != [NSNull null] ) self.venueDisplay = [event objectForKey:@"venue_display"];
    if ( [event objectForKey:@"venue_id"] != [NSNull null] ) self.venueId = [event objectForKey:@"venue_id"];
    if ( [event objectForKey:@"venue_name"] != [NSNull null] ) self.venueName = [event objectForKey:@"venue_name"];
    if ( [event objectForKey:@"venue_url"] != [NSNull null] ) self.venueURL = [event objectForKey:@"venue_url"];
    if ( [event objectForKey:@"watching_count"] != [NSNull null] ) self.watchingCount = [[event objectForKey:@"watching_count"] integerValue];
}

- (void)eventImage:(NSDictionary *)image {
    NSData *data = [NSData dataWithContentsOfURL:[self.image objectForKey:@"url"]];
    [self.delegate didFinishLoadingImageData:[UIImage imageWithData:data]];
}

@end
