//
//  EventsTableViewCell.m
//  MYSCHEDULER
//
//  Created by John Setting on 8/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "EventsTableViewCell.h"

@implementation EventsTableViewCell

@synthesize shareButton;
@synthesize imageViewPic;
@synthesize eventDescription;
@synthesize eventTime;
@synthesize eventTitle;
@synthesize event;
@synthesize eventInfo;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		[self.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundFindFriendsCell.png"]]];
        [self setSelectionStyle:UITableViewCellSelectionStyleGray];
		
        self.imageViewPic = [[UIImageView alloc] init];
		[self.contentView addSubview:self.imageViewPic];
        
		self.eventTitle = [[UILabel alloc] init];
		self.eventTitle.textAlignment = NSTextAlignmentLeft;
		self.eventTitle.backgroundColor = [UIColor clearColor];
		[self.eventTitle setFont:[UIFont boldSystemFontOfSize:12.0f]];
		self.eventTitle.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.eventTitle setTextColor:[UIColor colorWithRed:87.0f/255.0f green:72.0f/255.0f blue:49.0f/255.0f alpha:1.0f]];
		[self.eventTitle setShadowColor:[UIColor whiteColor]];
		[self.eventTitle setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
		[self.contentView addSubview:self.eventTitle];
		
		self.eventDescription = [[UILabel alloc] init];
		self.eventDescription.textAlignment = NSTextAlignmentLeft;
		self.eventDescription.backgroundColor = [UIColor clearColor];
		[self.eventDescription setFont:[UIFont boldSystemFontOfSize:10.0f]];
		self.eventDescription.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.eventDescription setTextColor:[UIColor colorWithRed:87.0f/255.0f green:72.0f/255.0f blue:49.0f/255.0f alpha:1.0f]];
		[self.eventDescription setShadowColor:[UIColor whiteColor]];
		[self.eventDescription setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
		[self.contentView addSubview:self.eventDescription];
		
		self.eventTime = [[UILabel alloc] init];
		self.eventTime.textAlignment = NSTextAlignmentLeft;
		self.eventTime.backgroundColor = [UIColor clearColor];
		[self.eventTime setFont:[UIFont boldSystemFontOfSize:10.0f]];
		self.eventTime.lineBreakMode = NSLineBreakByTruncatingTail;
		[self.eventTime setTextColor:[UIColor colorWithRed:87.0f/255.0f green:72.0f/255.0f blue:49.0f/255.0f alpha:1.0f]];
		[self.eventTime setShadowColor:[UIColor whiteColor]];
		[self.eventTime setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
		[self.contentView addSubview:self.eventTime];
		 
        self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.shareButton setImage:[UIImage imageNamed:@"shareIcon.png"] forState:UIControlStateNormal];
        [self.shareButton addTarget:self action:@selector(didTapShareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.shareButton];
    }
    return self;
}

#pragma mark - PAPFindFriendsCell

- (void)setCellForEvent:(SchedulesEvent *)aEvent
{
	if (![aEvent valueForKey:@"location"] || [[aEvent valueForKey:@"location"] isKindOfClass:[NSNull class]]) {
		[eventTitle			setFrame:CGRectMake( 75.0f, 14.0f, 180.0f, 14.0f)];
	} else {
		[eventTitle			setFrame:CGRectMake( 75.0f, 4.0f, 180.0f, 14.0f)];
		[eventDescription	setFrame:CGRectMake( 75.0f, 20.0f, 180.0f, 14.0f)];
	}
	
    [shareButton	setFrame:CGRectMake( 270.0f, 0, self.frame.size.width - 280.0f, self.frame.size.height)];
	
	[eventTitle setText:[aEvent valueForKey:@"title"]];
	[eventDescription setText:[aEvent valueForKey:@"description"]];
		
	[eventTime	setFrame:CGRectMake( 25.0f, 16.0f, 45.0f, 10.0f)];
	
    if ([[aEvent valueForKey:@"allDay"] isEqual:@NO]) {
		[eventTime setText:[self NSDateFormatter:[aEvent valueForKey:@"startDate"]]];
	} else if ([[aEvent valueForKey:@"allDay"] isEqual:@YES]){
        [eventTime setText:@"All Day"];
	}

    /*
	[imageViewPic setFrame:CGRectMake( 10.0f, 15.0f, 10.0f, 10.0f)];
	UIGraphicsBeginImageContextWithOptions(imageViewPic.bounds.size, NO, [UIScreen mainScreen].scale);
	CGContextRef contextRef = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(contextRef, [[EKEventEvent calendar] CGColor]);
	CGContextFillEllipseInRect(contextRef,(CGRectMake (0.f, 0.f, imageViewPic.bounds.size.width, imageViewPic.bounds.size.height)));
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	imageViewPic.image = image;
	
	UIGraphicsEndImageContext();
    */
}

- (void)setCellForFeedEvent:(NSDictionary *)feed
{
	eventInfo = feed;
	[eventTitle			setFrame:CGRectMake( 75.0f, 4.0f, 180.0f, 14.0f)];
	[eventDescription	setFrame:CGRectMake( 75.0f, 20.0f, 180.0f, 14.0f)];
	
	[eventTime	setFrame:CGRectMake( 15.0f, 16.0f, 45.0f, 10.0f)];
	[eventTime	setText:[[self FeedsDateFormatter:eventInfo[@"start_time"]] objectAtIndex:0]];
	[eventTitle setText:eventInfo[@"title"]];

	if (!eventInfo[@"description"] || [eventInfo[@"description"] isKindOfClass:[NSNull class]]) {
		[eventTitle			setFrame:CGRectMake( 75.0f, 14.0f, 180.0f, 14.0f)];
	} else {
		[eventTitle			setFrame:CGRectMake( 75.0f, 4.0f, 180.0f, 14.0f)];
		[eventDescription	setFrame:CGRectMake( 75.0f, 20.0f, 180.0f, 14.0f)];
		[eventDescription	setText:eventInfo[@"description"]];
	}
	
	UIButton *comment = [UIButton buttonWithType:UIButtonTypeCustom];
	[comment setFrame:CGRectMake(0, 0, 80, 20)];
	//[comment addTarget:self action:@selector(:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:comment];
	
	UIButton *share = [UIButton buttonWithType:UIButtonTypeCustom];
	[share setFrame:CGRectMake(80, 0, 80, 20)];
	//[share addTarget:self action:@selector(:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:share];
	
	UIButton *like = [UIButton buttonWithType:UIButtonTypeCustom];
	[like setFrame:CGRectMake(160, 0, 80, 20)];
	//[like addTarget:self action:@selector(:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:like];
}


- (NSString *)NSDateFormatter:(NSDate *)date
{
	static NSDateFormatter *dateFormat = nil;
	
	if (nil == dateFormat) {
		dateFormat = [[NSDateFormatter alloc] init];
		[dateFormat setDateFormat:@"h:mm a"];
	}
	
	return [dateFormat stringFromDate:date];
}

- (NSArray *)FeedsDateFormatter:(NSString *)date
{
	
	static NSDateFormatter *stringToDateFormatter = nil;
	if (nil == stringToDateFormatter) {
		stringToDateFormatter = [[NSDateFormatter alloc] init];
		[stringToDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	}
	
	NSDate *dateFromString = [[NSDate alloc] init];
	dateFromString = [stringToDateFormatter dateFromString:date];
	
	static NSDateFormatter *dateToStringFormatter = nil;
	if (nil == dateToStringFormatter) {
		dateToStringFormatter = [[NSDateFormatter alloc] init];
		[dateToStringFormatter setDateFormat:@"hh:mm a"];
	}
	
	static NSDateFormatter *monthFormatter = nil;
	if (nil == monthFormatter) {
		monthFormatter = [[NSDateFormatter alloc] init];
		[monthFormatter setDateFormat:@"MMMM yyyy"];
	}
	
	static NSDateFormatter *dayFormatter = nil;
	if (nil == dayFormatter)
	{
		dayFormatter = [[NSDateFormatter alloc] init];
		[dayFormatter setDateFormat:@"dd"];
	}
	
	
	static NSDateFormatter *dayStringFormatter = nil;
	if (nil == dayStringFormatter)
	{
		dayStringFormatter = [[NSDateFormatter alloc] init];
		[dayStringFormatter setDateFormat:@"EEEE"];
	}
	
	NSArray *array = [[NSArray alloc] initWithObjects:[dateToStringFormatter stringFromDate:dateFromString], [monthFormatter stringFromDate:dateFromString], [dayFormatter stringFromDate:dateFromString], [dayStringFormatter stringFromDate:dateFromString], nil];
	
	return array;
}

+ (CGFloat)heightForCell {
    return 67.0f;
}

/* Inform delegate that the share button was pressed */
- (void)didTapShareButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapShareActionButton:)])
    {
        [self.delegate cell:self didTapShareActionButton:self.event];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
 	// Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
	NSInteger inset = 9;
    frame.origin.x += inset;
    frame.size.width -= 2 * inset;
    [super setFrame:frame];
}

@end
