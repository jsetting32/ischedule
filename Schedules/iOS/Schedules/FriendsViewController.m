//
//  FriendsViewController.m
//  Schedules
//
//  Created by John Setting on 11/15/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "FriendsViewController.h"

@interface FriendsViewController ()

@end

@implementation FriendsViewController
@synthesize friendTableView;
@synthesize friendsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    friendTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    friendTableView.delegate = self;
    friendTableView.dataSource = self;
    [self.view addSubview:friendTableView];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadUsers];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void) loadUsers {
    
    friendsArray = [NSMutableArray array];
    
    if ([self.title isEqualToString:@"Friends"]) {
        PFQuery *friends = [PFQuery queryWithClassName:@"Activity"];
        [friends whereKey:@"type" equalTo:@"friend"];
        [friends whereKey:@"fromUser" equalTo:[PFUser currentUser]];

        PFQuery *friended = [PFQuery queryWithClassName:@"Activity"];
        [friended whereKey:@"type" equalTo:@"friend"];
        [friended whereKey:@"toUser" equalTo:[PFUser currentUser]];

        PFQuery *query = [PFQuery orQueryWithSubqueries:@[friended, friends]];
        [query setCachePolicy:kPFCachePolicyNetworkElseCache];
        [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
            if (!error) {
                for (PFObject *object in results) {
                    if ([[[object valueForKey:@"fromUser"] objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                        PFQuery *user = [PFUser query];
                        [user getObjectInBackgroundWithId:[[object valueForKey:@"toUser"] objectId] block:^(PFObject *object, NSError *error) {
                            if (!error) {
                                [friendsArray addObject:object];
                                [friendTableView reloadData];
                            }
                        }];
                    } else if ([[[object valueForKey:@"toUser"] objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                        PFQuery *user = [PFUser query];
                        [user getObjectInBackgroundWithId:[[object valueForKey:@"fromUser"] objectId] block:^(PFObject *object, NSError *error) {
                            if (!error) {
                                [friendsArray addObject:object];
                                [friendTableView reloadData];
                            }
                        }];
                    }
                }
            }
        }];
        
    } else if ([self.title isEqualToString:@"Sent Requests"]) {
    
        PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
        [query whereKey:@"type" equalTo:@"pending"];
        [query whereKey:@"fromUser" equalTo:[PFUser currentUser]];
        [query setCachePolicy:kPFCachePolicyNetworkElseCache];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject *object in objects) {
                    PFQuery *user = [PFUser query];
                    [user getObjectInBackgroundWithId:[[object valueForKey:@"toUser"] objectId] block:^(PFObject *object, NSError *error) {
                        if (!error) {
                            [friendsArray addObject:object];
                            [friendTableView reloadData];
                        }
                    }];
                }
            }
        }];
    } else if ([self.title isEqualToString:@"Friend Requests"]) {
      
        PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
        [query whereKey:@"type" equalTo:@"pending"];
        [query whereKey:@"toUser" equalTo:[PFUser currentUser]];
        [query setCachePolicy:kPFCachePolicyNetworkElseCache];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject *object in objects) {
                    NSLog(@"%@", object);
                    NSLog(@"%@", [[object valueForKey:@"fromUser"] objectId]);
                    NSLog(@"%@", [[PFUser currentUser]objectId]);
                    PFQuery *user = [PFUser query];
                    [user getObjectInBackgroundWithId:[[object valueForKey:@"fromUser"] objectId] block:^(PFObject *object, NSError *error) {
                        if (!error) {
                            [friendsArray addObject:object];
                            [friendTableView reloadData];
                        }
                    }];
                }
            }
        }];
    }
}

#pragma mark - Table view data source
// Add new methods
/*
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[CustomFooterView alloc] init];
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 30;
}
 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

}
*/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [friendsArray count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"FollowingFollowersCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [[self.friendsArray objectAtIndex:indexPath.row] objectForKey:@"firstName"], [[self.friendsArray objectAtIndex:indexPath.row] objectForKey:@"lastName"]];
    cell.detailTextLabel.text = [[self.friendsArray objectAtIndex:indexPath.row] objectForKey:@"username"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SearchedUserViewController *searchedUser = [[SearchedUserViewController alloc] init];
    searchedUser.user = [self.friendsArray objectAtIndex:indexPath.row];
    searchedUser.bar = @"pushed";
    [self.navigationController pushViewController:searchedUser animated:YES];
    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        if ([self.title isEqualToString:@"Friends"]) {
            [tableView beginUpdates];
            [self deleteUser:[self.friendsArray objectAtIndex:indexPath.row]];
            [self.friendsArray removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        } else if ([self.title isEqualToString:@"Sent Requests"]) {
            [tableView beginUpdates];
            [self deleteUser:[self.friendsArray objectAtIndex:indexPath.row]];
            [self.friendsArray removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
        }
        

        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    } else {
        // editingStyle == UITableViewCellEditingStyleNone
    }
}

- (void) deleteUser:(PFUser *)user {
    
    if ([self.title isEqualToString:@"Friends"]) {
        [Utility unfriendUserInBackground:user block:^(BOOL succeeded, NSError *error) {
        }];
    } else if ([self.title isEqualToString:@"Sent Requests"]) {
        [Utility unfriendUserRequestEventually:user];
    } 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
