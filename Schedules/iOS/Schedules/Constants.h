//
//  Constant.h
//  Fraple
//
//  Created by John Setting on 8/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

typedef enum {
	SCHHomeTabBarItemIndex = 0,
	SCHEmptyTabBarItemIndex = 1,
	SCHActivityTabBarItemIndex = 2
} SCHTabBarControllerViewControllerIndex;


// Define an array of Facebook Ids for accounts to auto-follow on signup
#define kSCHAutoFollowAccountIds @[ ]

#pragma mark - NSUserDefaults
extern NSString *const kSCHUserDefaultsActivityFeedViewControllerLastRefreshKey;
extern NSString *const kSCHUserDefaultsCacheFriendsKey;

#pragma mark - Launch URLs

extern NSString *const kSCHLaunchURLHostTakePicture;


#pragma mark - NSNotification
extern NSString *const SCHAppDelegateApplicationDidReceiveRemoteNotification;
extern NSString *const SCHUtilityUserFriendChangedNotification;
extern NSString *const SCHUtilityUserLikedUnlikedPhotoCallbackFinishedNotification;
extern NSString *const SCHUtilityDidFinishProcessingProfilePictureNotification;
extern NSString *const SCHTabBarControllerDidFinishEditingPhotoNotification;
extern NSString *const SCHTabBarControllerDidFinishImageFileUploadNotification;
extern NSString *const SCHPhotoDetailsViewControllerUserDeletedPhotoNotification;
extern NSString *const SCHPhotoDetailsViewControllerUserLikedUnlikedPhotoNotification;
extern NSString *const SCHPhotoDetailsViewControllerUserCommentedOnPhotoNotification;


#pragma mark - User Info Keys
extern NSString *const SCHPhotoDetailsViewControllerUserLikedUnlikedPhotoNotificationUserInfoLikedKey;
extern NSString *const kSCHEditPhotoViewControllerUserInfoCommentKey;


#pragma mark - Installation Class

// Field keys
extern NSString *const kSCHInstallationUserKey;


#pragma mark - PFObject Activity Class
// Class key
extern NSString *const kSCHActivityClassKey;

// Field keys
extern NSString *const kSCHActivityTypeKey;
extern NSString *const kSCHActivityFromUserKey;
extern NSString *const kSCHActivityToUserKey;
extern NSString *const kSCHActivityContentKey;
extern NSString *const kSCHActivityPhotoKey;

// Type values
extern NSString *const kSCHActivityTypeLike;
extern NSString *const kSCHActivityTypeFriend;
extern NSString *const kSCHActivityTypeComment;
extern NSString *const kSCHActivityTypeJoined;


#pragma mark - PFObject User Class
// Field keys
extern NSString *const kSCHUserDisplayNameKey;
extern NSString *const kSCHUserIDKey;
extern NSString *const kSCHUserPhotoIDKey;
extern NSString *const kSCHUserProfilePicSmallKey;
extern NSString *const kSCHUserProfilePicMediumKey;
extern NSString *const kSCHUserFriendsKey;
extern NSString *const kSCHUserAlreadyAutoFollowedFriendsKey;


#pragma mark - PFObject Photo Class
// Class key
extern NSString *const kSCHPhotoClassKey;

// Field keys
extern NSString *const kSCHPhotoPictureKey;
extern NSString *const kSCHPhotoThumbnailKey;
extern NSString *const kSCHPhotoUserKey;
extern NSString *const kSCHPhotoOpenGraphIDKey;


#pragma mark - Cached Photo Attributes
// keys
extern NSString *const kSCHPhotoAttributesIsLikedByCurrentUserKey;
extern NSString *const kSCHPhotoAttributesLikeCountKey;
extern NSString *const kSCHPhotoAttributesLikersKey;
extern NSString *const kSCHPhotoAttributesCommentCountKey;
extern NSString *const kSCHPhotoAttributesCommentersKey;


#pragma mark - Cached User Attributes
// keys
extern NSString *const kSCHUserAttributesPhotoCountKey;
extern NSString *const kSCHUserAttributesIsFriendedByCurrentUserKey;


#pragma mark - PFPush Notification Payload Keys

extern NSString *const kASNSAlertKey;
extern NSString *const kASNSBadgeKey;
extern NSString *const kASNSSoundKey;

extern NSString *const kSCHPushPayloadPayloadTypeKey;
extern NSString *const kSCHPushPayloadPayloadTypeActivityKey;

extern NSString *const kSCHPushPayloadActivityTypeKey;
extern NSString *const kSCHPushPayloadActivityLikeKey;
extern NSString *const kSCHPushPayloadActivityCommentKey;
extern NSString *const kSCHPushPayloadActivityFollowKey;

extern NSString *const kSCHPushPayloadFromUserObjectIdKey;
extern NSString *const kSCHPushPayloadToUserObjectIdKey;
extern NSString *const kSCHPushPayloadPhotoObjectIdKey;
