//
//  LoginViewController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    
        _username = [[UITextField alloc] initWithFrame:CGRectMake(20, 100, 280, 40)];
        _username.delegate = self;
        [_username setBackgroundColor:[UIColor lightGrayColor]];
        [_username setPlaceholder:@"Username"];
        [_username setTextAlignment:NSTextAlignmentCenter];
        [_username setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [_username setAutocorrectionType:UITextAutocorrectionTypeNo];
        _username.layer.cornerRadius = 5;
        [self.view addSubview: _username];

        _password = [[UITextField alloc] initWithFrame:CGRectMake(20, 150, 280, 40)];
        _password.delegate = self;
        [_password setBackgroundColor:[UIColor lightGrayColor]];
        [_password setPlaceholder:@"Password"];
        [_password setSecureTextEntry:YES];
        [_password setTextAlignment:NSTextAlignmentCenter];
        [_password setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [_password setAutocorrectionType:UITextAutocorrectionTypeNo];
        _password.layer.cornerRadius = 5;
        [self.view addSubview: _password];
        
        _loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_loginButton setFrame:CGRectMake(20, 200, 280, 40)];
        _loginButton.layer.cornerRadius = 10;
        [_loginButton setTitle:@"Login" forState:UIControlStateNormal];
        [_loginButton setBackgroundColor:[UIColor lightGrayColor]];
        [_loginButton addTarget:self action:@selector(didTapParseLogin) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_loginButton];
        
        _facebookButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_facebookButton setFrame:CGRectMake(20, 300, 135, 40)];
        _facebookButton.layer.cornerRadius = 10;
        [_facebookButton setTitle:@"Facebook" forState:UIControlStateNormal];
        [_facebookButton setBackgroundColor:[UIColor lightGrayColor]];
        [_facebookButton addTarget:self action:@selector(didTapFacebookLogin) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_facebookButton];

        _twitterButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_twitterButton setFrame:CGRectMake(165, 300, 135, 40)];
        _twitterButton.layer.cornerRadius = 10;
        [_twitterButton setTitle:@"Twitter" forState:UIControlStateNormal];
        [_twitterButton setBackgroundColor:[UIColor lightGrayColor]];
        [_twitterButton addTarget:self action:@selector(didTapTwitterLogin) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_twitterButton];
        
        _forgotPassword = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_forgotPassword setFrame:CGRectMake(20, 250, 280, 40)];
        _forgotPassword.layer.cornerRadius = 10;
        [_forgotPassword setTitle:@"Forgot Password" forState:UIControlStateNormal];
        [_forgotPassword setBackgroundColor:[UIColor lightGrayColor]];
        [_forgotPassword addTarget:self action:@selector(forgotPasswordButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_forgotPassword];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)]];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void) forgotPasswordButtonPressed {
    UIAlertView *resetPasswordAlertView = [[UIAlertView alloc] initWithTitle:@"Reset Password" message:@"Enter your email address" delegate:self cancelButtonTitle:@"Enter" otherButtonTitles:nil];
    [resetPasswordAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [resetPasswordAlertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView.title isEqualToString:@"Reset Password"]) {
        if (buttonIndex == 0) { // cancel button pressed
            [PFUser requestPasswordResetForEmailInBackground:[alertView textFieldAtIndex:0].text block:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [[[UIAlertView alloc] initWithTitle:@"Success" message:@"We have sent you an email about resetting your password" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:@"%@", error] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                }
            }];
        }
    }
}

- (void) didTapParseLogin {
    
    [self dismissKeyboard];
    
    if ([[_username text] length] < 4 || [[_password text] length] < 4) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Entry" message:@"Username and Password must both be at least 4 characters long." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    } else {
        [PF_MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [PFUser logInWithUsernameInBackground:[_username text] password:[_password text] block:^(PFUser *user, NSError *error) {
            
            
            [PF_MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
            if (user) {
                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                NSLog(@"%@",error);
                [[[UIAlertView alloc] initWithTitle:@"Login Failed." message:@"Invalid Username and/or Password." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
            }
        }];
    }
}

- (void)didTapFacebookLogin  {
    // The permissions requested from the user
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location", @"email" ];
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
            }
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");

            if ([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
				// Send request to Facebook
				[[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
					if (!error) {
						
						NSDictionary *userData = (NSDictionary *)result;
						[[NSNotificationCenter defaultCenter] postNotificationName:@"getUsername" object:userData[@"name"]];
							
                        NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", userData[@"id"]]];
                        NSData *pictureURLData = [[NSData alloc] initWithContentsOfURL:pictureURL];
							
                        NSURL *coverURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@?fields=cover&return_ssl_resources=1", userData[@"id"]]];
                        NSData *coverURLData = [[NSData alloc] initWithContentsOfURL:coverURL];
                        NSDictionary* coverJson = [NSJSONSerialization JSONObjectWithData:coverURLData options:kNilOptions error:&error];
                        NSURL *coverUrl = [[NSURL alloc] initWithString:coverJson[@"cover"][@"source"]];
                        NSData *coverPhotoData = [[NSData alloc] initWithContentsOfURL:coverUrl];
							
                        PFObject *userObject = [PFUser currentUser];
                        NSLog(@"%@", userObject);
                        PFQuery *user = [PFUser query];

                        [user getObjectInBackgroundWithId:[userObject objectId] block:^(PFObject *userObject, NSError *error) {
                            if (!error) {
                                
                                [userObject setObject:userData[@"last_name"] forKey:@"lastName"];
                                [userObject setObject:userData[@"first_name"] forKey:@"firstName"];
                                [userObject setObject:userData[@"username"] forKey:@"username"];
                                [userObject setObject:userData[@"email"] forKey:@"email"];
                                [userObject setObject:userData[@"gender"] forKey:@"gender"];
                                [userObject setObject:userData[@"birthday"] forKey:@"birthday"];
                                [userObject setObject:userData[@"location"][@"name"] forKey:@"location"];
                                [userObject setObject:[PFFile fileWithName:@"coverPicture" data:coverPhotoData] forKey:@"coverPicture"];
                                [userObject setObject:[PFFile fileWithName:@"profilePicture" data:pictureURLData] forKey:@"profilePicture"];
                                
                                
                                [userObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                    if (succeeded) {
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                        // After a user registers, we gain access to the users calendars
                                        // then we access all events of all calendars from the current user
                                        // then push each event to Parse using this method declared in CalendarStore.h.
                                        _HUD = [PF_MBProgressHUD showHUDAddedTo:self.presentingViewController.view animated:YES];
                                        _HUD.labelText = @"Signing Up...";
                                        [_HUD show:YES];
                                        [CalendarStore getEventsForSchedules:userObject block:^(BOOL finished, NSError *error) {
                                            [_HUD hide:YES];
                                        }];
                                    } else {
                                        NSLog(@"%@", error);
                                    }
                                }];
                                
                            } else {
                                [[[UIAlertView alloc] initWithTitle:@"Error Getting User Information!" message:[NSString stringWithFormat:@"%@", error] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                            }
                        }];
                        
					} else if ([error.userInfo[FBErrorParsedJSONResponseKey][@"body"][@"error"][@"type"] isEqualToString:@"OAuthException"]) {
						// Since the request failed, we can check if it was due to an invalid session
						NSLog(@"The facebook session was invalidated");
						[PF_MBProgressHUD hideHUDForView:self.view animated:YES];
					} else {
						NSLog(@"Some other error: %@", error);
						[PF_MBProgressHUD hideHUDForView:self.view animated:YES];
					}
				}];
            }
        } else {
            NSLog(@"User with facebook logged in!");
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
}

- (void)didTapTwitterLogin  {
    NSLog(@"Twitter");
    
    [PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Twitter login.");
            return;
        } else if (user.isNew) {
            [self dismissViewControllerAnimated:YES completion:nil];
            NSLog(@"User signed up and logged in with Twitter!");
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
            NSLog(@"User logged in with Twitter!");
        }
    }];
}


- (void)dismissKeyboard {
    [_username resignFirstResponder];
    [_password resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
