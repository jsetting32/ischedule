//
//  CustomHeaderView.m
//  MYSCHEDULER
//
//  Created by John Setting on 9/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CustomHeaderView.h"

@implementation CustomHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        
		_leftTitleLabel = [[UILabel alloc] init];
        _leftTitleLabel.textAlignment = NSTextAlignmentLeft;
        _leftTitleLabel.opaque = NO;
        _leftTitleLabel.backgroundColor = [UIColor clearColor];
        _leftTitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        _leftTitleLabel.textColor = [UIColor whiteColor];
        _leftTitleLabel.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _leftTitleLabel.shadowOffset = CGSizeMake(0, -1);
        [self addSubview:_leftTitleLabel];
		
		_rightTitleLabel = [[UILabel alloc] init];
        _rightTitleLabel.textAlignment = NSTextAlignmentRight;
        _rightTitleLabel.opaque = NO;
        _rightTitleLabel.backgroundColor = [UIColor clearColor];
        _rightTitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        _rightTitleLabel.textColor = [UIColor whiteColor];
        _rightTitleLabel.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        _rightTitleLabel.shadowOffset = CGSizeMake(0, -1);
        [self addSubview:_rightTitleLabel];
    
    }
    return self;
}

-(void) layoutSubviews
{
    CGFloat coloredBoxMargin = 6.0;
    CGFloat coloredBoxHeight = 40.0;
    self.coloredBoxRect = CGRectMake(coloredBoxMargin, coloredBoxMargin, self.bounds.size.width-coloredBoxMargin*2, coloredBoxHeight);
	
    CGFloat paperMargin = 9.0;
    self.paperRect = CGRectMake(paperMargin, CGRectGetMaxY(self.coloredBoxRect), self.bounds.size.width-paperMargin*2, self.bounds.size.height-CGRectGetMaxY(self.coloredBoxRect));
	
	self.leftTitleLabel.frame = CGRectMake(coloredBoxMargin + 5, coloredBoxMargin, self.bounds.size.width-coloredBoxMargin*2 - 10, coloredBoxHeight);
	self.rightTitleLabel.frame = CGRectMake(coloredBoxMargin + 5, coloredBoxMargin, self.bounds.size.width-coloredBoxMargin*2 - 10, coloredBoxHeight);
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	
    UIColor * whiteColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    UIColor * shadowColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5];
	
    CGContextSetFillColorWithColor(context, whiteColor.CGColor);
    CGContextFillRect(context, _paperRect);
	
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, CGSizeMake(0, 2), 3.0, shadowColor.CGColor);
    CGContextSetFillColorWithColor(context, self.lightColor.CGColor);
    CGContextFillRect(context, self.coloredBoxRect);
    CGContextRestoreGState(context);
	
	drawGlossAndGradient(context, self.coloredBoxRect, self.lightColor.CGColor, self.darkColor.CGColor);
	
    CGContextSetStrokeColorWithColor(context, self.darkColor.CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextStrokeRect(context, rectFor1PxStroke(self.coloredBoxRect));
}


@end
