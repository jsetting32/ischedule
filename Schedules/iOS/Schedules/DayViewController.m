//
//  DayViewController.m
//  iSchedj
//
//  Created by John Setting on 3/10/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "DayViewController.h"
#import "DayView.h"
#import "WeekView.h"
#import "SWRevealViewController.h"
#import "CalendarDataSource.h"
#import "ListTableViewCell.h"
#import "DayHeaderView.h"
#import "CarouselView.h"
#import "AppDelegate.h"
#import <EventKitUI/EventKitUI.h>


@interface DayViewController () <CalendarDataSourceDelegate, DayViewDelegate, EKEventViewDelegate, EKEventEditViewDelegate, UITableViewDelegate, UITableViewDataSource, CarouselViewDelegate, EKCalendarChooserDelegate>
@property (nonatomic) CalendarDataSource *dataSource;
@property (nonatomic) DayView *dayView;
@property (nonatomic) WeekView *weekView;
@property (nonatomic) UITableView *listView;
@property (nonatomic) CarouselView *carouselView;
@property (nonatomic) DayHeaderView *headerView;
@property BOOL firstTimeShown;
@end

static BOOL debug = FALSE;

@implementation DayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration {
    
    if (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        [self.navigationController setNavigationBarHidden:NO];
        [self.view addSubview:self.dayView];
        [self.view addSubview:self.carouselView];
        [self.weekView removeFromSuperview];
    } else {
        [self loadWeekView];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    [self loadDataSource];
    [self setupRevealController];
    [self loadDayView];
    [self loadCaroselView];
    [self loadListView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![PFUser currentUser]) [(AppDelegate *)[[UIApplication sharedApplication] delegate] presentLoginViewController:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([PFUser currentUser])
        [self cloudGetFriends];
}

#pragma mark - Event Datasource
- (void)loadDataSource
{
	self.dataSource = [CalendarDataSource sharedInstance];
	[self.dataSource setDelegate:self];
	[self.dataSource loadEventsWithCalendars:nil];
}

#pragma mark - Load SubViews
- (void)loadDayView
{
    self.dayView = [[DayView alloc] initWithFrame:self.view.frame];
    [self.dayView setDelegate:self];
    [self.view addSubview:self.dayView];
}

- (void) drawLine
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([CalendarDataSource isDateInToday:[self.dataSource date]]) {
            [self.dayView drawTimeLineWithLabel:[NSDate date]];
            return;
        }
        [self.dayView removeTimeLineWithLabel];
    });
}

- (void)loadCaroselView
{
    self.carouselView = [[CarouselView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x - 1, 64, self.view.frame.size.width + 2, 65)];
    [self.carouselView setDelegate:self];
    [self.carouselView setBackgroundColor:[UIColor whiteColor]];
    [self.carouselView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    [self.carouselView.layer setBorderWidth:.5f];
    [self.view addSubview:self.carouselView];
}

- (void)loadListView
{
    self.listView = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, 60, self.view.frame.size.width, self.view.frame.size.height - 60)];
    [self.listView setDelegate:self];
    [self.listView setDataSource:self];
}


- (void)loadWeekView
{
    self.weekView = [[WeekView alloc] initWithFrame:self.view.frame];
    [self.weekView setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController setNavigationBarHidden:YES];
    [self.view addSubview:self.weekView];
}


- (void)setupRevealController {
	//self.title = NSLocalizedString(@"iSchedj", nil);
    
    //[[self revealViewController] panGestureRecognizer];
    [[self revealViewController] tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:[self revealViewController] action:@selector(revealToggle:)];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addEvent:)];
    UIBarButtonItem *leftRevealButtonItem1 = [[UIBarButtonItem alloc] initWithTitle:@"Today" style:UIBarButtonItemStyleDone target:self action:@selector(todayButtonSelected:)];
    self.navigationItem.leftBarButtonItems = @[revealButtonItem,leftRevealButtonItem1, addButton];
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:[self revealViewController] action:@selector(rightRevealToggle:)];
	self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    UIBarButtonItem *rightRevealButtonItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(loadListView:)];
    UIBarButtonItem *rightRevealButtonItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(loadCalendars)];
    self.navigationItem.rightBarButtonItems = @[rightRevealButtonItem, rightRevealButtonItem2,rightRevealButtonItem1];
    

}

#pragma mark - Load List View or Load Day View
- (void)loadListView:(id)sender
{
    [self.carouselView removeFromSuperview];
    [self.dayView removeFromSuperview];
    [self.view addSubview:self.listView];

    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:[self revealViewController] action:@selector(rightRevealToggle:)];
	self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    UIBarButtonItem *rightRevealButtonItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(loadDayView:)];
    UIBarButtonItem *rightRevealButtonItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(loadCalendars)];
    self.navigationItem.rightBarButtonItems = @[rightRevealButtonItem, rightRevealButtonItem2,rightRevealButtonItem1];

}


- (void)loadDayView:(id)sender
{
    [self.listView removeFromSuperview];
    [self.view addSubview:self.dayView];
    [self.view addSubview:self.carouselView];

    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:[self revealViewController] action:@selector(rightRevealToggle:)];
	self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    UIBarButtonItem *rightRevealButtonItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(loadListView:)];
    UIBarButtonItem *rightRevealButtonItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(loadCalendars)];
    self.navigationItem.rightBarButtonItems = @[rightRevealButtonItem, rightRevealButtonItem2,rightRevealButtonItem1];

}

- (void)todayButtonSelected:(UIBarButtonItem *)sender
{
	dispatch_async(dispatch_get_main_queue(), ^{
        [self.dataSource setSelectedDate:[NSDate date]];
        [self.dayView loadEventsIntoDayView:[self.dataSource eventsByGivenDate:[self.dataSource date]]];
        [self.carouselView setWeek:[CalendarDataSource getDaysOfWeekGivenADayDate:[self.dataSource date]] withDate:[self.dataSource date]];
        [self.listView reloadData];
        //if ([[self.dataSource events] count] != 0) {
        //    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:[self.dataSource returnTodaySectionHeaderNumber]];
        //    [self.listView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
        //}
    });
}

- (void)loadCalendars
{
    [self.dataSource loadCalendars:self];
}

- (void)addEvent:(UIBarButtonItem *)sender
{
    EKEventEditViewController *eventController = [[EKEventEditViewController alloc] init];
    eventController.eventStore = [self.dataSource store];
    eventController.editViewDelegate = self;
    [self.navigationController presentViewController:eventController animated:YES completion:nil];
}

#pragma mark - Event View Controller Delegate Methods
- (void)eventViewController:(EKEventViewController *)controller didCompleteWithAction:(EKEventViewAction)action
{
    if (debug) NSLog(@"eventViewController:%@\ndidCompleteWithAction:%u", controller, action);

    if (action == EKEventViewActionDeleted) {
        [self.dataSource deleteEvent:controller.event];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    if (debug) NSLog(@"eventEditViewController:%@\ndidCompleteWithAction:%u", controller, action);

    if (action == EKEventEditViewActionSaved) {
        [self.dataSource addEvent:controller.event];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else if (action == EKEventEditViewActionDeleted) {
        [self.dataSource deleteEvent:controller.event];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else if (action == EKEventEditViewActionCancelled) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - DayView Delegate Methods
- (void)didClickOnEvent:(DayViewCell *)tappedDay
{
    if (debug) NSLog(@"didClickOnEvent:%@", tappedDay);
    EKEventViewController *eventViewController = [[EKEventViewController alloc] init];
    eventViewController.event = [self.dataSource eventByIndentifier:[[tappedDay getInfo] objectAtIndex:0]];
    eventViewController.delegate = self;
    eventViewController.allowsEditing = YES;
    [self.navigationController pushViewController:eventViewController animated:YES];
}


- (void)didHoldPressEvent:(DayViewCell *)tappedDay
{
    if (debug) NSLog(@"didHoldPressEvent:%@", tappedDay);
}


- (void)didAddEvent:(DayViewCell *)dayViewCell
{
    EKEventEditViewController *eventViewController = [[EKEventEditViewController alloc] init];
    eventViewController.eventStore = [self.dataSource store];
    eventViewController.editViewDelegate = self;
    eventViewController.event = [[dayViewCell getInfo] objectAtIndex:5];
    [self.navigationController presentViewController:eventViewController animated:YES completion:nil];
    
}

- (void)didFinishPanning:(DayViewCell *)tappedDay
{
    if (debug) NSLog(@"Did finish panning");
}

- (void)calendarsDidFinishLoading:(id)calendars
{
    if (debug) NSLog(@"calendarsDidFinishLoading:%@", calendars);
    [self loadCalendarChooser];
}

#pragma mark - Carousel Delegate Methods
- (void)didTapDateButton:(NSArray *)dayInfo
{
    if (debug) NSLog(@"didTapDateButton:%@", dayInfo);
    NSArray *info = [[CalendarDataSource getDaysOfWeekGivenADayDate:[self.dataSource date]] objectAtIndex:[[dayInfo objectAtIndex:1] integerValue]];
    NSString *stringDate = [info objectAtIndex:2];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    NSDate *date = [CalendarDataSource dateAtBeginningOfDayForDate:[dateFormatter dateFromString:stringDate]];
    [self.dayView loadEventsIntoDayView:[self.dataSource eventsByGivenDate:date]];
    [self drawLine];
    [self.carouselView setFullDateLabel:date];
    [self.dataSource setSelectedDate:date];
}

- (void)didSwipeLeft:(UISwipeGestureRecognizer *)sender
{
    if (debug) NSLog(@"didSwipeLeft:%@", sender);
    [self.dataSource setSelectedDate:[CalendarDataSource dateByFutureWeek:[self.dataSource date]]];
    [self drawLine];
    [self.carouselView setOtherWeek:[CalendarDataSource getDaysOfWeekGivenADayDate: [self.dataSource date]] withDate:[self.dataSource date]];
}


- (void)didSwipeRight:(UISwipeGestureRecognizer *)sender
{
    if (debug) NSLog(@"didSwipeRight:%@", sender);
    [self.dataSource setSelectedDate:[CalendarDataSource dateByPreviousWeek:[self.dataSource date]]];
    [self drawLine];
    [self.carouselView setOtherWeek:[CalendarDataSource getDaysOfWeekGivenADayDate: [self.dataSource date]] withDate:[self.dataSource date]];

}

#pragma mark - Calendar Chooser
- (void)loadCalendarChooser
{
    EKCalendarChooser *chooser = [[EKCalendarChooser alloc] initWithSelectionStyle:EKCalendarChooserSelectionStyleMultiple displayStyle:EKCalendarChooserDisplayAllCalendars eventStore:[self.dataSource store]];
    [chooser setDelegate:self];
    [chooser setShowsCancelButton:YES];
    [chooser setShowsDoneButton:YES];
    [chooser setSelectedCalendars:[NSSet setWithArray:[self.dataSource chosenCalendars]]];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:chooser] animated:YES completion:nil];
}

- (void)dismissViewController:(UINavigationController *)sender{
    [sender dismissViewControllerAnimated:YES completion:nil];
}

- (void)calendarChooserDidFinish:(EKCalendarChooser *)calendarChooser
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.dataSource setCalendars:calendarChooser.selectedCalendars];
        [self.dayView loadEventsIntoDayView:[self.dataSource eventsByGivenDate:[self.dataSource date]]];
        [self.carouselView setWeek:[CalendarDataSource getDaysOfWeekGivenADayDate:[self.dataSource date]] withDate:[self.dataSource date]];
        [self.listView reloadData];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)calendarChooserDidCancel:(EKCalendarChooser *)calendarChooser
{
    [self.dataSource setCalendars:calendarChooser.selectedCalendars];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Calendar DataSource Delegate Methods
- (void)eventsDidStartLoading:(NSDate *)fromDate toDate:(NSDate *)toDate {
    if (debug) NSLog(@"eventsDidStartLoading:%@\n toDate:%@", fromDate, toDate);
}

- (void)eventsDidFailWithError:(NSError *)error {
    if (debug) NSLog(@"eventsDidFailWithError:%@", error);
}

- (void)eventsDidFinishLoading:(NSArray *)events
{
    if (debug) NSLog(@"eventsDidFinishLoading:%@", events);
	dispatch_async(dispatch_get_main_queue(), ^{
        [self.dayView loadEventsIntoDayView:[self.dataSource eventsByGivenDate:[self.dataSource date]]];
        [self.carouselView setWeek:[CalendarDataSource getDaysOfWeekGivenADayDate:[self.dataSource date]] withDate:[self.dataSource date]];
        [self.listView reloadData];
//        if ([[self.dataSource events] count] != 0) {
//            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:[self.dataSource returnTodaySectionHeaderNumber]];
//            [self.listView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
//        }
    });
}

- (void)eventDidFinishSaving:(EKEvent *)event
{
    if (debug) NSLog(@"eventDidFinishSaving:%@", event);
    [self.dayView didAddEventToView:event];
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.listView reloadData];
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:[self.dataSource returnTodaySectionHeaderNumber]];
        [self.listView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    });
}

- (void)eventDidFinishDeleting:(EKEvent *)event
{
    if (debug) NSLog(@"eventDidFinishDeleting:%@", event);
    [self.dayView didDeleteEventFromView:event];
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.listView reloadData];
	});
}


#pragma mark - UITableView Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	ListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
        cell = [[ListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [cell setEventForCell:[self.dataSource eventAtIndexPath:indexPath]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.listView deselectRowAtIndexPath:[self.listView indexPathForSelectedRow] animated:YES];
    EKEventViewController *eventViewController = [[EKEventViewController alloc] init];
    [eventViewController setEvent:[self.dataSource eventAtIndexPath:indexPath]];
    eventViewController.allowsEditing = YES;
    [eventViewController setDelegate:self];
    [self.navigationController pushViewController:eventViewController animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor whiteColor]];
    [view setFrame:CGRectMake(0, 0, 320, 25)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:view.frame];
    [label setText:[NSString stringWithFormat:@"  %@",[self.dataSource returnSectionHeaderText:section]]];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    
    if ([self.dataSource returnTodaySectionHeaderNumber] == section)
        [label setTextColor:[UIColor orangeColor]];
    
    [label setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.2f]];
    [view addSubview:label];
    
    return view;
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (debug) NSLog(@"numberOfSectionsInTableView:%i", [self.dataSource returnNumberOfSections]);
    return [self.dataSource returnNumberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (debug) NSLog(@"tableView:numberOfRowsInSection:%i", [self.dataSource returnNumberOfRows:section]);
    return [self.dataSource returnNumberOfRows:section];
}

- (void) cloudGetFriends {
    NSLog(@"Running Cloud Code");
    
    [PFCloud callFunctionInBackground:@"getFriends" withParameters:@{} block:^(id friends, NSError *error) {
        if (!error) {
            NSLog(@"%@", friends);
            /****
             
             This shouldnt have to be here, but we will have this for the time being...
             
             In this process I have to parse the object returned that are within the Activity
             Class (in the Data Browser) and of type ("friend") and where either fromUser or
             toUser is the current user's id. This object is 'id friends' which at the moment
             is a __NSArrayM_ with multiple PFObjects, when they should be PFUsers but here
             we do that.
             
             ****/
            
            
            NSMutableArray *array = [NSMutableArray array];
            for (PFObject *friend in friends) {
                if ([[friend[@"fromUser"]objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                    [array addObject:[friend[@"toUser"] objectId]];
                } else if ([[friend[@"toUser"]objectId] isEqualToString:[[PFUser currentUser] objectId]]) {
                    [array addObject:[friend[@"fromUser"] objectId]];
                }
            }
            
            [[Cache sharedCache] setFriends:array];
            
        } else {
            NSLog(@"%@", error);
        }
    }];
}

- (void)loadObjects
{
    NSLog(@"Loading Objects");
}


#pragma mark - Memory Management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

    [[[UIAlertView alloc] initWithTitle:@"Memory Warning" message:@"Too much memory has been cached" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:@"Cancel", nil] show];
}

@end
