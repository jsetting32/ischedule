//
//  SCHProfileImageView.h
//  Schedules
//
//  Created by John Setting on 11/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

@class PFImageView;
@interface SCHProfileImageView : UIView

@property (nonatomic, strong) UIButton *profileButton;
@property (nonatomic, strong) PFImageView *profileImageView;

- (void)setFile:(PFFile *)file;

@end
