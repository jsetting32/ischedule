//
//  LoginTabBarController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "LoginTabBarController.h"
#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "TutorialViewController.h"

@interface LoginTabBarController () <UITabBarControllerDelegate>
@property (nonatomic) BOOL isAnimating;
@property (strong, nonatomic) UILabel *appTitle;
@property (strong, nonatomic) LoginViewController *login;
@property (strong, nonatomic) SignUpViewController *signUp;
@property (strong, nonatomic) TutorialViewController *tutorial;
@end

@implementation LoginTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor = [UIColor blackColor];
        self.delegate = self;
        
        // Custom initialization
        _appTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 80)];
        [_appTitle setText:@"Schedules"];
        [_appTitle setFont:[UIFont fontWithName:@"Chalkduster" size:24.0f]];
        [_appTitle setTextColor:[UIColor orangeColor]];
		[_appTitle setShadowOffset:CGSizeMake(1.0f, 2.0f)];
		[_appTitle setShadowColor:[UIColor yellowColor]];
        [_appTitle setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview: _appTitle];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    
    self.signUp = [[SignUpViewController alloc] init];
    UIImage *signUpImage = [[UIImage imageNamed:@"SignUp.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *signUpImageItem = [[UITabBarItem alloc] initWithTitle:@"Sign Up" image:signUpImage tag:0];
    [self.signUp setTabBarItem:signUpImageItem];
    [controllers addObject:self.signUp];
    
    self.login = [[LoginViewController alloc] init];
    UIImage *loginImage = [[UIImage imageNamed:@"Login.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *loginImageItem = [[UITabBarItem alloc] initWithTitle:@"Login" image:loginImage tag:1];
    [self.login setTabBarItem:loginImageItem];
    [controllers addObject:self.login];
    
    self.tutorial = [[TutorialViewController alloc] init];
    UIImage *tutorialImage = [[UIImage imageNamed:@"Tutorial.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *tutorialImageItem = [[UITabBarItem alloc] initWithTitle:@"Tutorial" image:tutorialImage tag:2];
    [self.tutorial setTabBarItem:tutorialImageItem];
    [controllers addObject:self.tutorial];
    
    self.viewControllers = controllers;
    [self setSelectedIndex:1];
    [self createGradientBackground];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) createGradientBackground
{
    UIColor *colorOne = [UIColor colorWithRed:(120/255.0) green:(135/255.0) blue:(150/255.0) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(57/255.0)  green:(79/255.0)  blue:(96/255.0)  alpha:1.0];
    CAGradientLayer *bgLayer = [GradientLayer gradientClash:colorOne colorTwo:colorTwo];
    bgLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:bgLayer atIndex:0];
}


#pragma mark - Sliding Animation (between tabs)
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSInteger controllerIndex = [[tabBarController viewControllers] indexOfObject:viewController];
    
    if(controllerIndex == self.selectedIndex || self.isAnimating) {
        return NO;
    }
    
    // Get the views.
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = [viewController view];
    
    // Get the size of the view area.
    CGRect viewSize = fromView.frame;
    BOOL scrollRight = controllerIndex > tabBarController.selectedIndex;
    
    // Add the to view to the tab bar view.
    [fromView.superview addSubview:toView];
    
    // Position it off screen.
    toView.frame = CGRectMake((scrollRight ? 320 : -320), viewSize.origin.y, 320, viewSize.size.height);
    
    [UIView animateWithDuration:0.2 animations: ^{
        // Animate the views on and off the screen. This will appear to slide.
        fromView.frame = CGRectMake((scrollRight ? -320 : 320), viewSize.origin.y, 320, viewSize.size.height);
        toView.frame = CGRectMake(0, viewSize.origin.y, 320, viewSize.size.height);
        self.isAnimating = YES;
    } completion:^(BOOL finished) {
        
        if (finished) {
            // Remove the old view from the tabbar view.
            [fromView removeFromSuperview];
            tabBarController.selectedIndex = controllerIndex;
            self.isAnimating = NO;
        }
    }];
    
    return NO;
}

#pragma mark - DismissViewControllerAnimated override method
- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    [UIView animateWithDuration:1.5 animations:^{
        self.view.alpha = 0;
    } completion:^(BOOL b){
        self.view.alpha = 1;
        [super dismissViewControllerAnimated:flag completion:completion];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
