//
//  DayViewCell.m
//  iSchedj
//
//  Created by John Setting on 3/20/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "DayViewCell.h"
#import "CalendarDataSource.h"

#define FONT_SIZE 12.0

static BOOL shouldAllowPan = YES;

@interface DayViewCell() <UIGestureRecognizerDelegate>
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) NSString *identifier;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *startDate;
@property (nonatomic) NSString *endDate;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UILabel *locationLabel;
@property (nonatomic) UIView *edgeView;
@property (nonatomic) EKEvent *event;
@end

@implementation DayViewCell

#pragma mark Init & Friends
+ (DayViewCell*) eventView
{
	DayViewCell *event = [[DayViewCell alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
	return event;
}

+ (DayViewCell *)eventViewWithEvent:(EKEvent *)event
{
    DayViewCell *cell = [[DayViewCell alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
	cell.identifier = [event eventIdentifier];
	cell.startDate = [[CalendarDataSource convertDates:event] objectAtIndex:1];
	cell.endDate = [[CalendarDataSource convertDates:event] objectAtIndex:2];
	cell.titleLabel.text = [event title];
	cell.locationLabel.text = [event location];
    cell.imageView.image = [CalendarDataSource eventCalendarColor:event];
    return cell;
}

+ (DayViewCell *)eventViewWIthEventfulEvent:(EventfulEvent *)event
{
    DayViewCell *cell = [[DayViewCell alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    return cell;
}

- (NSArray *)getInfo
{
    return [NSArray arrayWithObjects:self.identifier, self.startDate, self.endDate, self.titleLabel, self.locationLabel, self.event,nil];
}


- (id) initWithFrame:(CGRect)frame
{
    if(!(self=[super initWithFrame:frame])) return nil;
    [self _setupView];
    return self;
}
- (id) initWithCoder:(NSCoder *)decoder
{
    if(!(self=[super initWithCoder:decoder])) return nil;
    [self _setupView];
	return self;
}

- (void)didTapView:(UITapGestureRecognizer *)sender
{
    [self.delegate didTapEventView:self recognizer:sender];
}

- (void)didPanView:(UIPanGestureRecognizer *)sender
{
    if(shouldAllowPan) {
        [self.delegate didPanEventView:self recognizer:sender];
    } else if(sender.state == UIGestureRecognizerStateEnded || sender.state == UIGestureRecognizerStateFailed || sender.state == UIGestureRecognizerStateCancelled) {
        shouldAllowPan = NO;
    }
}


- (void)didLongPressEventView:(UILongPressGestureRecognizer *)sender
{
    if(UIGestureRecognizerStateBegan == sender.state) {
        shouldAllowPan = NO;
    }
    
    if(UIGestureRecognizerStateChanged == sender.state) {
        shouldAllowPan = YES;
        [self.delegate didLongPressEventView:self recognizer:sender];
    }
}

- (void)addGestures
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapView:)];
    [tapGesture setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapGesture];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPressEventView:)];
    longPressGesture.delegate = self;
    [longPressGesture setNumberOfTouchesRequired:1];
    [longPressGesture setMinimumPressDuration:.25];
    [self addGestureRecognizer:longPressGesture];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPanView:)];
    panGesture.delegate = self;
    [self addGestureRecognizer:panGesture];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {

    if([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        return NO;
    }
    return YES;
}

- (void) _setupView
{
    [self addGestures];
    
	self.alpha = 1;
    
	CGRect r = CGRectInset(self.bounds, 5, 22);
	r.size.height = 14;
	r.origin.y = 5;
    
	self.titleLabel = [[UILabel alloc] initWithFrame:r];
	[self.titleLabel setNumberOfLines:2];
	[self.titleLabel setBackgroundColor:[UIColor clearColor]];
	[self.titleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.titleLabel setFont:[UIFont boldSystemFontOfSize:10]];
	[self addSubview:self.titleLabel];
    
    
	r.origin.y = 20;
	r.size.height = 14*2;
    
	self.locationLabel = [[UILabel alloc] initWithFrame:r];
	[self.locationLabel setNumberOfLines:2];
	[self.locationLabel setBackgroundColor:[UIColor clearColor]];
	[self.locationLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	[self.locationLabel setFont:[UIFont systemFontOfSize:8]];
	[self addSubview:self.locationLabel];
    
    r.origin.y = 10;
	r.size.height = 14*2;
    
    /*
    [self.layer setCornerRadius:2.0f];
    [self.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.layer setBorderWidth:1.5f];
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.8];
    [self.layer setShadowRadius:3.0];
    [self.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    */
    
}

- (void)setEdgeColor:(UIColor *)color
{
    self.edgeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, CGRectGetHeight(self.frame))];
	self.edgeView.backgroundColor = color;
	self.edgeView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	[self addSubview:self.edgeView];
}

- (void) layoutSubviews {
	[super layoutSubviews];
    
	CGFloat cellHeight = CGRectGetHeight(self.frame);
	if (cellHeight < 45){
	
    }
    
    self.titleLabel.frame = CGRectInset(self.bounds, 5, 5);

	CGFloat heightPrime = cellHeight > 200 ? 14 * 2 : 14;
	CGRect frame = CGRectInset(self.bounds, 5, 5);
	frame.size.height = heightPrime;
	frame = CGRectIntersection(frame, self.bounds);
    
	[self.titleLabel setFrame:frame];
	[self.titleLabel sizeToFit];
    
	heightPrime = cellHeight > 200 ? (FONT_SIZE+2.0) * 2 : FONT_SIZE+2;
	frame = CGRectInset(self.bounds, 5, 5);
	frame.size.height = heightPrime;
	frame.origin.y += CGRectGetHeight(self.titleLabel.frame);
	frame = CGRectIntersection(frame, self.bounds);
    
	[self.locationLabel setFrame:frame];
	self.locationLabel.hidden = self.locationLabel.text.length > 0 ? NO : YES;
	[self.locationLabel sizeToFit];
    
}

- (CGFloat) contentHeight{
	if(!self.locationLabel.hidden && self.locationLabel.text.length > 0)
		return CGRectGetMaxY(self.locationLabel.frame) - 4;
    
	if(!self.titleLabel.hidden && self.titleLabel.text.length > 0)
		return CGRectGetMaxY(self.titleLabel.frame) - 4;
	return 0;
}


- (NSString *)identifier
{
    if (!_identifier) {
        _identifier = [NSString string];
    }
    return _identifier;
}


@end
