//
//  DayView.h
//  iSchedj
//
//  Created by John Setting on 3/10/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

/* 
 * The DayView class draws a calendar-like day view into a view when instantiated with a 
 * frame.
 *
 * Example:
 *
 * DayView *dayView = [[DayView alloc] initWithFrame:CGRectMake(10, 70, 300, 300)];
 * [self.view addSubview:dayView];
 *
 * To add events to the dayView, you must be the pass 
 *
 */

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

@class DayViewCell;
@protocol DayViewDelegate;

@interface DayView : UIView
@property (nonatomic, weak)id<DayViewDelegate>delegate;
- (void)loadEventsIntoDayView:(NSArray *)events;
- (void)didAddEventToView:(EKEvent *)event;
- (void)didDeleteEventFromView:(EKEvent *)event;
- (void)drawTimeLineWithLabel:(NSDate *)date;
- (void)removeTimeLineWithLabel;
@end

@protocol DayViewDelegate <NSObject>
- (void)didFinishPanning:(DayViewCell *)tappedDay;
- (void)didClickOnEvent:(DayViewCell *)tappedDay;
- (void)didAddEvent:(DayViewCell *)dayViewCell;
- (void)didHoldPressEvent:(DayViewCell *)tappedDay;
@end
