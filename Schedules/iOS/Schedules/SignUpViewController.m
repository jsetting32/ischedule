//
//  SignUpViewController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        _firstname = [[UITextField alloc] initWithFrame:CGRectMake(20, 100, 135, 40)];
        _firstname.delegate = self;
        [_firstname setBackgroundColor:[UIColor lightGrayColor]];
        [_firstname setPlaceholder:@"First Name"];
        [_firstname setTextAlignment:NSTextAlignmentCenter];
        _firstname.layer.cornerRadius = 5;
        [_firstname setTag:0];
        [self.view addSubview: _firstname];
        
        _lastname = [[UITextField alloc] initWithFrame:CGRectMake(165, 100, 135, 40)];
        _lastname.delegate = self;
        [_lastname setBackgroundColor:[UIColor lightGrayColor]];
        [_lastname setPlaceholder:@"Last Name"];
        [_lastname setTextAlignment:NSTextAlignmentCenter];
        _lastname.layer.cornerRadius = 5;
        [_lastname setTag:1];
       [self.view addSubview: _lastname];
        
        _email = [[UITextField alloc] initWithFrame:CGRectMake(20, 150, 280, 40)];
        _email.delegate = self;
        [_email setBackgroundColor:[UIColor lightGrayColor]];
        [_email setPlaceholder:@"Email Address"];
        [_email setTextAlignment:NSTextAlignmentCenter];
        _email.layer.cornerRadius = 5;
        [_email setTag:2];
       [self.view addSubview: _email];
        
        _username = [[UITextField alloc] initWithFrame:CGRectMake(20, 200, 280, 40)];
        _username.delegate = self;
        [_username setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [_username setAutocorrectionType:UITextAutocorrectionTypeNo];
        [_username setBackgroundColor:[UIColor lightGrayColor]];
        [_username setPlaceholder:@"Username"];
        [_username setTextAlignment:NSTextAlignmentCenter];
        _username.layer.cornerRadius = 5;
        [_username setTag:3];
        [self.view addSubview: _username];
        
        _password = [[UITextField alloc] initWithFrame:CGRectMake(20, 250, 280, 40)];
        _password.delegate = self;
        [_password setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [_password setAutocorrectionType:UITextAutocorrectionTypeNo];
        [_password setBackgroundColor:[UIColor lightGrayColor]];
        [_password setPlaceholder:@"Password"];
        [_password setTextAlignment:NSTextAlignmentCenter];
        _password.layer.cornerRadius = 5;
        [_password setTag:4];
        [self.view addSubview: _password];

        /* Here is an attempt to place a small padding to the left part of the text views /
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        _firstname.leftView = paddingView;
        _firstname.leftViewMode = UITextFieldViewModeAlways;
        _lastname.leftView = paddingView;
        _lastname.leftViewMode = UITextFieldViewModeAlways;
        _email.leftView = paddingView;
        _email.leftViewMode = UITextFieldViewModeAlways;
        _username.leftView = paddingView;
        _username.leftViewMode = UITextFieldViewModeAlways;
        _password.leftView = paddingView;
        _password.leftViewMode = UITextFieldViewModeAlways;
        / Here is an attempt to place a small padding to the left part of the text views */
        
        _signUpButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_signUpButton setFrame:CGRectMake(20, 300, 280, 40)];
        _signUpButton.layer.cornerRadius = 10;
        [_signUpButton setTitle:@"Sign Up" forState:UIControlStateNormal];
        [_signUpButton setBackgroundColor:[UIColor lightGrayColor]];
        [_signUpButton addTarget:self action:@selector(didTapSignup) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_signUpButton];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (void)didTapSignup {
    
    [self dismissKeyboard];
    
    NSString *firstname = [_firstname text];
    NSString *lastname = [_lastname text];
    NSString *email = [_email text];
    NSString *username = [_username text];
    NSString *password = [_password text];
    
    if ([firstname length] < 2 || [lastname length] < 2) {
        [[[UIAlertView alloc] initWithTitle:@"Invalid Entry" message:@"Your first and last name must both be at least 2 characters long." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
    } else if ([username length] < 4 || [password length] < 4) {
        [[[UIAlertView alloc] initWithTitle:@"Invalid Entry" message:@"Username and Password must both be at least 4 characters long." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
    } else if ([email length] < 8) {
        [[[UIAlertView alloc] initWithTitle:@"Invalid Entry" message:@"Please enter your email address." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
    } else {
        
        [PF_MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
        PFUser *newUser = [PFUser user];
        [newUser setObject:firstname forKey:@"firstName"];
        [newUser setObject:lastname forKey:@"lastName"];
        newUser.email = email;
        newUser.username = username;
        newUser.password = password;
        [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [PF_MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
            if (error) {
                NSString *errorString = [[error userInfo] objectForKey:@"error"];
                [[[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil]show];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
                
                // After a user registers, we gain access to the users calendars
                // then we access all events of all calendars from the current user
                // then push each event to Parse using this method declared in CalendarStore.h.
                _HUD = [PF_MBProgressHUD showHUDAddedTo:self.presentingViewController.view animated:YES];
                _HUD.labelText = @"Signing Up...";
                [_HUD show:YES];
                [CalendarStore getEventsForSchedules:newUser block:^(BOOL finished, NSError *error) {
                    if (finished) {
                        [_HUD hide:YES];
                    } else {
                        NSLog(@"%@", error);
                    }
                }];
            }
        }];
    }
    
}

#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0) {
        [self setViewMovedUp:YES];
    } else if (self.view.frame.origin.y < 0) {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0) {
        [self setViewMovedUp:YES];
    } else if (self.view.frame.origin.y < 0) {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender tag] == 0) {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0) {
            [self setViewMovedUp:NO];
        }
    } else if ([sender tag] == 1) {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0) {
            [self setViewMovedUp:YES];
        }
    } else if ([sender tag] == 2) {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0) {
            [self setViewMovedUp:YES];
        }
    } else if ([sender tag] == 3) {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0) {
            [self setViewMovedUp:YES];
        }
    } else if ([sender tag] == 4) {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0) {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp) {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    } else {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}



- (void)dismissKeyboard {
    [_firstname resignFirstResponder];
    [_lastname resignFirstResponder];
    [_email resignFirstResponder];
    [_username resignFirstResponder];
    [_password resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
