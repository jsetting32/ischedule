//
//  TutorialViewController.h
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CycleScrollView.h"

@interface TutorialViewController : UIViewController <CycleScrollViewDatasource, CycleScrollViewDelegate>

@end
