//
//  GeoLocation.m
//  MyScheduler
//
//  Created by John Setting on 2/21/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "GeoLocation.h"

@interface GeoLocation()
@property (nonatomic) id WOEID;
@property (nonatomic) CLLocation *currentLocation;
@property (nonatomic) float latitudeLocation;
@property (nonatomic) float longitudeLocation;
@property (nonatomic) NSDictionary *address;
@property (nonatomic) NSString *cityName;
@property (nonatomic) NSString *stateName;
@property (nonatomic) NSString *countryName;
@property (nonatomic) NSString *zipCode;
@property (nonatomic) NSString *location;

@end

@implementation GeoLocation
@synthesize locationManager;
@synthesize delegate;

- (id) init {
    if ((self = [super init]) == nil) return nil;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
    return self;
}

- (id)initWithCustomLocation:(NSString *)location {
    if ((self = [super init]) == nil) return nil;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.location = location;
    [self getWOEIDFromLocation:nil];
	return self;
}

- (void)getWOEIDFromLocation:(NSString *)location {
    NSString *query = [[NSString stringWithFormat:@"select * from geo.places where text=\"%@\"", self.location] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://query.yahooapis.com/v1/public/yql?q=%@&format=json&diagnostics=true&callback=", query]];
	NSError *error;
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url] options:kNilOptions error:&error];
    [self.delegate WOEIDupdate:[[[[[json objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"place"] objectForKey:@"locality1"] objectForKey:@"woeid"]];
    //NSLog(@"%@", [[[[[json objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"place"] objectForKey:@"locality1"] objectForKey:@"woeid"]);
}

- (void)getWOEID:(NSString *)city state:(NSString *)state
{
	NSString *query = [[NSString stringWithFormat:@"select * from geo.places where text=\"%@, %@\"", city, state] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://query.yahooapis.com/v1/public/yql?q=%@&format=json&diagnostics=true&callback=", query]];
	NSError *error;
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:url] options:kNilOptions error:&error];
	[self.delegate WOEIDupdate:[[[[[[json objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"place"] objectAtIndex:0] objectForKey:@"locality1"] objectForKey:@"woeid"]];
}


#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	[manager stopUpdatingLocation];
    [[[CLGeocoder alloc] init] reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
		if (error){
			NSLog(@"Geocode failed with error: %@", error);
			return;
		}
		
		CLPlacemark *placemark = [placemarks objectAtIndex:0];
		self.address = [placemark addressDictionary];
		self.cityName = [placemark locality];
		self.stateName = [placemark administrativeArea];
		self.countryName = [placemark ISOcountryCode];
		self.zipCode = [placemark postalCode];
		[self getWOEID:[placemark locality] state:[placemark administrativeArea]];
        [self.delegate didReceivePlacemark:placemark];
	}];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.delegate locationError:error];
	[manager stopUpdatingLocation];
}


@end
