//
//  SCHImageView.h
//  Schedules
//
//  Created by John Setting on 11/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//


@interface SCHImageView : UIImageView

@property (nonatomic, strong) UIImage *placeholderImage;

- (void) setFile:(PFFile *)file;

@end
