//
//  LeftViewController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "LeftViewController.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "DayViewController.h"
#import "ProfileViewController.h"
#import "UserTableViewCell.h"
#import "SearchedUserViewController.h"
#import "EventfulViewController.h"

@interface LeftViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic, retain) UITableView *mainTableView;
@property (nonatomic, retain) UISearchDisplayController *searchController;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *searchResults;
@end

@implementation LeftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.title = @"iSchedj";
        [self.view setBackgroundColor:[UIColor whiteColor]];
        // Custom initialization

        _searchResults = [NSMutableArray array];
        
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) style:UITableViewStylePlain];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView setShowsHorizontalScrollIndicator:NO];
        [_mainTableView setShowsVerticalScrollIndicator:NO];
        [_mainTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_mainTableView];

        _searchBar = [[UISearchBar alloc] initWithFrame:self.view.frame];
        [_searchBar setBarStyle:UIBarStyleDefault];
        [_searchBar setShowsCancelButton:NO];
        [_searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
        [_searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [_searchBar setDelegate:self];

        UISearchDisplayController *searchCon = [[UISearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
        _searchController = searchCon;
        [_searchController setDelegate:self];
        [_searchController setSearchResultsDataSource:self];
        [_searchController setSearchResultsDelegate:self];
        [_searchController setActive:NO animated:YES];
        [_mainTableView setTableHeaderView:_searchBar];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


#pragma mark - UITableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.mainTableView) {
        switch (section) {
            case 0:
                return 4;
                break;
            case 1:
                return 0;
                break;
            case 2:
                return 0;
                break;
            default:
                break;
        }
    } else if (tableView == self.searchDisplayController.searchResultsTableView) {
        return self.searchResults.count;
    }

    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableView == self.mainTableView) return 1;
    if (tableView == self.searchDisplayController.searchResultsTableView) return 1;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [[cell textLabel] setTextColor:[UIColor blackColor]];
	}
	
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        PFUser *obj2 = [self.searchResults objectAtIndex:indexPath.row];
        
        if (obj2[@"firstName"] && obj2[@"lastName"]) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", obj2[@"firstName"], obj2[@"lastName"]];
            cell.detailTextLabel.text = obj2[@"username"];
        } else {
            cell.textLabel.text = obj2[@"username"];
        }

        [obj2[@"profilePicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!data) {
                cell.imageView.image = [UIImage imageNamed:@"AvatarPlaceholder.png"];
            } else {
                cell.imageView.image = [UIImage imageWithData:data];
            }
            [self.searchDisplayController.searchResultsTableView reloadData];
        }];
        
        //CGFloat widthScale = 35 / cell.imageView.image.size.width;
        //CGFloat heightScale = 35 / cell.imageView.image.size.height;
        //cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
        //cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
        
    } else if (tableView == self.mainTableView) {
        switch (indexPath.section) {
                
            case 0:
                if (row == 0) {
                    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [PFUser currentUser][@"firstName"], [PFUser currentUser][@"lastName"]];
                    CGFloat widthScale = 35 / cell.imageView.image.size.width;
                    CGFloat heightScale = 35 / cell.imageView.image.size.height;
                    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
                    //cell.imageView.layer.cornerRadius = 40.0f;
                    //cell.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                    //cell.imageView.layer.borderWidth = 2.0f;
                    //cell.imageView.layer.masksToBounds = YES;
                    //cell.imageView.image = [UIImage imageNamed:@"SignUp.png"];
                } else if (row == 1) {
                    cell.textLabel.text = @"Find Events";
                    CGFloat widthScale = 35 / cell.imageView.image.size.width;
                    CGFloat heightScale = 35 / cell.imageView.image.size.height;
                    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
                    //cell.imageView.layer.cornerRadius = 40.0f;
                    //cell.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                    //cell.imageView.layer.borderWidth = 2.0f;
                    //cell.imageView.layer.masksToBounds = YES;
                    //cell.imageView.image = [UIImage imageNamed:@"SignUp.png"];
                } else if (row == 2) {
                    cell.textLabel.text = @"Profile";
                    [[PFUser currentUser][@"profilePicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                        //cell.imageView.image = [UIImage imageWithData:data];
                    }];
                    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    CGFloat widthScale = 35 / cell.imageView.image.size.width;
                    CGFloat heightScale = 35 / cell.imageView.image.size.height;
                    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
                    //cell.imageView.layer.cornerRadius = 40.0f;
                    //cell.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                    //cell.imageView.layer.borderWidth = 2.0f;
                    //cell.imageView.layer.masksToBounds = YES;

                    /*
                    cell.textLabel.text = @"Dummy";
                    CGFloat widthScale = 35 / cell.imageView.image.size.width;
                    CGFloat heightScale = 35 / cell.imageView.image.size.height;
                    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
                    //cell.imageView.layer.cornerRadius = 40.0f;
                    //cell.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                    //cell.imageView.layer.borderWidth = 2.0f;
                    //cell.imageView.layer.masksToBounds = YES;
                    //cell.imageView.image = [UIImage imageNamed:@"SignUp.png"];
                    */
                } else if (row == 3) {
                    cell.textLabel.text = @"Logout";
                    CGFloat widthScale = 35 / cell.imageView.image.size.width;
                    CGFloat heightScale = 35 / cell.imageView.image.size.height;
                    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    cell.imageView.transform = CGAffineTransformMakeScale(widthScale, heightScale);
                    //cell.imageView.layer.cornerRadius = 40.0f;
                    //cell.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                    //cell.imageView.layer.borderWidth = 2.0f;
                    //cell.imageView.layer.masksToBounds = YES;
                    //cell.imageView.image = [UIImage imageNamed:@"SignUp.png"];

                }
                
                break;
                
            case 1:
                if (row == 0) {
                } else if (row == 1) {
                } else if (row == 2) {
                } else if (row == 3) {
                } else if (row == 4) {
                } else if (row == 5) {
                } else if (row == 6) {
                }
                break;
                
            case 2:
                if (row == 0) {
                } else if (row == 1) {
                } else if (row == 2) {
                }
                break;
                
            default:
                break;
        }
    }
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController = self.revealViewController;
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;
    NSInteger row = indexPath.row;
    
    
    if (tableView == self.searchController.searchResultsTableView) {
        SearchedUserViewController *searchUserProfile = [[SearchedUserViewController alloc] init];
        searchUserProfile.user = [self.searchResults objectAtIndex:row];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchUserProfile];
        [revealController setFrontViewController:navigationController animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else if (tableView == self.mainTableView) {

        switch (indexPath.section) {
            case 0:
                if (row == 0) {
                    if ( ![frontNavigationController.topViewController isKindOfClass:[DayViewController class]] ) {
                        DayViewController *frontViewController = [[DayViewController alloc] init];
                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
                        [revealController setFrontViewController:navigationController animated:YES];
                    } else {
                        [revealController revealToggle:self];
                    }
                } else if (row == 1) {
                    if ( ![frontNavigationController.topViewController isKindOfClass:[EventfulViewController class]] ) {
                        EventfulViewController *frontViewController = [[EventfulViewController alloc] init];
                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
                        [revealController setFrontViewController:navigationController animated:YES];
                    } else {
                        [revealController revealToggle:self];
                    }

                } else if (row == 2) {
                    if ( ![frontNavigationController.topViewController isKindOfClass:[ProfileViewController class]] ) {
                        ProfileViewController *frontViewController = [[ProfileViewController alloc] init];
                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
                        [revealController setFrontViewController:navigationController animated:YES];
                    } else {
                        [revealController revealToggle:self];
                    }
                } else if (row == 3) {
                    [(AppDelegate *)[[UIApplication sharedApplication] delegate] logout];
                    [revealController revealToggleAnimated:YES];
                }
                break;
            
            case 1:
                break;
            case 2:
                break;
            default:
                break;
        }
    }
    [self.mainTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,20)];
	
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,0,240,20)];
	[tempLabel setTextAlignment:NSTextAlignmentRight];
    
    switch (section)
    {
        case 0:
            return nil;
        case 1:
        {
            //tempLabel.text = NSLocalizedString(@"Information", @"Information");
            //[tempView addSubview:tempLabel];
        }
            break;
            
        case 2:
        {
            //tempLabel.text = NSLocalizedString(@"Tools", @"Tools");
            //[tempView addSubview:tempLabel];
        }
            break;
    }
    
    return tempView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat headerheight = 20.0;
    
    switch (section)
    {
        case 0: { headerheight = 0.0; } break;
        case 1: { headerheight = 22.0; } break;
        case 2: { headerheight = 22.0; } break;
    }
    return headerheight;
}


#pragma mark - UISearchBar Delegate Methods
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{

}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}


#pragma mark - UISearchDisplayController Delegate Methods

- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView
{
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
{
    
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""]) return NO;
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" containsString:searchString];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            [self.searchResults removeAllObjects];
            [self.searchResults addObjectsFromArray:objects];
            [self.searchDisplayController.searchResultsTableView reloadData];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Failed Search" message:[NSString stringWithFormat:@"%@", [error userInfo]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{
    //[controller.searchBar setFrame:CGRectMake(0, 0, 260, 43)];
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView
{
    //[controller.searchBar setFrame:CGRectMake(0, 0, 260, 43)];
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    [self.revealViewController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    [self.revealViewController setFrontViewPosition:FrontViewPositionRight animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
