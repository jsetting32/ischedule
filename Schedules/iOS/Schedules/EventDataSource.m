//
//  EventDataSource.m
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "EventDataSource.h"
#import "EventfulAPI.h"
#import "EventfulEvent.h"
#import "GeoLocation.h"

@interface EventDataSource() <EventfulAPIDelegate, GeoLocationDelegate>
@property (nonatomic) EventfulAPI *api;
@property (nonatomic) GeoLocation *geoLocation;
@end

@implementation EventDataSource

+ (id)sharedInstance
{
    static EventDataSource *sharedObject = nil;
    if( sharedObject == nil ) sharedObject = [[EventDataSource alloc] init];
    return sharedObject;
}

- (void)getEventsByLocation:(NSString *)location {
    self.api = [[EventfulAPI alloc] init];
    [self.api setDelegate:self];
    [self.api getEventsByLocation:location];
}

- (void)getEventsByCurrentLocation {
    
    /*
    NSError *error = nil;
    if (![CLLocationManager locationServicesEnabled]) {
        [self.delegate eventsDidFinishWithError:error];
        return;
    }
    */
    
    NSLog(@"getEventsByCurrentLocation");
    self.geoLocation = [[GeoLocation alloc] init];
    self.geoLocation.delegate = self;
}

- (void)didFinishLoadingEvents:(NSDictionary *)events
{
    [self.delegate eventsDidFinishLoading:[[events objectForKey:@"events"] objectForKey:@"event"]];
}

- (void)fetchQuery:(id)query didFailWithError:(NSError *)error
{
    [self.delegate eventsDidFinishWithError:error];
}

- (void)didReceivePlacemark:(CLPlacemark *)placemark
{
    NSLog(@"didReceivePlacemark:placemark = %@", placemark);
    self.api = [[EventfulAPI alloc] init];
    [self.api setDelegate:self];
    [self.api getEventsByLocation:[placemark locality]];
}

- (void)WOEIDupdate:(id)WOEID
{
    
}

- (void)locationError:(NSError *)error
{
    
}

@end

