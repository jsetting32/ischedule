//
//  SignUpViewController.h
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PF_MBProgressHUD.h>
#import "CalendarStore.h"

@interface SignUpViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, strong) PF_MBProgressHUD *HUD;
@property (nonatomic, strong) UILabel       *appTitle;
@property (nonatomic, strong) UITextField   *firstname;
@property (nonatomic, strong) UITextField   *lastname;
@property (nonatomic, strong) UITextField   *email;
@property (nonatomic, strong) UITextField   *username;
@property (nonatomic, strong) UITextField   *password;
@property (nonatomic, strong) UIButton      *signUpButton;

@end
