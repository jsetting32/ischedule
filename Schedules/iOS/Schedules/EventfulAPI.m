//
//  EventfulAPI.m
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "EventfulAPI.h"
#import "EventfulEvent.h"

NSString *api_key = @"dcHGtwbgPrXtWmhQ";
@interface EventfulAPI() <NSURLConnectionDelegate>
@property (nonatomic) NSMutableData *data;
@property (nonatomic) float contentSize;
@property (nonatomic) NSURLConnection *theConnection;
@property (nonatomic) int pageSize;
@property (nonatomic) int pageCount;
@property (nonatomic) int totalItems;
@end

static int page_size = 100;

@implementation EventfulAPI

- (void) getEventsByLocation:(NSString *)location
{
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" "];
    location = [[location componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@"+"];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.eventful.com/json/events/search?app_key=%@&location=%@&page_size=%i", api_key, location, page_size]];
	self.theConnection = [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
}

- (void) getEventsByVenue:(NSString *)venue location:(NSString *)location
{
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" "];
    venue = [[venue componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@"+"];
    location = [[venue componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@"+"];
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.eventful.com/json/venues/search?app_key=%@&location=%@&keywords=%@&page_size=%i",  api_key ,location ,venue, page_size]];
	self.theConnection = [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:url] delegate:self];
}


#pragma mark - NSURLConnectionDelegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	self.data = [[NSMutableData alloc] init];
	//NSLog(@"Expected data length %lld", [response expectedContentLength]);
	//self.contentSize = (float)[response expectedContentLength];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[self.data appendData:data];
	//float progress = (float)[self.data length] / (float)self.contentSize;
	//NSLog(@"Progress: %f", progress);
}


- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Succeeded! Received %lu bytes of data",(unsigned long)[self.data length]);
	NSError *error;
    [self.delegate didFinishLoadingEvents:[NSJSONSerialization JSONObjectWithData:self.data options:kNilOptions error:&error]];
    self.theConnection = nil;
    self.data = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.theConnection = nil;
    self.data = nil;
    [self.delegate fetchQuery:connection didFailWithError:error];
}

@end
