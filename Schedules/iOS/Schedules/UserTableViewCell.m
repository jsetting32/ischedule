//
//  UserTableViewCell.m
//  Schedules
//
//  Created by John Setting on 11/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "UserTableViewCell.h"

@implementation UserTableViewCell
@synthesize avatarImageButton;
@synthesize nameButton;
@synthesize descriptionButton;
@synthesize user;
@synthesize followButton;
@synthesize avatarImageView;
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		[self.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundFindFriendsCell.png"]]];
        [self setSelectionStyle:UITableViewCellSelectionStyleDefault];
        
        self.avatarImageView = [[SCHProfileImageView alloc] init];
        [self.avatarImageView setFrame:CGRectMake( 10.0f, 10.0f, 40.0f, 40.0f)];
        [self.contentView addSubview:self.avatarImageView];
        
        self.avatarImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.avatarImageButton setBackgroundColor:[UIColor clearColor]];
        [self.avatarImageButton setFrame:CGRectMake( 10.0f, 10.0f, 40.0f, 40.0f)];
        [self.avatarImageButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.avatarImageButton];
        
        self.nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.nameButton setBackgroundColor:[UIColor clearColor]];
        [self.nameButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [self.nameButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
		[self.nameButton.titleLabel setShadowOffset:CGSizeMake( 0.0f, 1.0f )];
		[self.nameButton setTitleColor:[UIColor colorWithRed:87.0f/255.0f green:72.0f/255.0f blue:49.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self.nameButton setTitleColor:[UIColor colorWithRed:134.0f/255.0f green:100.0f/255.0f blue:65.0f/255.0f alpha:1.0f] forState:UIControlStateHighlighted];
        [self.nameButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.nameButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.nameButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.nameButton];
		
		self.descriptionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.descriptionButton setBackgroundColor:[UIColor clearColor]];
        [self.descriptionButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [self.descriptionButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
		[self.descriptionButton.titleLabel setShadowOffset:CGSizeMake( 0.0f, 1.0f )];
		[self.descriptionButton setTitleColor:[UIColor colorWithRed:87.0f/255.0f green:72.0f/255.0f blue:49.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self.descriptionButton setTitleColor:[UIColor colorWithRed:134.0f/255.0f green:100.0f/255.0f blue:65.0f/255.0f alpha:1.0f] forState:UIControlStateHighlighted];
        [self.descriptionButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.descriptionButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.descriptionButton addTarget:self action:@selector(didTapUserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.descriptionButton];
		
		
        
        self.followButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.followButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
        [self.followButton setTitleEdgeInsets:UIEdgeInsetsMake( 0.0f, 10.0f, 0.0f, 0.0f)];
        [self.followButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollow.png"] forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollowing.png"] forState:UIControlStateSelected];
        [self.followButton setImage:[UIImage imageNamed:@"IconTick.png"] forState:UIControlStateSelected];
        [self.followButton setTitle:@"Follow  " forState:UIControlStateNormal]; // space added for centering
        [self.followButton setTitle:@"Following" forState:UIControlStateSelected];
        [self.followButton setTitleColor:[UIColor colorWithRed:84.0f/255.0f green:57.0f/255.0f blue:45.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.followButton setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self.followButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateSelected];
        [self.followButton.titleLabel setShadowOffset:CGSizeMake( 0.0f, -1.0f)];
        [self.followButton addTarget:self action:@selector(didTapFollowButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.followButton];
    }
    return self;
}


#pragma mark - PAPFindFriendsCell

- (void)setUser:(PFUser *)aUser {
    user = aUser;
    
    // Configure the cell
    [avatarImageView setFile:[self.user objectForKey:@"profilePicture"]];
    
    NSString *nameString = [self.user objectForKey:@"username"];
	if (nameString != nil) {
		[nameButton setTitle:[self.user objectForKey:@"username"] forState:UIControlStateNormal];
		[nameButton setTitle:[self.user objectForKey:@"username"] forState:UIControlStateHighlighted];
		[nameButton setFrame:CGRectMake( 60.0f, 10.0f, 144.0f, 20.0f)];
	}
    
	NSString *descriptionString = [self.user objectForKey:@"name"];
	if (descriptionString != nil){
        [descriptionButton setTitle:[self.user objectForKey:@"name"] forState:UIControlStateNormal];
		[descriptionButton setTitle:[self.user objectForKey:@"name"] forState:UIControlStateHighlighted];
		[descriptionButton setFrame:CGRectMake( 60.0f, 30.0f, 144.0f, 20.0f)];
	}
	
	NSLog(@"%@ : %@", descriptionString, nameString);
	
	// Set follow button
    [followButton setFrame:CGRectMake( 208.0f, 14.0f, 103.0f, 32.0f)];
}

+ (CGFloat)heightForCell {
    return 60.0f;
}

/* Inform delegate that a user image or name was tapped */
- (void)didTapUserButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapUserButton:)]) {
        [self.delegate cell:self didTapUserButton:self.user];
    }
}

/* Inform delegate that the follow button was tapped */
- (void)didTapFollowButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapFollowButton:)]) {
        [self.delegate cell:self didTapFollowButton:self.user];
    }
}

@end
