//
//  FrapleConstants.m
//  Fraple
//
//  Created by John Setting on 8/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "Constants.h"

NSString *const kSCHUserDefaultsActivityFeedViewControllerLastRefreshKey    = @"com.parse.Schedules.userDefaults.activityFeedViewController.lastRefresh";
NSString *const kSCHUserDefaultsCacheFriendsKey                             = @"com.parse.Schedules.userDefaults.cache.friends";


#pragma mark - Launch URLs

NSString *const kSCHLaunchURLHostTakePicture = @"camera";


#pragma mark - NSNotification

NSString *const SCHAppDelegateApplicationDidReceiveRemoteNotification           = @"com.parse.Schedules.appDelegate.applicationDidReceiveRemoteNotification";
NSString *const SCHUtilityUserFriendChangedNotification                         = @"com.parse.Schedules.utility.userFriendChanged";
NSString *const SCHUtilityUserLikedUnlikedPhotoCallbackFinishedNotification     = @"com.parse.Schedules.utility.userLikedUnlikedPhotoCallbackFinished";
NSString *const SCHUtilityDidFinishProcessingProfilePictureNotification         = @"com.parse.Schedules.utility.didFinishProcessingProfilePictureNotification";
NSString *const SCHTabBarControllerDidFinishEditingPhotoNotification            = @"com.parse.Schedules.tabBarController.didFinishEditingPhoto";
NSString *const SCHTabBarControllerDidFinishImageFileUploadNotification         = @"com.parse.Schedules.tabBarController.didFinishImageFileUploadNotification";
NSString *const SCHPhotoDetailsViewControllerUserDeletedPhotoNotification       = @"com.parse.Schedules.photoDetailsViewController.userDeletedPhoto";
NSString *const SCHPhotoDetailsViewControllerUserLikedUnlikedPhotoNotification  = @"com.parse.Schedules.photoDetailsViewController.userLikedUnlikedPhotoInDetailsViewNotification";
NSString *const SCHPhotoDetailsViewControllerUserCommentedOnPhotoNotification   = @"com.parse.Schedules.photoDetailsViewController.userCommentedOnPhotoInDetailsViewNotification";


#pragma mark - User Info Keys
NSString *const SCHPhotoDetailsViewControllerUserLikedUnlikedPhotoNotificationUserInfoLikedKey = @"liked";
NSString *const kSCHEditPhotoViewControllerUserInfoCommentKey = @"comment";

#pragma mark - Installation Class

// Field keys
NSString *const kSCHInstallationUserKey     = @"user";

#pragma mark - Activity Class
// Class key
NSString *const kSCHActivityClassKey        = @"Activity";

// Field keys
NSString *const kSCHActivityTypeKey         = @"type";
NSString *const kSCHActivityFromUserKey     = @"fromUser";
NSString *const kSCHActivityToUserKey       = @"toUser";
NSString *const kSCHActivityContentKey      = @"content";
NSString *const kSCHActivityPhotoKey        = @"photo";

// Type values
NSString *const kSCHActivityTypeLike        = @"like";
NSString *const kSCHActivityTypeFriend      = @"friend";
NSString *const kSCHActivityTypeComment     = @"comment";
NSString *const kSCHActivityTypeJoined      = @"joined";

#pragma mark - User Class
// Field keys
NSString *const kSCHUserDisplayNameKey                          = @"username";
NSString *const kSCHUserIDKey                                   = @"objectId";
NSString *const kSCHUserPhotoIDKey                              = @"photoId";
NSString *const kSCHUserProfilePicSmallKey                      = @"profilePictureSmall";
NSString *const kSCHUserProfilePicMediumKey                     = @"profilePictureMedium";
NSString *const kSCHUserFriendsKey                              = @"userFriends";
NSString *const kSCHUserAlreadyAutoFollowedFriendsKey           = @"userAlreadyAutoFollowedFriends";

#pragma mark - Photo Class
// Class key
NSString *const kSCHPhotoClassKey = @"Photo";

// Field keys
NSString *const kSCHPhotoPictureKey         = @"image";
NSString *const kSCHPhotoThumbnailKey       = @"thumbnail";
NSString *const kSCHPhotoUserKey            = @"user";
NSString *const kSCHPhotoOpenGraphIDKey     = @"fbOpenGraphID";


#pragma mark - Cached Photo Attributes
// keys
NSString *const kSCHPhotoAttributesIsLikedByCurrentUserKey = @"isLikedByCurrentUser";
NSString *const kSCHPhotoAttributesLikeCountKey            = @"likeCount";
NSString *const kSCHPhotoAttributesLikersKey               = @"likers";
NSString *const kSCHPhotoAttributesCommentCountKey         = @"commentCount";
NSString *const kSCHPhotoAttributesCommentersKey           = @"commenters";


#pragma mark - Cached User Attributes
// keys
NSString *const kSCHUserAttributesPhotoCountKey                 = @"photoCount";
NSString *const kSCHUserAttributesIsFriendedByCurrentUserKey    = @"isFriendedByCurrentUser";


#pragma mark - Push Notification Payload Keys

NSString *const kASNSAlertKey = @"alert";
NSString *const kASNSBadgeKey = @"badge";
NSString *const kASNSSoundKey = @"sound";

// the following keys are intentionally kept short, APNS has a maximum payload limit
NSString *const kSCHPushPayloadPayloadTypeKey          = @"p";
NSString *const kSCHPushPayloadPayloadTypeActivityKey  = @"a";

NSString *const kSCHPushPayloadActivityTypeKey     = @"t";
NSString *const kSCHPushPayloadActivityLikeKey     = @"l";
NSString *const kSCHPushPayloadActivityCommentKey  = @"c";
NSString *const kSCHPushPayloadActivityFollowKey   = @"f";

NSString *const kSCHPushPayloadFromUserObjectIdKey = @"fu";
NSString *const kSCHPushPayloadToUserObjectIdKey   = @"tu";
NSString *const kSCHPushPayloadPhotoObjectIdKey    = @"pid";