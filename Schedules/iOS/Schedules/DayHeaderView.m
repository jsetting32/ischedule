//
//  DayHeaderView.m
//  iSchedj
//
//  Created by John Setting on 3/30/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "DayHeaderView.h"

@interface DayHeaderView()
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIView *view1;
@property (nonatomic) UIView *view2;
@property (nonatomic) UIView *view3;
@property (nonatomic) int currIndex;
@property (nonatomic) int nextIndex;
@property (nonatomic) int prevIndex;
@end

@implementation DayHeaderView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.scrollView = [[UIScrollView alloc] init];
        self.scrollView.showsHorizontalScrollIndicator = YES;
        self.scrollView.showsVerticalScrollIndicator = YES;
        
        self.view1 = [[UIView alloc] init];
        [self.view1 setBackgroundColor:[UIColor blueColor]];
        [self.scrollView addSubview:self.view1];
        
        self.view2 = [[UIView alloc] init];
        [self.view2 setBackgroundColor:[UIColor yellowColor]];
        [self.scrollView addSubview:self.view2];
        
        self.view3 = [[UIView alloc] init];
        [self.view3 setBackgroundColor:[UIColor greenColor]];
        [self.scrollView addSubview:self.view3];

        [self addSubview:self.scrollView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [self.view1 setFrame:CGRectMake(0, 0, 320, 44)];
    [self.view2 setFrame:CGRectMake(320, 0, 320, 44)];
    [self.view3 setFrame:CGRectMake(640, 0, 320, 44)];

    [self.scrollView setFrame:CGRectMake(0, 0, 320, 70)];
    [self.scrollView setContentSize:CGSizeMake(960, 70)];
    [self.scrollView scrollRectToVisible:CGRectMake(320,0,320,416) animated:NO];
}

- (void)loadPageWithId:(int)index onPage:(int)page
{
    // load data for page
    switch (page) {
        case 0:
            break;
        case 1:
            //pageTwoDoc.text = [documentTitles objectAtIndex:index];
            break;
        case 2:
            //pageThreeDoc.text = [documentTitles objectAtIndex:index];
            break;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender
{
    // All data for the documents are stored in an array (documentTitles).
    // We keep track of the index that we are scrolling to so that we
    // know what data to load for each page.
    if(self.scrollView.contentOffset.x > self.scrollView.frame.size.width)
    {
        // We are moving forward. Load the current doc data on the first page.
        [self loadPageWithId:self.currIndex onPage:0];
        
        // Add one to the currentIndex or reset to 0 if we have reached the end.
        //self.currIndex = (self.currIndex >= [documentTitles count]-1) ? 0 : self.currIndex + 1;
        [self loadPageWithId:self.currIndex onPage:1];
        
        // Load content on the last page. This is either from the next item in the array
        // or the first if we have reached the end.
        //self.nextIndex = (self.currIndex >= [documentTitles count]-1) ? 0 : self.currIndex + 1;
        
        [self loadPageWithId:self.nextIndex onPage:2];
    }
    if(self.scrollView.contentOffset.x < self.scrollView.frame.size.width) {
        // We are moving backward. Load the current doc data on the last page.
        [self loadPageWithId:self.currIndex onPage:2];
        
        // Subtract one from the currentIndex or go to the end if we have reached the beginning.
        //currIndex = (currIndex == 0) ? [documentTitles count]-1 : currIndex - 1;
        [self loadPageWithId:self.currIndex onPage:1];
        
        // Load content on the first page. This is either from the prev item in the array
        // or the last if we have reached the beginning.
        //prevIndex = (currIndex == 0) ? [documentTitles count]-1 : currIndex - 1;
        
        [self loadPageWithId:self.prevIndex onPage:0];
    }     
    
    // Reset offset back to middle page
    [self.scrollView scrollRectToVisible:CGRectMake(320,0,320,416) animated:NO];
}

@end
