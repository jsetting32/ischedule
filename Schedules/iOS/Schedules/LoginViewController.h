//
//  LoginViewController.h
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientLayer.h"
#import <Parse/PF_MBProgressHUD.h>
#import "CalendarStore.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) PF_MBProgressHUD *HUD;
@property (nonatomic, strong) UITextField *username;
@property (nonatomic, strong) UITextField *password;
@property (nonatomic, strong) UIButton *forgotPassword;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UIButton *facebookButton;
@property (nonatomic, strong) UIButton *twitterButton;
@end
