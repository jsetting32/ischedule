//
//  EventfulEventCell.m
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "EventfulEventCell.h"
#import "EventfulImageView.h"
#import "EventfulEvent.h"

@interface EventfulEventCell()
@property (nonatomic) UILabel *eventTitleLabel;
@property (nonatomic) UILabel *eventVenueName;
@property (nonatomic) UILabel *eventLocation;
@property (nonatomic) UILabel *eventDescription;
@property (nonatomic) UIButton *eventImageButton;
@property (nonatomic) EventfulImageView *eventImageView;
@end


@implementation EventfulEventCell

#pragma mark - NSObject

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundFindFriendsCell.png"]];
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDetailButton;
        
        self.eventImageView = [[EventfulImageView alloc] init];
        self.eventImageView.frame = CGRectMake( 10.0f, 14.0f, 40.0f, 40.0f);
        [self.contentView addSubview:self.eventImageView];
        
        self.eventImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.eventImageButton.backgroundColor = [UIColor clearColor];
        self.eventImageButton.frame = CGRectMake( 10.0f, 14.0f, 40.0f, 40.0f);
        [self.eventImageButton addTarget:self action:@selector(didTapImageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.eventImageButton];
        
        self.eventTitleLabel = [[UILabel alloc] init];
        self.eventTitleLabel.font = [UIFont systemFontOfSize:11.0f];
        self.eventTitleLabel.textColor = [UIColor grayColor];
        self.eventTitleLabel.backgroundColor = [UIColor clearColor];
        self.eventTitleLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.eventTitleLabel.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.eventTitleLabel];

        /*
        self.eventVenueName = [[UILabel alloc] init];
        self.eventVenueName.font = [UIFont systemFontOfSize:11.0f];
        self.eventVenueName.textColor = [UIColor grayColor];
        self.eventVenueName.backgroundColor = [UIColor clearColor];
        self.eventVenueName.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.eventVenueName.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.eventVenueName];
        */
        
        self.eventLocation = [[UILabel alloc] init];
        self.eventLocation.font = [UIFont systemFontOfSize:11.0f];
        self.eventLocation.textColor = [UIColor grayColor];
        self.eventLocation.backgroundColor = [UIColor clearColor];
        self.eventLocation.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.eventLocation.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.eventLocation];
        
        self.eventDescription = [[UILabel alloc] init];
        self.eventDescription.font = [UIFont systemFontOfSize:11.0f];
        self.eventDescription.textColor = [UIColor grayColor];
        self.eventDescription.backgroundColor = [UIColor clearColor];
        self.eventDescription.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.eventDescription.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.eventDescription];
        /*
        self.nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.nameButton.backgroundColor = [UIColor clearColor];
        self.nameButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:8.0f];
        self.nameButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.nameButton setTitleColor:[UIColor colorWithRed:87.0f/255.0f green:72.0f/255.0f blue:49.0f/255.0f alpha:1.0f]
                              forState:UIControlStateNormal];
        [self.nameButton setTitleColor:[UIColor colorWithRed:134.0f/255.0f green:100.0f/255.0f blue:65.0f/255.0f alpha:1.0f]
                              forState:UIControlStateHighlighted];
        [self.nameButton setTitleShadowColor:[UIColor whiteColor]
                                    forState:UIControlStateNormal];
        [self.nameButton setTitleShadowColor:[UIColor whiteColor]
                                    forState:UIControlStateSelected];
        [self.nameButton.titleLabel setShadowOffset:CGSizeMake( 0.0f, 1.0f)];
        [self.nameButton addTarget:self action:@selector(didTapUserButtonAction:)
                  forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.nameButton];
        
        self.photoLabel = [[UILabel alloc] init];
        self.photoLabel.font = [UIFont systemFontOfSize:11.0f];
        self.photoLabel.textColor = [UIColor grayColor];
        self.photoLabel.backgroundColor = [UIColor clearColor];
        self.photoLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        self.photoLabel.shadowOffset = CGSizeMake( 0.0f, 1.0f);
        [self.contentView addSubview:self.photoLabel];
        */
        
        /*
        self.followButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.followButton.titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        self.followButton.titleEdgeInsets = UIEdgeInsetsMake( 0.0f, 10.0f, 0.0f, 0.0f);
        [self.followButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollow.png"]
                                     forState:UIControlStateNormal];
        [self.followButton setBackgroundImage:[UIImage imageNamed:@"ButtonFollowing.png"]
                                     forState:UIControlStateSelected];
        [self.followButton setImage:[UIImage imageNamed:@"IconTick.png"]
                           forState:UIControlStateSelected];
        [self.followButton setTitle:NSLocalizedString(@"Follow  ", @"Follow string, with spaces added for centering")
                           forState:UIControlStateNormal];
        [self.followButton setTitle:@"Following"
                           forState:UIControlStateSelected];
        [self.followButton setTitleColor:[UIColor colorWithRed:84.0f/255.0f green:57.0f/255.0f blue:45.0f/255.0f alpha:1.0f]
                                forState:UIControlStateNormal];
        [self.followButton setTitleColor:[UIColor whiteColor]
                                forState:UIControlStateSelected];
        [self.followButton setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f]
                                      forState:UIControlStateNormal];
        [self.followButton setTitleShadowColor:[UIColor blackColor]
                                      forState:UIControlStateSelected];
        self.followButton.titleLabel.shadowOffset = CGSizeMake( 0.0f, -1.0f);
        [self.followButton addTarget:self action:@selector(didTapFollowButtonAction:)
                    forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.followButton];
         
         */
    }
    return self;
}


#pragma mark - PAPFindFriendsCell

- (void)setEventForCell:(EventfulEvent *)aEvent {
    self.event = aEvent;
    
    // Configure the cell
    [self.eventImageView setImage:[[self.event image] objectForKey:@"url"]];
    
    // Set name
    [self.eventTitleLabel setText:[aEvent title]];
    [self.eventTitleLabel setFrame:CGRectMake( 60.0f, 10.0f, 200.0f, 12.0f)];

    // Set name
    //[self.eventVenueName setText:[aEvent venueName]];
    //[self.eventVenueName setFrame:CGRectMake( 60.0f, 25.0f, 200.0f, 12.0f)];
    [self.eventLocation setText:[NSString stringWithFormat:@"%@, %@", [aEvent cityName], [aEvent regionAbbr]]];
    [self.eventLocation setFrame:CGRectMake( 60.0f, 25.0f, 200.0f, 12.0f)];

    // Set name
    [self.eventDescription setText:[aEvent description]];
    [self.eventDescription setFrame:CGRectMake( 60.0f, 40.0f, 200.0f, 12.0f)];

    /*
    CGSize nameSize = [nameString boundingRectWithSize:CGSizeMake(144.0f, CGFLOAT_MAX)
                                               options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0f]}
                                               context:nil].size;
    */
    
    //[self.nameButton setTitle:[aEvent title] forState:UIControlStateNormal];
    //[self.nameButton setTitle:[aEvent title] forState:UIControlStateHighlighted];
    
    //[self.nameButton setFrame:CGRectMake( 60.0f, 17.0f, nameSize.width, nameSize.height)];
    
    // Set photo number label
    /*
    CGSize photoLabelSize = [@"photos" boundingRectWithSize:CGSizeMake(144.0f, CGFLOAT_MAX)
                                                    options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11.0f]}
                                                    context:nil].size;
    */
    
    //[self.photoLabel setFrame:CGRectMake( 60.0f, 17.0f + nameSize.height, 140.0f, photoLabelSize.height)];
    
    // Set follow button
    //[self.followButton setFrame:CGRectMake( 208.0f, 20.0f, 103.0f, 32.0f)];
}


#pragma mark - ()

+ (CGFloat)heightForCell {
    return 67.0f;
}

- (void)didFinishLoadingImageView:(UIImage *)image
{
    
}

/* Inform delegate that a event image was tapped */
- (void)didTapImageButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapImageButton:)]) {
        [self.delegate cell:self didTapImageButton:self.eventImageButton];
    }
}

/* Inform delegate that the follow button was tapped */
//- (void)didTapFollowButtonAction:(id)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:didTapFollowButton:)]) {
//        [self.delegate cell:self didTapFollowButton:self.user];
//    }
//}

@end
