//
//  FrapleUtility.h
//  Fraple
//
//  Created by John Setting on 8/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject


+ (void)likePhotoInBackground:(id)photo block:(void (^)(BOOL succeeded, NSError *error))completionBlock;
+ (void)unlikePhotoInBackground:(id)photo block:(void (^)(BOOL succeeded, NSError *error))completionBlock;

+ (void)processFacebookProfilePictureData:(NSData *)data;

+ (BOOL)userHasValidFacebookData:(PFUser *)user;
+ (BOOL)userHasProfilePictures:(PFUser *)user;

+ (NSString *)firstNameForDisplayName:(NSString *)displayName;

/************************** Friend Methods **************************/
+ (void)friendUserRequestInBackground:(PFUser *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;
+ (void)friendUserRequestEventually:(PFUser *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;


+ (void)unfriendUserRequestInBackground:(PFUser *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;
+ (void)unfriendUserRequestEventually:(PFUser *)user;


+ (void)friendUserInBackground:(PFUser *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;
+ (void)friendUserEventually:(PFUser *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;

+ (void)unfriendUserInBackground:(PFUser *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;
+ (void)unfriendUserEventually:(PFUser *)user;


+ (void)friendUsersEventually:(NSArray *)users block:(void (^)(BOOL succeeded, NSError *error))completionBlock;


+ (void)unfriendUsersInBackground:(NSArray *)user block:(void (^)(BOOL succeeded, NSError *error))completionBlock;
+ (void)unfriendUsersEventually:(NSArray *)users;
/************************** Friend Methods **************************/

+ (void)drawSideDropShadowForRect:(CGRect)rect inContext:(CGContextRef)context;
+ (void)drawSideAndBottomDropShadowForRect:(CGRect)rect inContext:(CGContextRef)context;
+ (void)drawSideAndTopDropShadowForRect:(CGRect)rect inContext:(CGContextRef)context;  
+ (void)addBottomDropShadowToNavigationBarForNavigationController:(UINavigationController *)navigationController;

+ (PFQuery *)queryForActivitiesOnPhoto:(PFObject *)photo cachePolicy:(PFCachePolicy)cachePolicy;
@end
