//
//  SchedulesEvent.h
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

@interface SchedulesEvent : NSObject <NSCoding>
{
    NSString            *   SeventId;
    NSString            *   Stitle;
    NSString            *   Slocation;
    NSString            *   Sdescription;
    NSString            *   ScalendarTitle;
    //EKCalendarType      *   ScalendarType;
    //BOOL                *   ScalendarAllowsModify;
    //NSData              *   ScalendarColor;
    //NSArray             *   Salarms;
    NSString            *   SURL;
    //NSDate              *   SlastModified;
    NSString            *   StimeZone;
    NSDate              *   SstartDate;
    NSDate              *   SendDate;
    BOOL                    SallDay;
    //BOOL                    Sfloating;
    //NSArray             *   Srecurrence;
    //NSArray             *   Sattendees;
    NSString            *   SeventIdentifier;
}

@property (nonatomic, copy) NSString            *   SeventId;
@property (nonatomic, copy) NSString            *   Stitle;
@property (nonatomic, copy) NSString            *   Slocation;
@property (nonatomic, copy) NSString            *   Sdescription;
@property (nonatomic, copy) NSString            *   ScalendarTitle;
//@property                   EKCalendarType      *   ScalendarType;
//@property                   BOOL                *   ScalendarAllowsModify;
//@property                   NSData      *   ScalendarColor;
//@property (nonatomic, copy) NSArray             *   Salarms;
@property (nonatomic, copy) NSString            *   SURL;
//@property (nonatomic, copy) NSDate              *   SlastModified;
@property (nonatomic, copy) NSString            *   StimeZone;
@property (nonatomic, copy) NSDate              *   SstartDate;
@property (nonatomic, copy) NSDate              *   SendDate;
@property                   BOOL                    SallDay;
//@property                   BOOL                    Sfloating;
//@property (nonatomic, copy) NSArray             *   Srecurrence;
//@property (nonatomic, copy) NSArray             *   Sattendees;
@property (nonatomic, copy) NSString            *   SeventIdentifier;
@end
