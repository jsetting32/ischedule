//
//  PhotoDetailViewController.h
//  SavingImagesTutorial
//
//  Created by Sidwyn Koh on 29/1/12.
//

#import <UIKit/UIKit.h>

@interface PhotoDetailViewController : UIViewController
{
    UIImageView *photoImageView;
    UIImage *selectedImage;
    NSString *imageName;
}
@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, strong) NSString *imageName;

@end
