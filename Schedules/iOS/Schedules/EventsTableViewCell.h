//
//  EventsTableViewCell.h
//  MYSCHEDULER
//
//  Created by John Setting on 8/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//
#import <EventKit/EventKit.h>
#import "SchedulesEvent.h"
@protocol EventsTableViewCellDelegate;

@interface EventsTableViewCell : UITableViewCell
{
	id _delegate;
    
    SchedulesEvent *event;
}

@property (nonatomic, strong) id<EventsTableViewCellDelegate> delegate;

@property (retain, nonatomic) SchedulesEvent *event;
@property (retain, nonatomic) NSDictionary *eventInfo;

@property (retain, nonatomic) UILabel *eventTitle;
@property (retain, nonatomic) UILabel *eventDescription;
@property (retain, nonatomic) UILabel *eventTime;

@property (retain, nonatomic) UIImageView *imageViewPic;

@property (retain, nonatomic) UIButton *shareButton;

- (void)setCellForEvent:(SchedulesEvent *)aEvent;
- (void)setCellForFeedEvent:(id)feed;

@end


/*!
 The protocol defines methods a delegate of a PAPBaseTextCell should implement.
 */
@protocol EventsTableViewCellDelegate <NSObject>
@optional

/*!
 Sent to the delegate when a user button is tapped
 @param aUser the PFUser of the user that was tapped
 */
- (void)cell:(EventsTableViewCell *)cellView didTapShareActionButton:(SchedulesEvent *)aEvent;
@end
