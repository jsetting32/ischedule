//
//  CalendarStore.h
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

/*                              Description
 *  CalendarDateSource is a shared instance among the life of the application.
 *  It loads all the iOS calendar events from the calendar data storage.
 *  
 *  When the events are finished loading, the delegate of CalendarStore
 *  receives an update of events that were found, based on the passed by 
 *  query.
 *
 *  CalendarDataSource is the model of the application. It will be where all
 *  our data will be located and all heavy computation will take place.
 */

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>
#import "EventfulEvent.h"
@protocol CalendarDataSourceDelegate;

@interface CalendarDataSource : NSObject
@property (weak, nonatomic) id<CalendarDataSourceDelegate> delegate;

+ (CalendarDataSource *)sharedInstance;

// Constructors...
- (void) loadEventsWithCalendars:(NSMutableArray *)calendars;
- (void) loadEventsWithCalendarsAndDate:(NSDate *)date calendars:(NSMutableArray *)calendars;
- (void) loadEventsWithCalendarsAndDateFrame:(NSDate *)startDate endDate:(NSDate *)endDate calendars:(NSMutableArray *)calendars;
- (void) loadCalendars:(id)sender;
- (void) runEventFetch:(NSDate *)date1 date2:(NSDate *)date2 withCalendars:(NSMutableArray *)calendars;

// Accessor Methods
- (EKEvent *)eventAtIndexPath:(NSIndexPath *)indexPath;
- (EKEvent *)eventAtIndexPathForSections:(NSIndexPath *)indexPath;
- (EKEvent *)eventByIndentifier:(NSString *)identifier;
- (UIImage *)eventCalendarColorAtIndexPath:(NSIndexPath *)indexPath;
- (NSMutableArray *)eventsByGivenDate:(NSDate *)givenDate;

// Mutator Methods
- (void)addEvent:(EKEvent *)aEvent;
- (void)deleteEvent:(EKEvent *)aEvent;

// Setter Methods
- (void)setSelectedDate:(NSDate *)date;
- (void)setCalendars:(NSSet *)calendars;

// Helper Methods
+ (BOOL)isDateInToday:(NSDate*)date;
+ (UIImage *)eventCalendarColor:(EKEvent *)event;
+ (CGColorRef)calendarColorForEvent:(EKEvent *)event;
+ (NSString *)returnLongStyleDateFormat:(NSDate *)date;
+ (NSArray *)convertDates:(EKEvent *)event;
+ (NSArray *)getDaysOfWeekGivenADayDate:(NSDate *)date;
+ (NSString *)convertDayToString:(int)dayInt;
+ (UIImage *)imageFromColor:(UIColor *)color;
+ (NSArray *)convertDatesForDayViewCell:(EKEvent *)event;
+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate;
+ (NSDate *)dateAtEndOfDayForDate:(NSDate *)inputDate;
+ (NSDate *)convertStringToDate:(NSString *)date;
- (EKEvent *)returnConvertedEvent:(EventfulEvent *)event;

+ (NSDate *)dateByPreviousWeek:(NSDate *)date;
+ (NSDate *)dateByFutureWeek:(NSDate *)date;

// Getter Methods
- (NSDate *)date;
- (NSUInteger)returnNumberOfSections;
- (NSUInteger)returnNumberOfRows:(NSInteger)inSection;
- (NSString *)returnSectionHeaderText:(NSInteger)section;
- (NSMutableArray *)events;
- (NSMutableArray *)chosenCalendars;
- (EKEventStore *)store;
- (NSInteger)returnTodaySectionHeaderNumber;
@end

@protocol CalendarDataSourceDelegate <NSObject>
@optional
- (void)eventsDidStartLoading:(NSDate *)fromDate toDate:(NSDate *)toDate;
- (void)calendarsDidFinishLoading:(id)calendars;
- (void)eventDidFinishDeleting:(EKEvent *)event;
- (void)eventsDidFailWithError:(NSError *)error;
@required
- (void)eventDidFinishSaving:(EKEvent *)event;
- (void)eventsDidFinishLoading:(NSArray *)events;
@end
