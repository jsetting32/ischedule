//
//  GradientLayer.h
//  Schedules
//
//  Created by John Setting on 10/30/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface GradientLayer : NSObject

+(CAGradientLayer*) greyGradient:(float)red green:(float)green blue:(float)blue;
+(CAGradientLayer*) gradientClash:(UIColor *)colorOne colorTwo:(UIColor *)colorTwo;

@end
