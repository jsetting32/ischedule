//
//  CalendarStore.m
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CalendarDataSource.h"
@interface CalendarDataSource()
@property (nonatomic) NSMutableArray *events;
@property (nonatomic) NSMutableDictionary *dictEvents;
@property (nonatomic) NSMutableDictionary *sections;
@property (nonatomic) NSArray *sortedDays;
@property (nonatomic) NSMutableArray *chosenCalendars;
@property (nonatomic) EKEventStore *store;
@property (nonatomic) NSDate *date;
@end


static BOOL debug = TRUE ;
@implementation CalendarDataSource

+ (CalendarDataSource *) sharedInstance
{
    static CalendarDataSource *sharedObject = nil;
    if( sharedObject == nil ) sharedObject = [[CalendarDataSource alloc] init];
    return sharedObject;
}

- (void) loadEventsWithCalendars:(NSMutableArray *)calendars
{
    [self runEventFetch:[[self createDefaultTimes] objectAtIndex:0] date2:[[self createDefaultTimes] objectAtIndex:1] withCalendars:calendars];
}

- (void) loadEventsWithCalendarsAndDate:(NSDate *)date calendars:(NSMutableArray *)calendars
{
    
    [self runEventFetch:date date2:date withCalendars:calendars];
}

- (NSMutableArray *)chosenCalendars
{
    if (!_chosenCalendars)
        _chosenCalendars = [NSMutableArray arrayWithArray:[self.store calendarsForEntityType:EKEntityTypeEvent]];
    return _chosenCalendars;
}

- (void) loadEventsWithCalendarsAndDateFrame:(NSDate *)startDate endDate:(NSDate *)endDate calendars:(NSMutableArray *)calendars
{
    if (!(startDate == nil || endDate == nil)) {
        [self runEventFetch:startDate date2:endDate withCalendars:nil];
	} else {
        [self loadEventsWithCalendars:calendars];
    }
}

- (void)runEventFetch:(NSDate *)date1 date2:(NSDate *)date2 withCalendars:(NSMutableArray *)calendars
{
    date1 = [CalendarDataSource dateAtBeginningOfDayForDate:date1];
    date2 = [CalendarDataSource dateAtEndOfDayForDate:date2];
    
	[self.store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            if ([self.delegate respondsToSelector:@selector(eventsDidFailWithError:)])
                [self.delegate eventsDidFailWithError:error];
            return;
        }
        
        if ([self.delegate respondsToSelector:@selector(eventsDidStartLoading:toDate:)])
            [self.delegate eventsDidStartLoading:date1 toDate:date2];
        
        // Create the predicate from the event store's instance method
        NSPredicate *predicate = [self.store predicateForEventsWithStartDate:date1 endDate:date2 calendars:calendars];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // Notify the delegate the events that were found
            // eventsMatchingPredicate is a synchronous function
            // so we make it asynchronous using dispatch_async
            self.events = [NSMutableArray arrayWithArray:[self.store eventsMatchingPredicate:predicate]];
            self.sections = [NSMutableDictionary dictionary];
            for (EKEvent * event in self.events){
                NSDate * dateRepresentingThisDay = [CalendarDataSource dateAtBeginningOfDayForDate:event.startDate];
                NSMutableArray * eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
                if (eventsOnThisDay == nil) {
                    eventsOnThisDay = [NSMutableArray array];
                    [self.sections setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
                }
                [eventsOnThisDay addObject:event];
            }
            self.sortedDays = [[self.sections allKeys] sortedArrayUsingSelector:@selector(compare:)];
            
            if ([self.delegate respondsToSelector:@selector(eventsDidFinishLoading:)])
                [self.delegate eventsDidFinishLoading:self.events];
        });
	}];
}

+ (NSString *)convertDayToString:(int)dayInt
{
    switch (dayInt) {
        case 0:
            return @"Sunday";
        case 1:
            return @"Monday";
        case 2:
            return @"Tuesday";
        case 3:
            return @"Wednesday";
        case 4:
            return @"Thursday";
        case 5:
            return @"Friday";
        case 6:
            return @"Saturday";
        default:
            return nil;
    }
}

+ (NSString *)returnLongStyleDateFormat:(NSDate *)date
{
    static NSDateFormatter *formatter = nil;
    
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEEE MMMM d, YYYY"];
    }
    
    return [formatter stringFromDate:date];
}


- (NSArray *)createDefaultTimes
{
    // Create the start date components
    NSDateComponents *beginning = [[NSDateComponents alloc] init];
    beginning.year = -1;
    beginning.month = 0;
    beginning.day = 0;
    NSDate *beginningDate = [[NSCalendar currentCalendar] dateByAddingComponents:beginning toDate:[NSDate date] options:0];
    
    // Create the end date components
    NSDateComponents *end = [[NSDateComponents alloc] init];
    end.year = 1;
    end.month = 0;
    end.day = 0;
    NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:end toDate:[NSDate date] options:0];
    
    return @[[CalendarDataSource dateAtBeginningOfDayForDate:beginningDate], [CalendarDataSource dateAtEndOfDayForDate:endDate]];
}


- (EKEvent *)eventByIndentifier:(NSString *)identifier
{
    for (EKEvent * event in self.events) if ([[event eventIdentifier] isEqualToString:identifier]) return event;
    NSLog(@"Error: There is no event with that identifier");
    return nil;
}

- (NSMutableDictionary *)dictEvents
{
    if (!_dictEvents) {
        _dictEvents = [NSMutableDictionary dictionary];
    }
    return _dictEvents;
}

- (EKEvent *)eventAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self.sections objectForKey:[self.sortedDays objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
}

- (NSString *)returnSectionHeaderText:(NSInteger)section
{
    return [self modifiedDateForSectionHeader:[self.sortedDays objectAtIndex:section]];
}

- (NSUInteger)returnNumberOfSections
{
    return [self.sections count];
}

- (NSUInteger)returnNumberOfRows:(NSInteger)inSection
{
    return [[self.sections objectForKey:[self.sortedDays objectAtIndex:inSection]] count];
}

- (void)addEvent:(EKEvent *)aEvent
{
    [self.events addObject:aEvent];
    
    NSDate *dateRepresentingThisDay = [CalendarDataSource dateAtBeginningOfDayForDate:aEvent.startDate];
    NSMutableArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    if (eventsOnThisDay == nil) {
        eventsOnThisDay = [NSMutableArray array];
        [self.sections setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
    }
    [eventsOnThisDay addObject:aEvent];
    
    self.sortedDays = [[self.sections allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    if( [self.delegate respondsToSelector:@selector(eventDidFinishSaving:) ] )
        [self.delegate eventDidFinishSaving:aEvent];
}

- (void)deleteEvent:(EKEvent *)aEvent
{
    [self.events removeObject:aEvent];
    NSDate *dateRepresentingThisDay = [CalendarDataSource dateAtBeginningOfDayForDate:aEvent.startDate];
    NSMutableArray *eventsOnThisDay = [self.sections objectForKey:dateRepresentingThisDay];
    [eventsOnThisDay removeObject:aEvent];
    [self.sections setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
    if (eventsOnThisDay == nil || [eventsOnThisDay count] == 0) [self.sections removeObjectForKey:dateRepresentingThisDay];
    self.sortedDays = [[self.sections allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    if( [self.delegate respondsToSelector:@selector(eventDidFinishDeleting:) ] )
        [self.delegate eventDidFinishDeleting:aEvent];
}


- (EKEvent *)eventAtIndexPathForSections:(NSIndexPath *)indexPath {
    
    NSArray *sections = [NSArray arrayWithArray:[self.dictEvents allKeys]];
    NSArray * sortedKeys = [sections sortedArrayUsingComparator:^(id string1, id string2) {
        return [([(NSString *)string1 substringToIndex:5]) compare:([(NSString *)string2 substringToIndex:5]) options:NSNumericSearch];
    }];
    
    NSString *key = [sortedKeys objectAtIndex:[indexPath section]];
    return [[self.dictEvents objectForKey:key] objectAtIndex:[indexPath row]];
}

- (NSString *)modifiedDateForSectionHeader:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE-MMMM-dd";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
   [dateFormatter setTimeZone:gmt];
    NSString *new = [dateFormatter stringFromDate:date];
    NSArray *strings = [new componentsSeparatedByString:@"-"];
    NSString *string = [NSString stringWithFormat:@"%@ %@ %@", [[strings objectAtIndex:0] substringToIndex:3], [[strings objectAtIndex:1] substringToIndex:3], [strings objectAtIndex:2]];
    return string;
}

- (NSString *)modifiedDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
    [dateFormatter setTimeZone:gmt];
    return [dateFormatter stringFromDate:date];
}

- (UIImage *)eventCalendarColorAtIndexPath:(NSIndexPath *)indexPath {
	UIGraphicsBeginImageContext(CGSizeMake(5.0f,5.0f));
	CGContextRef contextRef = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(contextRef, [[(EKEvent *)[self.events objectAtIndex:[indexPath row]] calendar] CGColor]);
	CGContextFillEllipseInRect(contextRef,(CGRectMake (0.f, 0.f, 5.f, 5.f)));
	UIImage *drawingImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return drawingImage;
}

+(UIImage *)eventCalendarColor:(EKEvent *)event
{
    UIGraphicsBeginImageContext(CGSizeMake(5.0f,5.0f));
	CGContextRef contextRef = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(contextRef, [[event calendar] CGColor]);
	CGContextFillEllipseInRect(contextRef,(CGRectMake (0.f, 0.f, 5.f, 5.f)));
	UIImage *drawingImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return drawingImage;
}

+ (CGColorRef)calendarColorForEvent:(EKEvent *)event
{
    return [[event calendar] CGColor];
}

- (NSMutableArray *)eventsByGivenDate:(NSDate *)givenDate
{
    if (debug) NSLog(@"eventsByGivenDate:%@", givenDate);
    NSDate *start = [[self returnTheBeginningAndEndTimesOfPassedByDate:givenDate] objectAtIndex:0];
    NSDate *end = [[self returnTheBeginningAndEndTimesOfPassedByDate:givenDate] objectAtIndex:1];
    NSMutableArray *array = [NSMutableArray array];
    for (EKEvent * event in self.events) {
        if ((([[event startDate] compare:start] == NSOrderedDescending) && ([[event startDate] compare:end] == NSOrderedAscending)) || [[event startDate] compare:start] == NSOrderedSame || [[event endDate] compare:NSOrderedSame]) {
            for (id calendar in self.chosenCalendars) {
                if ([[[event calendar] title] isEqualToString:[calendar title]]) {
                    [array addObject:event];
                }
            }
        }
    }
    return array;
}


- (NSMutableArray *)events {
	if (!_events) {
		_events = [NSMutableArray array];
        //[self loadEvents];
	}
	return _events;
}

- (EKEventStore *)store {
    if (!_store) {
        _store = [[EKEventStore alloc] init];
	}
    return _store;
}

- (NSArray *)returnTheBeginningAndEndTimesOfPassedByDate:(NSDate *)passedByDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:passedByDate];
    NSDateComponents *components1 = [NSDateComponents new];
    components1.day = 1;
    NSDate *date = [calendar dateByAddingComponents:components1 toDate:[calendar dateFromComponents:components] options:0];
    return [NSArray arrayWithObjects:[calendar dateFromComponents:components], [date dateByAddingTimeInterval:-1], nil];
}
/*** Calendar Properties
 - allowsContentModifications
 - CGColor
 - immutable
 - title
 - type
 - allowedEntityTypes
 - source
 - subscribed
 - supportedEventAvailabilities
 - calendarIdentifier
 ***/
- (void)loadCalendars:(id)sender {
    if (debug) NSLog(@"loadCalendars");
    if ([self.delegate respondsToSelector:@selector(calendarsDidFinishLoading:)] )
        [self.delegate calendarsDidFinishLoading:[self.store calendarsForEntityType:EKEntityTypeEvent]];
	self.chosenCalendars = [NSMutableArray arrayWithArray:[self.store calendarsForEntityType:EKEntityTypeEvent]];
}

- (id)parseCalendar{
	return [[self.chosenCalendars objectAtIndex:0] identifier];
}




#pragma mark - Date Calculations
+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}


+ (NSDate *)dateAtEndOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:23];
    [dateComps setMinute:59];
    [dateComps setSecond:59];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (NSDate *)dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate
{
    // Use the user's current calendar
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setYear:numberOfYears];
    
    NSDate *newDate = [calendar dateByAddingComponents:dateComps toDate:inputDate options:0];
    return newDate;
}


+ (NSMutableArray *)getDaysOfWeekGivenADayDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"d"];
    
    NSMutableArray *arraymain = [NSMutableArray array];
    
    NSString *dayName = [dateFormatter stringFromDate:date];
    
    if ([dayName isEqualToString:@"Sunday"]) {
		
		[arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];

        for (int i = 1; i < 7; i++)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: 86400.0 * i]]]];
        
		[arraymain addObject:[dateFormatter1 stringFromDate:date]];
        
    } else if ([dayName isEqualToString:@"Monday"]) {
        
		for (int i = 1; i > 0; i--)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: -86400.0 * i]]]];

		[arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];

		for (int i = 1; i < 6; i++)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: 86400.0 * i]]]];
        
        [arraymain addObject:[dateFormatter1 stringFromDate:date]];
		
        
    } else if ([dayName isEqualToString:@"Tuesday"]) {
        
        for (int i = 2; i > 0; i--)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: -86400.0 * i]]]];

        [arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];

        for (int i = 1; i < 5; i++)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: 86400.0 * i]]]];
		
		
        [arraymain addObject:[dateFormatter1 stringFromDate:date]];
		
		
    } else if ([dayName isEqualToString:@"Wednesday"]) {
        
        for (int i = 3; i > 0; i--)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: -86400.0 * i]]]];

		[arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];
		
        for (int i = 1; i < 4; i++)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: 86400.0 * i]]]];
		
        [arraymain addObject:[dateFormatter1 stringFromDate:date]];
		
		
    } else if ([dayName isEqualToString:@"Thursday"]) {
        
        for (int i = 4; i > 0; i--)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: -86400.0 * i]]]];

		[arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];

		for (int i = 1; i < 3; i++)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: 86400.0 * i]]]];
		
        [arraymain addObject:[dateFormatter1 stringFromDate:date]];
		
		
    } else if ([dayName isEqualToString:@"Friday"]) {
        
        for (int i = 5; i > 0; i--)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: -86400.0 * i]]]];

		[arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];
        ;
		for (int i = 1; i < 2; i++)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: 86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: 86400.0 * i]]]];
		
		
        [arraymain addObject:[dateFormatter1 stringFromDate:date]];
		
		
    } else if ([dayName isEqualToString:@"Saturday"]) {
        
        for (int i = 6; i > 0; i--)
            [arraymain addObject:@[[dateFormatter stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [dateFormatter1 stringFromDate:[date dateByAddingTimeInterval: -86400.0 * i]],
                                   [NSString stringWithFormat:@"%@", [date dateByAddingTimeInterval: -86400.0 * i]]]];

		[arraymain addObject:@[[dateFormatter stringFromDate:date],
                               [dateFormatter1 stringFromDate:date],
                               [NSString stringWithFormat:@"%@", date]]];
		
        [arraymain addObject:[dateFormatter1 stringFromDate:date]];
		
    }
    return arraymain;
}


- (void)setCalendars:(NSSet *)calendars
{
    
    NSMutableArray *array = [NSMutableArray array];
    for (id calendar in calendars)
        [array addObject:calendar];
 
    self.chosenCalendars = array;
    [self loadEventsWithCalendars:self.chosenCalendars];
}

+ (BOOL)isDateInToday:(NSDate*)date
{
    if ([date compare:[self dateAtBeginningOfDayForDate:[NSDate date]]] == NSOrderedAscending) return NO;
    if ([date compare:[self dateAtEndOfDayForDate:[NSDate date]]] == NSOrderedDescending) return NO;
    return YES;
}


+ (NSArray *)convertDates:(EKEvent *)event
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm";
    
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
    [dateFormatter setTimeZone:gmt];
    NSString *start = [dateFormatter stringFromDate:event.startDate];
    NSString *end = [dateFormatter stringFromDate:event.endDate];
	
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.dateFormat = @"yyyy-MM-dd";
    NSString *date = [dateFormatter1 stringFromDate:event.endDate];
    
    return [NSArray arrayWithObjects:date, start, end, nil];
}

+ (UIImage *)imageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)setSelectedDate:(NSDate *)date
{
    self.date = date;
}

- (NSDate *)date
{
    if (!_date) {
        _date = [NSDate date];
    }
    return _date;
}

- (NSInteger)returnTodaySectionHeaderNumber
{
    int num = 0;
    for (NSDate * date in self.sortedDays) {
        if ([[self modifiedDateForSectionHeader:[NSDate date]] isEqualToString:[self modifiedDateForSectionHeader:date]])
            return num;
        num++;
    }
    return -1;
}

- (EKEvent *)returnConvertedEvent:(EventfulEvent *)event {
    
    EKEvent *anEvent = [EKEvent eventWithEventStore:[[CalendarDataSource sharedInstance] store]];
    
    if ([event allDay] == 0)
        anEvent.allDay = false;
    else
        anEvent.allDay = true;
    
    anEvent.title = event.title;
    anEvent.startDate = [CalendarDataSource convertStringToDate:event.startTime];
    anEvent.endDate = [CalendarDataSource convertStringToDate:event.stopTime];
    /*
     eventIdentifier  property
     availability  property
     startDate  property
     endDate  property
     allDay  property
     isDetached  property
     organizer  property
     status  property
     birthdayPersonID  property
     recurrenceRule  p
     */
    return anEvent;
}

+ (NSDate *)dateByPreviousWeek:(NSDate *)date
{
    return [date dateByAddingTimeInterval: -86400.0 * 7];
}

+ (NSDate *)dateByFutureWeek:(NSDate *)date
{
    return [date dateByAddingTimeInterval: 86400.0 * 7];
}


+ (NSArray *) convertDatesForDayViewCell:(EKEvent *)event
{
    NSString *start = [[CalendarDataSource convertDates:event] objectAtIndex:1];
    NSArray *spliArray = [start componentsSeparatedByString:@":"];
    NSString *end   = [[CalendarDataSource convertDates:event] objectAtIndex:2];
    NSArray *spliArray1 = [end componentsSeparatedByString:@":"];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:[event startDate] toDate:[event endDate] options:0];
    NSInteger days = [components day];
    return @[spliArray, spliArray1, [NSNumber numberWithInteger:days]];
}

+ (NSDate *)convertStringToDate:(NSString *)date
{
    static NSDateFormatter *formatter = nil;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    }
    return [formatter dateFromString:date];
}

@end




