//
//  EventDataSource.h
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EventfulDataSourceDelegate;

@interface EventDataSource : NSObject

@property (nonatomic, weak) id<EventfulDataSourceDelegate>delegate;

+ (id)sharedInstance;
- (void)getEventsByLocation:(NSString *)location;
- (void)getEventsByCurrentLocation;
@end

@protocol EventfulDataSourceDelegate <NSObject>
@required
- (void)eventsDidFinishLoading:(NSArray *)events;
- (void)eventsDidFinishWithError:(NSError *)error;
@end