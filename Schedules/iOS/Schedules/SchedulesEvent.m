//
//  SchedulesEvent.m
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SchedulesEvent.h"

#define kEventIdKey                         @"EventId"
#define kTitleKey                           @"Title"
#define kLocationKey                        @"Location"
#define kDescriptionKey                     @"Description"
#define kCalendarTitleKey                   @"CalendarTitle"
//#define kCalendarTypeKey                    @"CalendarType"
//#define kCalendarAllowsModificationKey      @"CalendarAllowsModification"
//#define kCalendarColorKey                   @"CalendarColor"
//#define kAlarmsKey                          @"Alarms"
#define kURLKey                             @"URL"
//#define kLastModifiedKey                    @"LastModifed"
#define kTimeZoneKey                        @"TimeZone"
#define kStartDateKey                       @"StartDate"
#define kEndDateKey                         @"EndDate"
#define kAllDayKey                          @"AllDay"
//#define kFloatingKey                        @"Floating"
//#define kRecurrenceKey                      @"Recurrence"
//#define kAttendeesKey                       @"Attendees"
#define kEventIdentifierKey                 @"EventIdentifier"

@implementation SchedulesEvent
@synthesize SeventId;
@synthesize Stitle;
@synthesize Slocation;
@synthesize Sdescription;
@synthesize ScalendarTitle;
//@synthesize ScalendarType;
//@synthesize ScalendarAllowsModify;
//@synthesize ScalendarColor;
//@synthesize Salarms;
@synthesize SURL;
//@synthesize SlastModified;
@synthesize StimeZone;
@synthesize SstartDate;
@synthesize SendDate;
@synthesize SallDay;
//@synthesize Sfloating;
//@synthesize Srecurrence;
//@synthesize Sattendees;
@synthesize SeventIdentifier;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.SeventId = [aDecoder decodeObjectForKey:kEventIdKey];
        self.Stitle = [aDecoder decodeObjectForKey:kTitleKey];
        self.Slocation = [aDecoder decodeObjectForKey:kLocationKey];
        self.Sdescription = [aDecoder decodeObjectForKey:kDescriptionKey];
        self.ScalendarTitle = [aDecoder decodeObjectForKey:kCalendarTitleKey];
        //self.ScalendarType = [aDecoder decodeObjectForKey:kCalendarTypeKey];
        //self.ScalendarAllowsModify = [aDecoder decodeBoolForKey:kCalendarAllowsModificationKey];
        //self.ScalendarColor = [aDecoder decodeObjectForKey:kCalendarColorKey];
        //self.Salarms = [aDecoder decodeObjectForKey:kAlarmsKey];
        self.SURL = [aDecoder decodeObjectForKey:kURLKey];
        //self.SlastModified = [aDecoder decodeObjectForKey:kLastModifiedKey];
        self.StimeZone = [aDecoder decodeObjectForKey:kTimeZoneKey];
        self.SstartDate = [aDecoder decodeObjectForKey:kStartDateKey];
        self.SendDate = [aDecoder decodeObjectForKey:kEndDateKey];
        self.SallDay = [aDecoder decodeBoolForKey:kAllDayKey];
        //self.Sfloating = [aDecoder decodeBoolForKey:kFloatingKey];
        //self.Srecurrence = [aDecoder decodeObjectForKey:kRecurrenceKey];
        //self.Sattendees = [aDecoder decodeObjectForKey:kAttendeesKey];
        self.SeventIdentifier = [aDecoder decodeObjectForKey:kEventIdentifierKey];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:SeventId forKey:kEventIdKey];
    [aCoder encodeObject:Stitle forKey:kTitleKey];
    [aCoder encodeObject:Slocation forKey:kLocationKey];
    [aCoder encodeObject:Sdescription forKey:kDescriptionKey];
    [aCoder encodeObject:ScalendarTitle forKey:kCalendarTitleKey];
    //[aCoder encodeObject:ScalendarType forKey:kCalendarTypeKey];
    //[aCoder encodeBool:ScalendarAllowsModify forKey:kCalendarAllowsModificationKey];
    //[aCoder encodeObject:ScalendarColor forKey:kCalendarColorKey];
    //[aCoder encodeObject:Salarms forKey:kAlarmsKey];
    [aCoder encodeObject:SURL forKey:kURLKey];
    //[aCoder encodeObject:SlastModified forKey:kLastModifiedKey];
    [aCoder encodeObject:StimeZone forKey:kTimeZoneKey];
    [aCoder encodeObject:SstartDate forKey:kStartDateKey];
    [aCoder encodeObject:SendDate forKey:kEndDateKey];
    [aCoder encodeBool:SallDay forKey:kAllDayKey];
    //[aCoder encodeBool:floating forKey:kFloatingKey];
    //[aCoder encodeObject:Srecurrence forKey:kRecurrenceKey];
    //[aCoder encodeObject:Sattendees forKey:kAttendeesKey];
    [aCoder encodeObject:SeventIdentifier forKey:kEventIdentifierKey];
}

@end
