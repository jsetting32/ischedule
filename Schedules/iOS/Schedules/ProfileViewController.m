//
//  ProfileViewController.m
//  Schedules
//
//  Created by John Setting on 10/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize profileTableView;
@synthesize refreshControl;
@synthesize events;
@synthesize headerView;
@synthesize dict;
@synthesize sortedDays;
@synthesize tapRecognizer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = [NSString stringWithFormat:@"%@ %@", [PFUser currentUser][@"firstName"], [PFUser currentUser][@"lastName"]];
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];

    configuredView = NO;
    [self createTableHeaderView];
    [self formatHeaderView];
    [self configureLabels];
    [self loadNavigationBarItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self queryForHeaderLabels];

    if (!configuredView) {
        PFQuery *query = [PFQuery queryWithClassName:@"Events"];
        query.limit = 1000;
        [query orderByAscending:@"startDate"];
        [query whereKey:@"eventId" equalTo:[[PFUser currentUser] objectId]];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            events = [[NSMutableArray alloc] initWithArray:objects];
            dict = [NSMutableDictionary dictionary];
            
            for (SchedulesEvent *event in objects)
            {
                // Reduce event start date to date components (year, month, day)
                NSDate *dateRepresentingThisDay = [self dateAtBeginningOfDayForDate:[event valueForKey:@"startDate"]];
                
                // If we don't yet have an array to hold the events for this day, create one
                NSMutableArray *eventsOnThisDay = [dict objectForKey:dateRepresentingThisDay];
                if (eventsOnThisDay == nil) {
                    eventsOnThisDay = [NSMutableArray array];
                    
                    // Use the reduced date as dictionary key to later retrieve the event list this day
                    [dict setObject:eventsOnThisDay forKey:dateRepresentingThisDay];
                }
                // Add the event to the list for this day
                [eventsOnThisDay addObject:event];
            }
            
            sortedDays = (NSMutableArray *)[[dict allKeys] sortedArrayUsingSelector:@selector(compare:)];
            
            [self.profileTableView reloadData];
            configuredView = YES;
        }];
    }
}

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void) loadNavigationBarItems
{
    
    mainController = [self revealViewController];
    mainController.delegate = self;
    [self.navigationController.navigationBar addGestureRecognizer:mainController.panGestureRecognizer];
    
    UIBarButtonItem *leftRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:mainController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = leftRevealButtonItem;
    
    UIBarButtonItem *revealButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:mainController action:@selector(rightRevealToggle:)];
    
    UIBarButtonItem *fixed = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixed.width = -100.0f;
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:revealButton, nil];
}

- (void) presentEventAddController {
    EventAddController *event = [[EventAddController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:event];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void) createTableHeaderView
{
    mainController = [self revealViewController];
    mainController.delegate = self;
    [self.navigationController.view addGestureRecognizer:mainController.panGestureRecognizer];
    
    profileTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height) style:UITableViewStyleGrouped];
    [profileTableView setDelegate:self];
    [profileTableView setDataSource:self];
    [self.view addSubview:profileTableView];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [profileTableView addSubview:refreshControl];

    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    [headerView setBackgroundColor:[UIColor grayColor]];
    profileTableView.tableHeaderView = headerView;
}

- (void) handleRefresh {
    [refreshControl beginRefreshing];
    PFQuery *query = [PFQuery queryWithClassName:@"Events"];
    query.limit = 1000;
    [query orderByAscending:@"startDate"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        events = [NSMutableArray arrayWithObject:objects];
        [self.profileTableView reloadData];
        configuredView = YES;
        [refreshControl endRefreshing];
    }];
}

- (void) formatHeaderView
{
	coverPicture = [UIButton buttonWithType:UIButtonTypeCustom];
	coverPicture.frame = CGRectMake(0, 0, 320, 150);
	[[coverPicture imageView] setContentMode:UIViewContentModeScaleAspectFill];
    [coverPicture addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [coverPicture setTag:1];
	[headerView addSubview:coverPicture];
    
    profilePicture = [UIButton buttonWithType:UIButtonTypeCustom];
	profilePicture.frame = CGRectMake(120, 35, 80, 80);
	[[profilePicture imageView] setContentMode:UIViewContentModeScaleAspectFill];
	profilePicture.layer.cornerRadius = 40.0f;
	profilePicture.layer.borderColor = [[UIColor whiteColor] CGColor];
	profilePicture.layer.borderWidth = 2.0f;
	profilePicture.layer.masksToBounds = YES;
	[profilePicture addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [profilePicture setTag:0];
    [headerView addSubview:profilePicture];

    
    PFQuery *profileUser = [PFUser query];
    [profileUser whereKey:@"objectId" equalTo:[[PFUser currentUser] objectId]];
    [profileUser findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (objects.count > 0) {
                PFObject *user = [objects objectAtIndex:0];

                [[user objectForKey:@"profilePicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                    if (!data)
                        [profilePicture setImage:[UIImage imageNamed:@"AvatarPlaceholder.png"] forState:UIControlStateNormal];
                    else
                        [profilePicture setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
                }];
                
                [[user objectForKey:@"coverPicture"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                    if (!data)
                        [coverPicture setImage:[UIImage imageNamed:@"AvatarPlaceholder.png"] forState:UIControlStateNormal];
                    else
                        [coverPicture setImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
                }];
            }
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Error finding objects" delegate:Nil cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
        }
    }];
}

- (void) queryForHeaderLabels
{
    
    PFQuery *friends = [PFQuery queryWithClassName:@"Activity"];
    [friends whereKey:@"type" equalTo:@"friend"];
    [friends whereKey:@"fromUser" equalTo:[PFUser currentUser]];
    
    PFQuery *friended = [PFQuery queryWithClassName:@"Activity"];
    [friended whereKey:@"type" equalTo:@"friend"];
    [friended whereKey:@"toUser" equalTo:[PFUser currentUser]];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:@[friended, friends]];
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [friendsmade setTitle:[NSString stringWithFormat:@"%d friend%@", number, number==1?@"":@"s"] forState:UIControlStateNormal];
        } else {
            NSLog(@"%@", error);
        }
    }];
    
    PFQuery *queryCount = [PFQuery queryWithClassName:@"Activity"];
    [queryCount whereKey:@"type" equalTo:@"pending"];
    [queryCount whereKey:@"fromUser" equalTo:[PFUser currentUser]];
    [queryCount setCachePolicy:kPFCachePolicyNetworkElseCache];
    [queryCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [sentrequests setTitle:[NSString stringWithFormat:@"%d request%@", number, number==1?@"":@"s"] forState:UIControlStateNormal];
        } else {
            NSLog(@"%@", error);
        }
    }];
    
    PFQuery *queryPendingCount = [PFQuery queryWithClassName:@"Activity"];
    [queryPendingCount whereKey:@"type" equalTo:@"pending"];
    [queryPendingCount whereKey:@"toUser" equalTo:[PFUser currentUser]];
    [queryPendingCount setCachePolicy:kPFCachePolicyNetworkElseCache];
    [queryPendingCount countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        if (!error) {
            [friendrequests setTitle:[NSString stringWithFormat:@"%d request%@", number, number==1?@"":@"s"] forState:UIControlStateNormal];
        } else {
            NSLog(@"%@", error);
        }
    }];
}

- (void) configureLabels
{
    friendsmade = [UIButton buttonWithType:UIButtonTypeCustom];
    friendsmade.frame = CGRectMake(0, 150, 107, 40);
    //[friends setBackgroundImage:[UIImage imageNamed:@"buttonbg.jpg"] forState:UIControlStateNormal];
    [friendsmade.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [friendsmade setTitleEdgeInsets:UIEdgeInsetsMake( 0.0f, 10.0f, 0.0f, 0.0f)];
    [friendsmade setTitleColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [friendsmade setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [friendsmade.titleLabel setShadowOffset:CGSizeMake(1.0f, -2.0f)];
    [friendsmade addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [friendsmade setTag:2];
    [headerView addSubview:friendsmade];
    
    sentrequests = [UIButton buttonWithType:UIButtonTypeCustom];
    sentrequests.frame = CGRectMake(107, 150, 106, 40);
    //[sentrequests setBackgroundImage:[UIImage imageNamed:@"buttonbg.jpg"] forState:UIControlStateNormal];
    [sentrequests.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [sentrequests setTitleEdgeInsets:UIEdgeInsetsMake( 0.0f, 10.0f, 0.0f, 0.0f)];
    [sentrequests setTitleColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [sentrequests setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [sentrequests.titleLabel setShadowOffset:CGSizeMake(1.0f, -2.0f)];
    [sentrequests addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [sentrequests setTag:3];
    [headerView addSubview:sentrequests];
    
    friendrequests = [UIButton buttonWithType:UIButtonTypeCustom];
    friendrequests.frame = CGRectMake(213, 150, 107, 40);
    //[friendrequests setBackgroundImage:[UIImage imageNamed:@"buttonbg.jpg"] forState:UIControlStateNormal];
    [friendrequests.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [friendrequests setTitleEdgeInsets:UIEdgeInsetsMake( 0.0f, 10.0f, 0.0f, 0.0f)];
    [friendrequests setTitleColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [friendrequests setTitleShadowColor:[UIColor colorWithRed:232.0f/255.0f green:203.0f/255.0f blue:168.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [friendrequests.titleLabel setShadowOffset:CGSizeMake(1.0f, -2.0f)];
    [friendrequests addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [friendrequests setTag:4];
    [headerView addSubview:friendrequests];
    
    [friendsmade setTitle:@"0 friends" forState:UIControlStateNormal];
    [[friendsmade titleLabel] setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [sentrequests setTitle:@"0 requests" forState:UIControlStateNormal];
    [[sentrequests titleLabel] setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    [friendrequests setTitle:@"0 requests" forState:UIControlStateNormal];
    [[friendrequests titleLabel] setFont:[UIFont fontWithName:@"Arial" size:12.0f]];

}


#pragma mark - Table view data source
// Add new methods
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CustomHeaderView* header = [[CustomHeaderView alloc] init];
    
    UIColor * light = [UIColor colorWithRed:105.0f/255.0f green:179.0f/255.0f blue:216.0f/255.0f alpha:1.0];
    UIColor *dark = [UIColor colorWithRed:21.0/255.0 green:92.0/255.0 blue:136.0/255.0 alpha:1.0];

    header.lightColor = light;
    header.darkColor = dark;
    
    NSDate *dateRepresentingThisDay = [sortedDays objectAtIndex:section];
	
    header.leftTitleLabel.text = [NSString stringWithFormat:@"%@ %@", [[self dateFormatter:dateRepresentingThisDay] objectAtIndex:2], [[self dateFormatter:dateRepresentingThisDay] objectAtIndex:3]];
	header.rightTitleLabel.text = [[self dateFormatter:dateRepresentingThisDay] objectAtIndex:1];
	
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[CustomFooterView alloc] init];
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDate *dateRepresentingThisDay = [sortedDays objectAtIndex:section];
    NSArray *eventsOnThisDay = [dict objectForKey:dateRepresentingThisDay];
    return [eventsOnThisDay count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dict count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"EventsTableViewCell";
	EventsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	
	if (cell == nil) {
    	cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
	}
	
	[cell setDelegate:self];
	
	if (![cell.backgroundView isKindOfClass:[CustomCellBackground class]]) {
		CustomCellBackground *backgroundCell = [[CustomCellBackground alloc] init];
		cell.backgroundView = backgroundCell;
	}
	
	if (![cell.selectedBackgroundView isKindOfClass:[CustomCellBackground class]]) {
		CustomCellBackground *selectedBackgroundCell = [[CustomCellBackground alloc] init];
		selectedBackgroundCell.selected = YES;
		cell.selectedBackgroundView = selectedBackgroundCell;
	}
	
	((CustomCellBackground *) cell.backgroundView).lastCell = indexPath.section == dict.count - 1;
    ((CustomCellBackground *) cell.selectedBackgroundView).lastCell = indexPath.section == dict.count - 1;
    
    
	NSDate *dateRepresentingThisDay = [sortedDays objectAtIndex:indexPath.section];
    NSArray *eventsOnThisDay = [dict objectForKey:dateRepresentingThisDay];
	SchedulesEvent *event = [eventsOnThisDay objectAtIndex:indexPath.row];
	[cell setCellForEvent:event];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventDetailController *view = [[EventDetailController alloc] init];
    view.event = [events objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:view animated:YES];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        EventsTableViewCell *cell = (EventsTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSLog(@"%@", [[cell eventDescription] text]);
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    } else {
        // editingStyle == UITableViewCellEditingStyleNone
    }
}

-(void)buttonPressed:(UIButton *)sender
{
    switch ([sender tag]) {
        case 0:
        {
            if (!([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])) {
                return;
            } else {
                NSString *viewPhoto = @"View Profile photo";
                NSString *takePhoto = @"Take Photo";
                NSString *editPhoto = @"Choose Existing Photo";
                NSString *cancelTitle = @"Cancel";
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Profile Photo" delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:viewPhoto, takePhoto, editPhoto, nil];
                [actionSheet showInView:self.navigationController.view];
            }
            break;
        }
            
        case 1:
        {
            if (!([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])) {
                return;
            } else {
                NSString *viewPhoto = @"View Cover photo";
                NSString *takePhoto = @"Take Photo";
                NSString *editPhoto = @"Choose Existing Photo";
                NSString *cancelTitle = @"Cancel";
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Cover Photo" delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:viewPhoto, takePhoto, editPhoto, nil];
                [actionSheet showInView:self.navigationController.view];
            }
            break;
        }
        
        case 2:
        {
            FriendsViewController *view = [[FriendsViewController alloc] init];
            view.title = @"Friends";
            [self.navigationController pushViewController:view animated:YES];
            break;
        }
            
        case 3:
        {
            FriendsViewController *view = [[FriendsViewController alloc] init];
            view.title = @"Sent Requests";
            [self.navigationController pushViewController:view animated:YES];
            break;
        }
        
        case 4:
        {
            FriendsViewController *view = [[FriendsViewController alloc] init];
            view.title = @"Friend Requests";
            [self.navigationController pushViewController:view animated:YES];
            break;
        }
        default:
            break;
    }
}

- (NSArray *)dateFormatter:(NSDate *)date
{
	
	static NSDateFormatter *stringToDateFormatter = nil;
	if (nil == stringToDateFormatter) {
		stringToDateFormatter = [[NSDateFormatter alloc] init];
		[stringToDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	}
	
	static NSDateFormatter *dateToStringFormatter = nil;
	if (nil == dateToStringFormatter) {
		dateToStringFormatter = [[NSDateFormatter alloc] init];
		[dateToStringFormatter setDateFormat:@"hh:mm a"];
	}
	
	static NSDateFormatter *monthFormatter = nil;
	if (nil == monthFormatter) {
		monthFormatter = [[NSDateFormatter alloc] init];
		[monthFormatter setDateFormat:@"MMMM yyyy"];
	}
	
	static NSDateFormatter *dayFormatter = nil;
	if (nil == dayFormatter)
	{
		dayFormatter = [[NSDateFormatter alloc] init];
		[dayFormatter setDateFormat:@"dd"];
	}
	
	static NSDateFormatter *dayStringFormatter = nil;
	if (nil == dayStringFormatter)
	{
		dayStringFormatter = [[NSDateFormatter alloc] init];
		[dayStringFormatter setDateFormat:@"EEEE"];
	}
	
	NSArray *array = [[NSArray alloc] initWithObjects:[dateToStringFormatter stringFromDate:date], [monthFormatter stringFromDate:date], [dayFormatter stringFromDate:date], [dayStringFormatter stringFromDate:date], nil];
	
	return array;
}

- (BOOL)shouldStartCameraController {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
		[[[UIAlertView alloc] initWithTitle:@"Simulator" message:@"Camera is not available while in the simulator" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
        && [[UIImagePickerController availableMediaTypesForSourceType:
             UIImagePickerControllerSourceTypeCamera] containsObject:(NSString *) kUTTypeImage]) {
        
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        } else if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    cameraUI.showsCameraControls = YES;
    cameraUI.delegate = self;

    [self presentViewController:cameraUI animated:YES completion:nil];

    return YES;
}


- (BOOL)shouldStartPhotoLibraryPickerController {
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] == NO
         && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary] containsObject:(NSString *)kUTTypeImage]) {
		
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]
               && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // Access the uncropped image from info dictionary

    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // Resize image
    UIGraphicsBeginImageContext(CGSizeMake(640, 960));
    [image drawInRect: CGRectMake(0, 0, 640, 960)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Upload image
    NSData *imageData = UIImageJPEGRepresentation(smallImage, 0.05f);
    
    if ([whichPhotoDidUserClickToEdit isEqualToString:@"Profile Photo"]) {
        [profilePicture setImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
        [self uploadImage:imageData name:whichPhotoDidUserClickToEdit];
    } else if ([whichPhotoDidUserClickToEdit isEqualToString:@"Cover Photo"]) {
        [coverPicture setImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
        [self uploadImage:imageData name:whichPhotoDidUserClickToEdit];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([actionSheet.title isEqualToString:@"Profile Photo"]) {
        whichPhotoDidUserClickToEdit = @"Profile Photo";
        if (buttonIndex == 0) {
            PhotoDetailViewController *pdvc = [[PhotoDetailViewController alloc] init];
            pdvc.selectedImage = profilePicture.imageView.image;
			[self presentViewController:[[UINavigationController alloc] initWithRootViewController:pdvc] animated:YES completion:nil];
		} else if (buttonIndex == 1) {
            [self shouldStartCameraController];
        } else if (buttonIndex == 2) {
            [self shouldStartPhotoLibraryPickerController];
        }
    } else if ([actionSheet.title isEqualToString:@"Cover Photo"]) {
        whichPhotoDidUserClickToEdit = @"Cover Photo";
        if (buttonIndex == 0) {
            PhotoDetailViewController *pdvc = [[PhotoDetailViewController alloc] init];
            pdvc.selectedImage = coverPicture.imageView.image;
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:pdvc] animated:YES completion:nil];
        } else if (buttonIndex == 1) {
            [self shouldStartCameraController];
        } else if (buttonIndex == 2) {
            [self shouldStartPhotoLibraryPickerController];
        }
    }
}


- (void)uploadImage:(NSData *)imageData name:(NSString *)name
{
    PFFile *imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"%@.png", name] data:imageData];
    
    // Save PFFile
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {

            // Create a PFObject around a PFFile and associate it with the current user
            PFUser *user = [PFUser currentUser];
            
            if ([name isEqualToString:@"Cover Photo"]) {
                [user setObject:imageFile forKey:@"coverPicture"];
            } else if ([name isEqualToString:@"Profile Photo"]) {
                [user setObject:imageFile forKey:@"profilePicture"];
            }
            
            // Set the access control list to current user for security purposes
            user.ACL = [PFACL ACLWithUser:[PFUser currentUser]];

            [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"Successful photo upload");
                }
                else{
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

// The following delegate methods will be called before and after the front view moves to a position
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position {
    if (position == FrontViewPositionRight) {
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTouchLeft:)];
        [self.navigationController.view addGestureRecognizer:tapRecognizer];
    } else if (position == FrontViewPositionLeftSide) {
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTouchRight:)];
        [self.navigationController.view addGestureRecognizer:tapRecognizer];
    } else if (position == FrontViewPositionLeft) {
        [self.navigationController.view removeGestureRecognizer:tapRecognizer];
        tapRecognizer = nil;
    }
}

- (void) handleTouchLeft: (UITapGestureRecognizer *)recognizer {
    [mainController revealToggle:self];
}

- (void) handleTouchRight: (UITapGestureRecognizer *)recognizer
{
    [mainController rightRevealToggle:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
