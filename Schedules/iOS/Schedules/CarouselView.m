//
//  CarouselView.m
//  iSchedj
//
//  Created by John Setting on 3/22/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import "CarouselView.h"
#import "CalendarDataSource.h"

@interface CarouselView()

@property (nonatomic) UIView *scrollView;

@property (nonatomic) UIButton *sunday;
@property (nonatomic) UIButton *monday;
@property (nonatomic) UIButton *tuesday;
@property (nonatomic) UIButton *wednesday;
@property (nonatomic) UIButton *thursday;
@property (nonatomic) UIButton *friday;
@property (nonatomic) UIButton *saturday;

@property (nonatomic) UILabel *sundayLabel;
@property (nonatomic) UILabel *mondayLabel;
@property (nonatomic) UILabel *tuesdayLabel;
@property (nonatomic) UILabel *wednesdayLabel;
@property (nonatomic) UILabel *thursdayLabel;
@property (nonatomic) UILabel *fridayLabel;
@property (nonatomic) UILabel *saturdayLabel;

@property (nonatomic) UILabel *dateLabel;
@property (nonatomic) NSArray *buttons;
@property (nonatomic) NSArray *labels;

@property (nonatomic) UISwipeGestureRecognizer *leftSwipe;
@property (nonatomic) UISwipeGestureRecognizer *rightSwipe;

@property (nonatomic) int selectedButton;
@end


@implementation CarouselView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.scrollView = [[UIView alloc] init];
        //[self.scrollView setBackgroundColor:[UIColor blueColor]];
        //self.scrollView.showsHorizontalScrollIndicator = NO;
        //self.scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:self.scrollView];
        
        self.dateLabel = [[UILabel alloc] init];
        [self.dateLabel setTextAlignment:NSTextAlignmentCenter];
        [self.dateLabel setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
        [self addSubview:self.dateLabel];
        
        
        for (UIButton * button in self.buttons) {

            [button setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor blackColor]] forState:UIControlStateHighlighted];
            [button setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor blackColor]] forState:UIControlStateSelected];
            
            [[button titleLabel] setFont:[UIFont fontWithName:@"Arial" size:18.0f]];
            [[button titleLabel] setTextAlignment:NSTextAlignmentCenter];
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [[button layer] setCornerRadius:15];
            [button setClipsToBounds:YES];
            [button addTarget:self action:@selector(buttonHighlight:) forControlEvents:UIControlEventTouchDown];
            [self.scrollView addSubview:button];
        }
        
        for (UILabel * label in self.labels) {
            [label setFont:[UIFont fontWithName:@"Arial" size:8.0f]];
            [label setTextColor:[UIColor blackColor]];
            [label setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:label];
        }

        self.leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeMethod:)];
        [self.leftSwipe setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        
        self.rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeMethod:)];
        [self.rightSwipe setDirection:(UISwipeGestureRecognizerDirectionRight)];

        [self.scrollView addGestureRecognizer:self.leftSwipe];
        [self.scrollView addGestureRecognizer:self.rightSwipe];

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //Drawing code
}
*/

- (void)swipeMethod:(UISwipeGestureRecognizer *)sender
{
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self.delegate didSwipeLeft:sender];
    } else if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        [self.delegate didSwipeRight:sender];
    } else {
        
    }
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.scrollView setFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y + 18, self.bounds.size.width, 30)];

    for (int i = 0; i < 7; i++) {
        [[self.buttons objectAtIndex:i] setFrame:CGRectMake(10 + 45*i, 0, 30, 30)];
        [self.scrollView bringSubviewToFront:[self.buttons objectAtIndex:i]];
    }

    for (int i = 0; i < 7; i++) {
        [[self.labels objectAtIndex:i] setFrame:CGRectMake(9 + 45*i, 0, 30, 15)];
    }
    
    [self.dateLabel setFrame:CGRectMake(0, 45, self.bounds.size.width, 25)];
}

- (int)returnButtonIndex:(UIButton *)sender
{
    for (int i = 0; i < [self.buttons count]; i++)
        if ([sender isEqual:[self.buttons objectAtIndex:i]])
            return i;
    return -1;
}

- (void)buttonHighlight:(UIButton *)sender {
    
    
    [UIView animateWithDuration:.2 animations:^{
        [self.delegate didTapDateButton:@[[[[self.buttons objectAtIndex:[self returnButtonIndex:sender]] titleLabel] text],
                                          [NSNumber numberWithInt:[self returnButtonIndex:sender]]]];
        
        [self setAllButtonsToUnselected];
        dispatch_async(dispatch_get_main_queue(), ^{
                [sender setSelected:YES];
                self.selectedButton = [self returnButtonIndex:sender];
        });
    
    }];
}

- (void)setAllButtonsToUnselected
{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (UIButton *button in self.buttons) [button setSelected:NO];
    });
}

- (void)setFullDateLabel:(NSDate *)date
{
    [self.dateLabel setText:[NSString stringWithFormat:@"%@", [CalendarDataSource returnLongStyleDateFormat:date]]];
}

- (BOOL)setWeek:(NSArray *)days withDate:(NSDate *)date
{
    [self setAllButtonsToUnselected];
    [self setFullDateLabel:date];

    for (int i = 0; i < 7; i++) {
        [[self.labels objectAtIndex:i] setText:[[[days objectAtIndex:i] objectAtIndex:0] substringToIndex:1]];
        if (i == 0 || i == 6) [[self.labels objectAtIndex:i] setTextColor:[UIColor grayColor]];
    }
    
    for (int i = 0; i < 7; i++) {
        
        [[self.buttons objectAtIndex:i] setTitle:[[days objectAtIndex:i] objectAtIndex:1] forState:UIControlStateNormal];
        
        NSDate * date = [CalendarDataSource convertStringToDate:[[days objectAtIndex:i] objectAtIndex:2]];
        if ([CalendarDataSource isDateInToday:date]) {
            [[self.buttons objectAtIndex:i] setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor orangeColor]] forState:UIControlStateSelected];
            [[self.buttons objectAtIndex:i] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[self.buttons objectAtIndex:i] setSelected:YES];
                [self.delegate didTapDateButton:@[[[[self.buttons objectAtIndex:i] titleLabel] text],
                                                  [NSNumber numberWithInt:[self returnButtonIndex:[self.buttons objectAtIndex:i]]]]];
                self.selectedButton = i;
            });
        } else {
            [[self.buttons objectAtIndex:i] setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor blackColor]] forState:UIControlStateHighlighted];
            [[self.buttons objectAtIndex:i] setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor blackColor]] forState:UIControlStateSelected];
            if (i == 0 || i == 6) [[self.buttons objectAtIndex:i] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            else [[self.buttons objectAtIndex:i] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
    
    
    
    return YES;
}


- (BOOL)setOtherWeek:(NSArray *)days withDate:(NSDate *)date
{
    [self setFullDateLabel:date];
    [self setAllButtonsToUnselected];
    
    
    for (int i = 0; i < 7; i++) {
        [[self.labels objectAtIndex:i] setText:[[[days objectAtIndex:i] objectAtIndex:0] substringToIndex:1]];
        if (i == 0) {
            [[self.labels objectAtIndex:i] setTextColor:[UIColor lightGrayColor]];
        } else if (i == 6) {
            [[self.labels objectAtIndex:i] setTextColor:[UIColor lightGrayColor]];
        }
    }
    
    for (int i = 0; i < 7; i++) {
        [[self.buttons objectAtIndex:i] setTitle:[[days objectAtIndex:i] objectAtIndex:1] forState:UIControlStateNormal];
        NSDate * date = [CalendarDataSource convertStringToDate:[[days objectAtIndex:i] objectAtIndex:2]];
        if ([CalendarDataSource isDateInToday:date]) {
            [[self.buttons objectAtIndex:i] setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor orangeColor]] forState:UIControlStateSelected];
            [[self.buttons objectAtIndex:i] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        } else {
            [[self.buttons objectAtIndex:i] setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor blackColor]] forState:UIControlStateHighlighted];
            [[self.buttons objectAtIndex:i] setBackgroundImage:[CalendarDataSource imageFromColor:[UIColor blackColor]] forState:UIControlStateSelected];
            [[self.buttons objectAtIndex:i] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            if (i == 0 || i == 6) [[self.buttons objectAtIndex:i] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            else [[self.buttons objectAtIndex:i] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self.buttons objectAtIndex:self.selectedButton] setSelected:YES];
    });
    [self.delegate didTapDateButton:@[[[[self.buttons objectAtIndex:self.selectedButton] titleLabel] text],
                                      [NSNumber numberWithInt:[self returnButtonIndex:[self.buttons objectAtIndex:self.selectedButton]]]]];

    
    
    return YES;
}



#pragma mark - Setter Methods
- (NSArray *)buttons
{
    if (!_buttons) {
        _buttons = [NSArray arrayWithObjects:self.sunday,
					self.monday,
					self.tuesday,
					self.wednesday,
					self.thursday,
					self.friday,
					self.saturday,
                    nil];
    }
    return _buttons;
}

- (NSArray *)labels
{
    if (!_labels) {
        _labels = [NSArray arrayWithObjects:self.sundayLabel,
                   self.mondayLabel,
                   self.tuesdayLabel,
                   self.wednesdayLabel,
                   self.thursdayLabel,
                   self.fridayLabel,
                   self.saturdayLabel,
                   nil];
    }
    return _labels;
}

- (UILabel *)sundayLabel
{
    if (!_sundayLabel) _sundayLabel = [[UILabel alloc] init];
    return _sundayLabel;
}
- (UILabel *)mondayLabel
{
    if (!_mondayLabel) _mondayLabel = [[UILabel alloc] init];
    return _mondayLabel;
}
- (UILabel *)tuesdayLabel
{
    if (!_tuesdayLabel) _tuesdayLabel = [[UILabel alloc] init];
    return _tuesdayLabel;
}
- (UILabel *)wednesdayLabel
{
    if (!_wednesdayLabel) _wednesdayLabel = [[UILabel alloc] init];
    return _wednesdayLabel;
}
- (UILabel *)thursdayLabel
{
    if (!_thursdayLabel) _thursdayLabel = [[UILabel alloc] init];
    return _thursdayLabel;
}
- (UILabel *)fridayLabel
{
    if (!_fridayLabel) _fridayLabel = [[UILabel alloc] init];
    return _fridayLabel;
}
- (UILabel *)saturdayLabel
{
    if (!_saturdayLabel) _saturdayLabel = [[UILabel alloc] init];
    return _saturdayLabel;
}

- (UIButton *)sunday
{
    if (!_sunday) _sunday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _sunday;
}

- (UIButton *)monday
{
    if (!_monday) _monday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _monday;
}

- (UIButton *)tuesday
{
    if (!_tuesday) _tuesday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _tuesday;
}

- (UIButton *)wednesday
{
    if (!_wednesday) _wednesday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _wednesday;
}

- (UIButton *)thursday
{
    if (!_thursday) _thursday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _thursday;
}

- (UIButton *)friday
{
    if (!_friday) _friday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _friday;
}

- (UIButton *)saturday
{
    if (!_saturday) _saturday = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    return _saturday;
}

@end
