//
//  EventfulAPI.h
//  iSchedj
//
//  Created by John Setting on 3/17/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EventfulAPIDelegate;
@interface EventfulAPI : NSObject

@property (nonatomic, weak) id<EventfulAPIDelegate>delegate;

// This method downloads all events from eventful
// given a specified location, city or state, etc.
- (void) getEventsByLocation:(NSString *)location;

// This method downloads all events from eventful
// given a venue name
- (void) getEventsByVenue:(NSString *)venue location:(NSString *)location;
@end

@protocol EventfulAPIDelegate <NSObject>
@required
- (void)didFinishLoadingEvents:(id)events;
- (void)fetchQuery:(id)query didFailWithError:(NSError *)error;
@end
