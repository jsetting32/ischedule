//
//  EventDetailController.h
//  Schedules
//
//  Created by John Setting on 11/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchedulesEvent.h"

@interface EventDetailController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    UITableView     *   eventTableView;
    PFObject        *   event;
}

@property (nonatomic, retain) UITableView       *   eventTableView;
@property (nonatomic, strong) PFObject          *   event;
@end
