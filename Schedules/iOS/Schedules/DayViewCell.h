//
//  DayViewCell.h
//  iSchedj
//
//  Created by John Setting on 3/20/14.
//  Copyright (c) 2014 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "EventfulEvent.h"

@protocol DayViewCellDelegate;
@interface DayViewCell : UIView
@property (nonatomic, weak) id <DayViewCellDelegate>delegate;

+ (DayViewCell*) eventView;
+ (DayViewCell *)eventViewWithEvent:(EKEvent *)event;
+ (DayViewCell *)eventViewWIthEventfulEvent:(EventfulEvent *)event;
- (CGFloat) contentHeight;
- (NSArray *)getInfo;
- (void)setEdgeColor:(UIColor *)color;
@end

@protocol DayViewCellDelegate <NSObject>
- (void)didTapEventView:(DayViewCell *)dayViewCell recognizer:(UITapGestureRecognizer *)recognizer;
- (void)didPanEventView:(DayViewCell *)dayViewCell recognizer:(UIPanGestureRecognizer *)recognizer;
- (void)didLongPressEventView:(DayViewCell *)dayViewCell recognizer:(UILongPressGestureRecognizer *)recognizer;
@end
